const express = require('express');
const bodyParser = require('body-parser');
var mongoose = require('mongoose'); 
const path = require('path');
const fs = require('fs');
const cors = require('cors');
const session = require('express-session')

const app = express(); 
const api  = require('./routes/api/index');
const index = require('./routes/index');
const dashboard  = require('./routes/dashboard/index');
const task  = require('./routes/task/index');
const agentCustomer = require('./routes/customer/agent/index');
const agentReport = require('./routes/report/agent/index');
const agentSetting = require('./routes/setting/agent/index');

const mongoDB = 'mongodb://localhost/MrRinggit';
mongoose.connect(mongoDB, { useUnifiedTopology: true,useNewUrlParser: true });
mongoose.set('useFindAndModify', false);

app.use(session({secret:"MrRinggit@1234", resave:false, saveUninitialized:true}))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');

app.use(express.static(path.join(__dirname, 'assets')));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const port = process.env.PORT || 7000; 

app.use('/api', api);
app.use('/dashboard', dashboard);
app.use('/task', task); 
app.use('/agent/customer', agentCustomer);
app.use('/agent/report', agentReport);
app.use('/agent/setting', agentSetting);

app.use('/', index);

app.listen(port);
console.log("Connected to port : " + port);
