const express = require('express');
const bodyParser = require('body-parser');
var mongoose = require('mongoose');
const path = require('path');
const fs = require('fs');
const moment = require('moment');
const cors = require('cors');
const session = require('express-session')
const https = require('https');

const app = express();
const api = require('./routes/api/index');
const index = require('./routes/index');
const dashboard = require('./routes/dashboard/index');
const task = require('./routes/task/index');

const User = require('./models/user/user');

const forceResetPassword = require('./routes/forceResetPassword/index');

const agentDashboard = require('./routes/dashboard/agent/index');

const agentTask = require('./routes/task/agent/index');
const agentTaskNewApplication = require('./routes/task/agent_NewApplication/index');
const agentTaskCommissionWithdrawal = require('./routes/task/agent_CommissionWithdrawal/index');
const agentTaskDebtCollection = require('./routes/task/agent_DebtCollection/index');

const agentCustomer = require('./routes/customer/agent/index');
const agentCustomerOnLoan = require('./routes/customer/agentOnLoan/index');
const agentCustomerOverduePayment = require('./routes/customer/agentOverduePayment/index');
const agentCustomerPaid = require('./routes/customer/agentPaid/index');
const agentCustomerInProgress = require('./routes/customer/agentInProgress/index');
const agentCustomerBlacklisted = require('./routes/customer/agentBlacklisted/index');
const agentCustomerRejected = require('./routes/customer/agentRejected/index');
const agentCustomerDetails = require('./routes/customer/customerDetails/agent/index');

const agentReport = require('./routes/report/agent/index');
const agentReportPerformance = require('./routes/report/agentPerformance/index');
const agentReportPrincipal = require('./routes/report/agentPrincipal/index');
const agentReportLendOut = require('./routes/report/agentLendOut/index');
const agentReportOutstanding = require('./routes/report/agentOutstanding/index');
const agentReportCollected = require('./routes/report/agentCollected/index');
const agentReportPaymentRecord = require('./routes/report/agentPaymentRecord/index');

const agentExpenses = require('./routes/expenses/agent/index');
const agentExpensesPending = require('./routes/expenses/agentPending/index');
const agentExpensesPaid = require('./routes/expenses/agentPaidExpenses/index');
const agentExpensesBonus = require('./routes/expenses/agentBonus/index');
const agentExpensesOverall = require('./routes/expenses/agentOverall/index');
const agentExpensesApplyNew = require('./routes/expenses/agentApplyNewExpenses/index');

const agentSetting = require('./routes/setting/agent/index');
const agentSettingChangePassword = require('./routes/setting/agentChangePassword/index');
const agentSettingChangeReason = require('./routes/setting/agentChangeReason/index');
const agentSettingChangeLanguage = require('./routes/setting/agentChangeLanguage/index');

const managerDashboard = require('./routes/dashboard/manager/index');

const managerTask = require('./routes/task/manager/index');
const managerTaskDisbursement = require('./routes/task/manager_Disbursement/index');
const managerTaskDebtCollection = require('./routes/task/manager_DebtCollection/index');
const managerTaskBlacklistRequest = require('./routes/task/manager_BlacklistRequest/index');
const managerTaskReleaseCommission = require('./routes/task/manager_ReleaseCommission/index');
const managerTaskPayExpenses = require('./routes/task/manager_PayExpenses/index');

const managerCustomer = require('./routes/customer/manager/index');
const managerCustomerOnLoan = require('./routes/customer/managerOnLoan/index');
const managerCustomerOverduePayment = require('./routes/customer/managerOverduePayment/index');
const managerCustomerPaid = require('./routes/customer/managerPaid/index');
const managerCustomerInProgress = require('./routes/customer/managerInProgress/index');
const managerCustomerBlacklisted = require('./routes/customer/managerBlacklisted/index');
const managerCustomerRejected = require('./routes/customer/managerRejected/index');
const managerCustomerDetails = require('./routes/customer/customerDetails/manager/index');

const managerReport = require('./routes/report/manager/index')
const managerReportPerformance = require('./routes/report/managerAgentPerformance/index')
const managerReportPrincipal = require('./routes/report/managerPrincipal/index')
const managerReportLendOut = require('./routes/report/managerLendOut/index')
const managerReportEarnings = require('./routes/report/managerEarnings/index')
const managerReportOutstanding = require('./routes/report/managerOutstanding/index')
const managerReportPaymentRecord = require('./routes/report/managerPaymentRecord/index')

const managerExpenses = require('./routes/expenses/manager/index');
const managerExpensesOverall = require('./routes/expenses/managerOverall/index')
const managerExpensesPending = require('./routes/expenses/managerPending/index')
const managerExpensesBankedIn = require('./routes/expenses/managerBankedIn/index')

const managerSetting = require('./routes/setting/manager/index')
const managerSettingChangePassword = require('./routes/setting/managerChangePassword/index');
const managerSettingChangeReason = require('./routes/setting/managerChangeReason/index');
const managerSettingChangeLanguage = require('./routes/setting/managerChangeLanguage/index');
const managerSettingLoanDetails = require('./routes/setting/managerLoanDetails/index');
const managerSettingManagerList = require('./routes/setting/managerList/index');
const managerSettingAgentList = require('./routes/setting/managerAgentList/index');

const mongoDB = 'mongodb://localhost/MrRinggit';
mongoose.connect(mongoDB, { useUnifiedTopology: true,useNewUrlParser: true });
mongoose.set('useFindAndModify', false);

app.use(session({ secret: "MrRinggit@1234", resave: false, saveUninitialized: true }))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'assets')));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const port = process.env.PORT || 7000;

app.use('/api', api);
app.use('/dashboard', dashboard);
app.use('/task', task);

app.use('/forceResetPassword', forceResetPassword);

app.use('/agent', dashboard);
app.use('/agent/dashboard', agentDashboard);
app.use('/agent/task', agentTask);
app.use('/agent/taskNewApplication', agentTaskNewApplication);
app.use('/agent/taskDebtCollection', agentTaskDebtCollection);
app.use('/agent/taskCommissionWithdrawal', agentTaskCommissionWithdrawal);

app.use('/agent/customer', agentCustomer);
app.use('/agent/customerOnLoan', agentCustomerOnLoan);
app.use('/agent/customerOverduePayment', agentCustomerOverduePayment);
app.use('/agent/customerPaid', agentCustomerPaid);
app.use('/agent/customerInProgress', agentCustomerInProgress);
app.use('/agent/customerBlacklisted', agentCustomerBlacklisted);
app.use('/agent/customerRejected', agentCustomerRejected);
app.use('/agent/customerDetails', agentCustomerDetails);

app.use('/agent/report', agentReport);
app.use('/agent/reportPerformance', agentReportPerformance);
app.use('/agent/reportPrincipal', agentReportPrincipal);
app.use('/agent/reportLendOut', agentReportLendOut);
app.use('/agent/reportOutstanding', agentReportOutstanding);
app.use('/agent/reportCollected', agentReportCollected);
app.use('/agent/reportPaymentRecord', agentReportPaymentRecord);

app.use('/agent/expenses', agentExpenses);
app.use('/agent/expensesPending', agentExpensesPending);
app.use('/agent/expensesPaid', agentExpensesPaid);
app.use('/agent/expensesBonus', agentExpensesBonus);
app.use('/agent/expensesOverall', agentExpensesOverall);
app.use('/agent/expensesApplyNew', agentExpensesApplyNew);

app.use('/agent/setting', agentSetting);
app.use('/agent/settingChangePassword', agentSettingChangePassword);
app.use('/agent/settingChangeReason', agentSettingChangeReason);
app.use('/agent/settingChangeLanguage', agentSettingChangeLanguage);

app.use('/manager', dashboard);
app.use('/manager/dashboard', managerDashboard);
app.use('/manager/task', managerTask);
app.use('/manager/taskDisbursement', managerTaskDisbursement);
app.use('/manager/taskDebtCollection', managerTaskDebtCollection);
app.use('/manager/taskBlacklistRequest', managerTaskBlacklistRequest);
app.use('/manager/taskReleaseCommission', managerTaskReleaseCommission);
app.use('/manager/taskPayExpenses', managerTaskPayExpenses);

app.use('/manager/customer', managerCustomer);
app.use('/manager/customerOnLoan', managerCustomerOnLoan);
app.use('/manager/customerOverduePayment', managerCustomerOverduePayment);
app.use('/manager/customerPaid', managerCustomerPaid);
app.use('/manager/customerInProgress', managerCustomerInProgress);
app.use('/manager/customerBlacklisted', managerCustomerBlacklisted);
app.use('/manager/customerRejected', managerCustomerRejected);
app.use('/manager/customerDetails', managerCustomerDetails);

app.use('/manager/report', managerReport);
app.use('/manager/reportPerformance', managerReportPerformance);
app.use('/manager/reportPrincipal', managerReportPrincipal);
app.use('/manager/reportLendOut', managerReportLendOut);
// app.use('/manager/reportEarnings', managerReportEarnings);
app.use('/manager/reportOutstanding', managerReportOutstanding);
app.use('/manager/reportPaymentRecord', managerReportPaymentRecord);

app.use('/manager/expenses', managerExpenses);
app.use('/manager/expensesOverall', managerExpensesOverall);
app.use('/manager/expensesPending', managerExpensesPending);
app.use('/manager/expensesBankedIn', managerExpensesBankedIn);
// app.use('/manager/expensesDetails', managerExpensesDetails);

app.use('/manager/setting', managerSetting);
app.use('/manager/settingChangePassword', managerSettingChangePassword);
app.use('/manager/settingChangeReason', managerSettingChangeReason);
app.use('/manager/settingChangeLanguage', managerSettingChangeLanguage);
app.use('/manager/settingLoanDetails', managerSettingLoanDetails);
app.use('/manager/settingManager', managerSettingManagerList);
app.use('/manager/settingAgent', managerSettingAgentList);

app.use('/', index);

app.use(isLoggedIn,function(req,res,next){
    res.render('partion/404')
})

app.listen(port);
console.log("Connected to port : " + port);

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		return next();
	}
	res.redirect('/login');
}

global.pushNotification = function(userId,messageData){

	User.findById(userId,{oneSignalId:1}).exec((err,userDetails)=>{
		let oneSignalId = userDetails.oneSignalId ? userDetails.oneSignalId : ""

		var data = { 
			"app_id": "adecc18e-b669-4c48-834c-01bb1be81980",
			"include_player_ids": [oneSignalId],
			"contents": {
				"en": messageData.content
			},
			"headings": {
				"en": messageData.header
			},
		};

		var headers = {
			"Content-Type": "application/json; charset=utf-8",
			"Authorization": "Basic NWMxNTEzOWUtYzk2My00NjQ0LTkxNjUtN2YwYWQxOGI3MDQz"
		};
		
		var options = {
			host: "onesignal.com",
			port: 443,
			path: "/api/v1/notifications",
			method: "POST",
			headers: headers
		};
		
		var req = https.request(options, function(res) {  
			res.on('data', function(data) {
				// console.log("Response:");
				// console.log(JSON.parse(data));
			});
		});
		
		req.on('error', function(e) {
			console.log("ERROR:");
			console.log(e);
		});
		
		req.write(JSON.stringify(data));
		req.end();
	})
};