const mongoose = require('mongoose');
const async = require("async");
const cron = require('node-cron');
const moment = require('moment');
const momentTZ = require('moment-timezone');
momentTZ.tz.setDefault("Asia/Kuala_Lumpur");

const mongoDB = 'mongodb://localhost/MrRinggit';
mongoose.connect(mongoDB, { useUnifiedTopology: true,useNewUrlParser: true });
mongoose.set('useFindAndModify', false);

let User = require('./models/user/user');
let Admin = require('./models/admin/admin');
let Contact = require('./models/contact/contact');
let Location = require('./models/location/location');
let CallRecord = require('./models/callRecord/callRecord');
let Loan = require('./models/loan/loan');
let BankDetail = require('./models/bankDetail/bankDetail');
let Payment = require('./models/payment/payment');
let StaticContent = require('./models/staticContent/staticContent');
let Language = require('./models/language/language');
let LoanDetail = require('./models/loanDetail/loanDetail');

// if(momentTZ("2020-02-20T13:47:39+08:00").format("YYYY-MM-DD") == momentTZ(moment.now()).format("YYYY-MM-DD")){
//     console.log(momentTZ("2020-02-20T13:47:39+08:00").format())
//     console.log(momentTZ(moment.now()).format())
//     console.log("true")
// }

let nowYYYYMMDD = momentTZ(/* '2020-03-20T05:51:07.034Z' */moment.now()).format("YYYY-MM-DD")
// console.log(nowYYYYMMDD);

console.log(moment().format("YYYY-MM-DD, h:mm:ss a"))
// cron.schedule('0 16 20 02 *', () => {
//     console.log(moment().format("YYYY-MM-DD, h:mm:ss a"))
//     callPaymentFunction()
//     cron.schedule('0 */1 * * *', () => {
//         console.log(moment().format("YYYY-MM-DD, h:mm:ss a"))
//         callPaymentFunction()
//       });
//     // console.log('running a task every minute');
// });


// cron.schedule('40 22 20 02 *', () => {
//     cron.schedule('0 0 0 */1 * *', () => {
//         // console.log('running a task every two minutes');
//       });
//     // console.log('running a task every minute');
// });

/* function callPaymentFunction() {
    Payment.find({status: "pending", paymentDueDate : {$lte : moment(nowYYYYMMDD).add(1,'days').add(-1,'seconds') }}).exec((err,PaymentAll)=>{
        // console.log(moment())
        // console.log(moment().add(1,'days'))
        // console.log(moment().add(1,'days').add(-1,'seconds'))
    
        // console.log(PaymentAll.length)
        let newPayment = []
        async.each(PaymentAll,(eachPayment,callback)=>{
            Loan.findById(eachPayment.loanId,{installment:1}).exec((err,loanInstallment)=>{
                let paymentDueDateYYYYMMDD = momentTZ(eachPayment.paymentDueDate).format("YYYY-MM-DD")
        
                // console.log(newPayment.map(function(d){return d['userId']}).indexOf(eachPayment.userId) < 0)
                // callback()
        
                if(newPayment.map(function(d){return d['userId']}).indexOf(eachPayment.userId) < 0){
                    newPayment.push({
                        userId : eachPayment.userId,
                        loanId  : eachPayment.loanId,
                        numberOverdue : 1,
                        amountOverdue : loanInstallment.installment,
                        dayOverdue : momentTZ(nowYYYYMMDD).diff(momentTZ(paymentDueDateYYYYMMDD), 'days'),
                        paymentDueDate : eachPayment.paymentDueDate,
                        overduePayments : [eachPayment] 
                    })
                    callback()
                }else{
                    newPayment[newPayment.map(function(d){return d['userId']}).indexOf(eachPayment.userId)].amountOverdue += loanInstallment.installment
                    momentTZ(nowYYYYMMDD).diff(momentTZ(paymentDueDateYYYYMMDD), 'days') > newPayment[newPayment.map(function(d){return d['userId']}).indexOf(eachPayment.userId)].paymentDueDate ? 
                        momentTZ(nowYYYYMMDD).diff(momentTZ(paymentDueDateYYYYMMDD), 'days') : ""
                    newPayment[newPayment.map(function(d){return d['userId']}).indexOf(eachPayment.userId)].overduePayments.push(eachPayment)
                    newPayment[newPayment.map(function(d){return d['userId']}).indexOf(eachPayment.userId)].numberOverdue += 1
                    paymentDueDateYYYYMMDD < momentTZ(newPayment[newPayment.map(function(d){return d['userId']}).indexOf(eachPayment.userId)].paymentDueDate).format("YYYY-MM-DD") ? 
                        newPayment[newPayment.map(function(d){return d['userId']}).indexOf(eachPayment.userId)].paymentDueDate = eachPayment.paymentDueDate  : ""
                    callback()
                }
                
            })
    
            // let paymentDueDateYYYYMMDD = momentTZ(eachPayment.paymentDueDate).format("YYYY-MM-DD")
            // if(paymentDueDateYYYYMMDD == nowYYYYMMDD){
            //     // Loan.findByIdAndUpdate(eachPayment.loanId,{nextPaymentToday : true}).exec((err,done)=>{
            //     //     Payment.findByIdAndUpdate(eachPayment.loanId,{nextPaymentToday : true}).exec((err,done)=>{
            //             console.log("here")
            //             callback()
            //     //     })
            //     // })
            // }else if(paymentDueDateYYYYMMDD < nowYYYYMMDD){
            //     console.log("here1")
            //     callback()
            // }else{
            //     console.log("here2")
            //     callback()
            // }
        },(err)=>{
            // console.log(newPayment[0])
            // console.log("2020-02-03T07:11:25.000Z to nowYYYYMMDD : " + momentTZ(nowYYYYMMDD).diff(momentTZ("2020-02-03T07:11:25.000Z"), 'days'))
            // console.log("2020-03-03T07:11:25.000Z to nowYYYYMMDD : " + momentTZ(nowYYYYMMDD).diff(momentTZ("2020-03-03T07:11:25.000Z"), 'days'))
            
            async.each(newPayment,(eachNewPayment,callback)=>{
                async.each(eachNewPayment.overduePayments,(eachOverduePayment,callback)=>{
                    let paymentDueDateYYYYMMDD = momentTZ(eachOverduePayment.paymentDueDate).format("YYYY-MM-DD")
                    if(paymentDueDateYYYYMMDD == nowYYYYMMDD){
                        Loan.findByIdAndUpdate(eachOverduePayment.loanId,{nextPaymentToday : true}).exec((err,done)=>{
                            Payment.findByIdAndUpdate(eachOverduePayment.loanId,{nextPaymentToday : true}).exec((err,done)=>{
                                console.log("here")
                                callback()
                            })
                        })
                    }else{
                        Loan.findByIdAndUpdate(eachOverduePayment.loanId,{
                            numberOverdue : eachNewPayment.numberOverdue,
                            dayOverdue : eachNewPayment.dayOverdue,
                            amountOverdue : eachNewPayment.amountOverdue,
                        }).exec((err,done)=>{
                            console.log("done")
                            callback()
                        })
                    }
                },(err)=>{
                    callback()
                })
            },(err)=>{
                console.log("done here")
            })
    
        })
    })

} */



Loan.find({status : "accepted"}).exec((err,loanAll)=>{
    // console.log(loanAll[0])
    // console.log("installment : "+loanAll[0].installment)
    // console.log("duration : "+ parseInt(loanAll[0].duration) )
    // console.log("paymentType : "+ loanAll[0].paymentType )approvalManagerTime

    // console.log(momentTZ(loanAll[0].approvalManagerTime).format())
    // console.log(momentTZ(moment(loanAll[0].approvalManagerTime).add(1,'M')).format())
    // console.log(momentTZ(moment.now()).format())
    // console.log(moment(moment.now()).format())


    async.each(loanAll,(eachLoan,callback)=>{
        for(let i=0; i< parseInt(eachLoan.duration ); i++ ){
            let newPayment = new Payment({
                userId : eachLoan.userId, 
                loanId : eachLoan._id,
                status : "preset",
                // amount : {type: Number},
                // imageUrl : {type: String},
                // paymentNumber : {type: Number, default : 0},
                // createdAt : {type: Date}, 
                // createdAtInitail : {type: Date, default: Date.now}, 
                // paidBy : {type: String, default : "Customer"},
                paymentDueDate : momentTZ(moment(eachLoan.approvalManagerTime).add((i+1),'M')).format(),
            })
            newPayment.save((err)=>{
                console.log("done + "+i)
            })
        }
        callback()
    },(err)=>{
        console.log("done")
    })
})
