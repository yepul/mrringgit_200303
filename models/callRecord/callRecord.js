const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let callRecordSchema = new Schema ({
    userId : {type: String}, 
    name : {type: String},
    number : {type: String},
    type : {type: String},
    duration : {type: String},
    date : {type: String},
})

module.exports = mongoose.model('callRecord', callRecordSchema);