const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let languageSchema = new Schema ({
    type : {type: String}, 
    title : {type: String},
    en : {type: String},
    bm : {type: String},
    ch : {type: String},
    status : {type: String},
    createdAt : {type: Date, default: Date.now},
})

module.exports = mongoose.model('language', languageSchema);