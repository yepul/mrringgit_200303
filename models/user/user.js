const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let userSchema = new Schema ({
    lang : {type: String},

    status : {type: String},
    createdAt : {type: Date, default: Date.now}, 

    phoneNumber : {type: String},
    verificationCode : {type: String},
    password : {type: String},
    referralCode : {type: String},

    username : {type: String},
    ic : {type: String},
    race : {type: String},
    gender : {type: String},
    education : {type: String},
    marriage : {type: String},
    profilePictureUrl : {
        value : {type: String},
        status : {type: String},
    },
    frontICUrl : {
        value : {type: String},
        status : {type: String},
    },
    backICUrl : {
        value : {type: String},
        status : {type: String},
    },

    residentialType : {type: String},
    address : {type: String},
    postcode : {type: String},
    city : {type: String},
    state : {type: String},
    residentialImageProofUrl : [{
        value : {type: String},
        status : {type: String},
    }],

    bankName : {type: String},
    bankAccountName : {type: String},
    bankAccountNumber : {type: String},
    bankAccountStatement : [{
        value : {type: String},
        status : {type: String},
    }],

    companyName : {type: String},
    companyWebsite : {type: String},
    position : {type: String},
    monthlyIncome : {type: String},
    companyContactNumber : {type: String},
    companyAddress : {type: String},
    companyPostcode : {type: String},
    companyCity : {type: String},
    companyState : {type: String},
    payslip : [{
        value : {type: String},
        status : {type: String},
    }],
    EPFStatement : [{
        value : {type: String},
        status : {type: String},
    }],
    companyOfferLetter : [{
        value : {type: String},
        status : {type: String},
    }],
    companyFormSSM : {
        value : {type: String},
        status : {type: String},
    },
    companySignboard : {
        value : {type: String},
        status : {type: String},
    },
    companyNameCard : {
        value : {type: String},
        status : {type: String},
    },
    companyOtherProof : [{
        value : {type: String},
        status : {type: String},
    }],

    emergencyContact : [{
        relationship : {type: String},
        name : {type: String},
        contactNumber : {type: String},
    }],

    currentLoanId : {type: String},
    agent : {type: String},
    agentId : {type: String},
    blacklisted : {type: Boolean, default: false},
    blacklistedAt : {type: Date}, 

    oneSignalId : {type: String},
	referBy: {type: String},
    
    balanceMoney : {type: Number, default : 0}, 
    commissionBalance : {type: Number, default : 0}, 
})

module.exports = mongoose.model('user', userSchema);