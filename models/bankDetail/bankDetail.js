const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let bankDetailSchema = new Schema ({
    accountName : {type: String},
    bankName : {type: String},
    accountNumber : {type: String},
    status : {type: String},
    createdAt : {type: Date, default: Date.now}, 
})

module.exports = mongoose.model('bankDetail', bankDetailSchema);