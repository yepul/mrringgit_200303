const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let expensesSchema = new Schema ({
    type : {type: String},
    agentId : {type: String},
    category : {type: String},
    remark : {type: String},
    expensesProof : {type: String},
    status : {type: String},
    expenses : {type: Number},
    // bonus : {type: Number},
    principal : {type: Number},
    createdAt : {type: Date, default: Date.now},

    approvalManager : {type: String, default:"pending"},
    approvalManagerTime : {type: Date},
    approvalManagerId : {type: String},
    approvalManagerName : {type: String},
    rejectionReasonManager : {type: String},
    disbursementProof : {type: String},

    bonusReasonManager : {type: String},

    userId : {type: String},
    
})

module.exports = mongoose.model('expenses', expensesSchema);