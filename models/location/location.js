const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let locationSchema = new Schema ({
    userId : {type: String}, 
    latitude : {type: String},
    longitude : {type: String},
})

module.exports = mongoose.model('location', locationSchema);