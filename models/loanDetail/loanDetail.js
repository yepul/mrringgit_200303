var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var loanDetailSchema = new Schema ({
    status : {type: String, default: "active"},
    lowestAmountPersonal : {type: Number},
    highestAmountPersonal : {type: Number},
    minimumDurationPersonal : {type: Number},
    highestDurationPersonal : {type: Number},
    interestPersonal : {type: Number},
    adminFeePersonal : {type: Number},
    overduePersonal : {type: Number},
    finePersonal : {type: Number},
    lowestAmountCompany : {type: Number},
    highestAmountCompany : {type: Number},
    minimumDurationCompany : {type: Number},
    highestDurationCompany : {type: Number},
    interestCompany : {type: Number},
    adminFeeCompany : {type: Number},
    overdueCompany : {type: Number},
    fineCompany : {type: Number}, 
});

module.exports = mongoose.model('LoanDetail', loanDetailSchema);
