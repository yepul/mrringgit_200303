const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let contactSchema = new Schema ({
    userId : {type: String}, 
    name : {type: String},
    number : {type: String},
})

module.exports = mongoose.model('contact', contactSchema);