const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let loanSchema = new Schema ({
    customerId : {type: String}, 
    loanType : {type: String},
    amount : {type: Number},
    duration : {type: String},
    interest : {type: Number},
    interestRate : {type: Number},
    offset : {type: Number},
    offsetRate : {type: Number},
    installment : {type: Number},
    paymentType : {type: String},
    requestedOn : {type: Date, default: Date.now},
    agentId : {type: String},
    approval : {type: String},
    rejectionReason : {type: String},
})

module.exports = mongoose.model('loan', loanSchema);