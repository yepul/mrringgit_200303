const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let loanSchema = new Schema ({
    userId : {type: String}, 
    loanType : {type: String},
    status : {type: String},
    amount : {type: Number},
    principalAmount : {type: Number},
    outstanding : {type: Number},
    paymentType : {type: String},
    duration : {type: String},
    interest : {type: Number},
    interestRate : {type: Number},
    interestRateTotal : {type: Number},
    offset : {type: Number, default: 0},
    offsetRate : {type: Number},
    disbursement : {type: Number},
    processingFee : {type: Number}, 
    installment : {type: Number},
    requestedOn : {type: Date, default: Date.now},
    agentId : {type: String},
    agent : {type: String},

    approvalAgent : {type: String, default:"pending"},
    approvalAgentTime : {type: Date},
    approvalAgentId : {type: String},
    approvalAgentName : {type: String},
    rejectionReasonAgent : {type: String},
    approvalManager : {type: String, default:"pending"},
    approvalManagerTime : {type: Date},
    approvalManagerId : {type: String},
    approvalManagerName : {type: String},
    rejectionReasonManager : {type: String},
    disbursementProof : {type: String},

    numberOverdue : {type: Number, default: 0},
    dayOverdue : {type: Number, default: 0},
    amountOverdue : {type: Number, default: 0},
    earningsAndLosses : {type: Number},

    paymentNumber : {type: Number, default : 0},
    nextPayment : {type: Date},
    nextPaymentToday : {type: Boolean, default : false},

    fine : {type: Number, default : 0},
    fullyPaidOn : {type: Date}, 
    rejectedAt : {type: Date},
})

module.exports = mongoose.model('loan', loanSchema);