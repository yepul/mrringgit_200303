const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let paymentSchema = new Schema ({
    userId : {type: String}, 
    loanId : {type: String},
    status : {type: String, default:"pending"},
    amount : {type: Number},
    imageUrl : {type: String},
    paymentNumber : {type: Number, default : 0},
    createdAt : {type: Date}, 
    createdAtInitail : {type: Date, default: Date.now}, 
    paidBy : {type: String, default : "Customer"},
    paymentDueDate : {type: Date},
    
    nextPaymentToday : {type: Boolean, default : false},

    principalPaid : {type: Number},
    interestPaid : {type: Number},

    approvalAgent : {type: String, default:"pending"},
    approvalAgentTime : {type: Date},
    approvalAgentId : {type: String},
    approvalAgentName : {type: String},
    rejectionReasonAgent : {type: String},
    approvalManager : {type: String, default:"pending"},
    approvalManagerTime : {type: Date},
    approvalManagerId : {type: String},
    approvalManagerName : {type: String},
    rejectionReasonManager : {type: String},
})

module.exports = mongoose.model('payment', paymentSchema);