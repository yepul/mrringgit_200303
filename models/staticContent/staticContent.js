var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var staticContentSchema = new Schema ({
    contentName : {type: String},
    type : {type: String},
    title : {type: String},
    description : {type: String},
    content : {type: String},
    contentImage : {type: String},
    status : {type: String},
    lastUpdate : {type: Date, default: Date.now}, 
});

module.exports = mongoose.model('StaticContent', staticContentSchema);
