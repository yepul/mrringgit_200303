var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var adminSchema = new Schema ({
    name : {type: String},
    email : {type: String},
    contactNumber : {type: String}, 
    ic : {type: String}, 
    referralCode : {type: String}, 
    superAdmin : { type: Boolean, default: false },
    level : {type: String},
    status : {type: String},
    createdAt : {type: Date, default: Date.now}, 
	referBy: {type: String},
	reason:[{
		type: {type: String},
		reason: {type: String},	
        createdAt : {type: Date, default: Date.now}, 
    }],
    bankDetail:{
        bankAccountName : {type: String},
        bankName : {type: String},
        bankAccountNumber : {type: String},
    },
    adminAccess : {
        viewAllCustomerInfo : { type: Boolean, default: false },
        editCustomerInfo : { type: Boolean, default: false },
        loanApplicationApproval : { type: Boolean, default: false },
        addLoan : { type: Boolean, default: false },
        disbursement : { type: Boolean, default: false },
        debtCollection : { type: Boolean, default: false },
        releaseCommission : { type: Boolean, default: false },
        blacklistRequest : { type: Boolean, default: false },
        payExpenses : { type: Boolean, default: false },
        bankedInBonusToAgent : { type: Boolean, default: false },
    },
    amountHold : {type: Number}, //principal
    password : {type: String},
    frontICUrl : {
        value : {type: String},
        status : {type: String, default: "pending"},
    },
    backICUrl : {
        value : {type: String},
        status : {type: String, default: "pending"},
    },
    forceUpdatePassword : { type: Boolean, default: false }, 
});

module.exports = mongoose.model('Admin', adminSchema);
