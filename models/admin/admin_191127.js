var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var adminSchema = new Schema ({
    name : {type: String},
    email : {type: String},
    level : {type: String},
    status : {type: String},
    createdAt : {type: Date, default: Date.now}, 
    password : {type: String},
});

module.exports = mongoose.model('Admin', adminSchema);
