const express = require('express');
const bcrypt = require('bcrypt');
const moment = require('moment');
let async = require('async');
let request = require('request');
let path = require('path');
const shortid = require('shortid');
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');
const momentTZ = require('moment-timezone');
momentTZ.tz.setDefault("Asia/Kuala_Lumpur");

let app = express();
let User = require('../models/user/user');
let Admin = require('../models/admin/admin');
let Loan = require('../models/loan/loan');
let Payment = require('../models/payment/payment');
let Expenses = require('../models/expenses/expenses');
let Language = require('../models/language/language');

const upload = require('../uploadMiddleware');
const Resize = require('../Resize');

app.get('/', isLoggedIn, (req, res) => {
    res.redirect('/dashboard');
})

app.get('/login', (req, res) => {
    req.session.lang && req.session.lang != undefined && req.session.lang != "undefined" && req.session.lang != "" ? "" : req.session.lang = "en"
    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type:"login"}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            res.render('login/index',{ langData : langData, lang : lang, loginFail : false});
        })
    })
})

app.post('/login', function (req, res, next) {
    // console.log(req.body)
    let name = req.body.username
    let password = req.body.password
    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type:"login"}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            Admin.findOne({ name: name },{name:1,email:1,level:1,password:1,forceUpdatePassword:1}).exec((err, admin) => {
                if (admin && bcrypt.compareSync(password, admin.password)) {
                    if(typeof admin.forceUpdatePassword != "undefined" && admin.forceUpdatePassword == true){
                        res.redirect('/forceResetPassword?id='+ admin._id)
                    }else if (admin.level == "agent") {
                        req.session.admin = {_id: admin._id, name: admin.name, email: admin.email, level: admin.level};
                        res.redirect('/agent/dashboard');
                    } else if (admin.level == "manager") {
                        req.session.admin = {_id: admin._id, name: admin.name, email: admin.email, level: admin.level};
                        res.redirect('/manager/dashboard');
                    } else {
                        res.render('login/index',{ langData : langData, lang : lang, loginFail : true});
                    }
                } else {
                    res.render('login/index',{ langData : langData, lang : lang, loginFail : true});
                }
            })

        })
    })
})

app.get('/logout', isLoggedIn, (req, res) => {
    req.session.admin = "";
    req.session.lang = "";
    res.redirect('/dashboard');
})

app.post('/uploadFrontIC', upload.single('image'), async function (req, res, next) {
    let userId = req.body.userId
    const imagePath = path.join(__dirname, '../assets/img/ic');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { frontICUrl: { status: "checking", value: req.headers.host + "/img/ic/" + filename } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadBackIC', upload.single('image'), async function (req, res, next) {
    let userId = req.body.userId
    const imagePath = path.join(__dirname, '../assets/img/ic');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { backICUrl: { value: req.headers.host + "/img/ic/" + filename, status: "checking" } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadProfilePicture', upload.single('image'), async (req, res) => {
    // console.log(req.file)
    let userId = req.body.userId/* .body *//* .userId */
    console.log("userId : " + userId)
    const imagePath = path.join(__dirname, '../assets/img/profilePicture');
    console.log("imagePath : " + imagePath)
    const fileUpload = new Resize(imagePath);
    // console.log("fileUpload : "+ fileUpload.stringify())
    const filename = await fileUpload.save(req.file.buffer);

    if (req.file) {
        User.findByIdAndUpdate(userId, { profilePictureUrl: { value: req.headers.host + "/img/profilePicture/" + filename, status: "checking" } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 401 })
            }
        })
    } else {
        res.json({ status: 400, msg: 'Please provide an image!' });
    }
})

app.post('/uploadResidentialImageProof', upload.single('image'), async function (req, res, next) {
    let userId = req.body.userId
    const imagePath = path.join(__dirname, '../assets/img/residentialProof');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { $push: { residentialImageProofUrl: { value: req.headers.host + "/img/residentialProof/" + filename, status: "checking" } } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadBankAccountStatement', upload.single('image'), async function (req, res, next) {
    let userId = req.body.userId
    console.log(userId)
    const imagePath = path.join(__dirname, '../assets/img/bankStatement');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { $push: { bankAccountStatement: { value: req.headers.host + "/img/bankStatement/" + filename, status: "checking" } } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadPayslip', upload.single('image'), async function (req, res, next) {
    let userId = req.body.userId
    const imagePath = path.join(__dirname, '../assets/img/payslip');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { $push: { payslip: { value: req.headers.host + "/img/payslip/" + filename, status: "checking" } } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadEpfStatement', upload.single('image'), async function (req, res, next) {
    let userId = req.body.userId
    const imagePath = path.join(__dirname, '../assets/img/epfStatement');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { $push: { EPFStatement: { value: req.headers.host + "/img/epfStatement/" + filename, status: "checking" } } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadOfferLetter', upload.single('image'), async function (req, res, next) {
    let userId = req.body.userId
    const imagePath = path.join(__dirname, '../assets/img/offerLetter');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { companyOfferLetter: { value: req.headers.host + "/img/offerLetter/" + filename, status: "checking" } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadCompanyFormSSM', upload.single('image'), async function (req,res, next){
    let userId = req.body.userId
    const imagePath = path.join(__dirname, '../assets/img/companyProof');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { companyFormSSM: { value: req.headers.host + "/img/companyProof/" + filename, status: "checking" } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadCompanySignboard', upload.single('image'), async function (req,res, next){
    let userId = req.body.userId
    const imagePath = path.join(__dirname, '../assets/img/companyProof');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { companySignboard: { value: req.headers.host + "/img/companyProof/" + filename, status: "checking" } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadCompanyNameCard', upload.single('image'), async function (req,res, next){
    let userId = req.body.userId
    const imagePath = path.join(__dirname, '../assets/img/companyProof');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { companyNameCard: { value: req.headers.host + "/img/companyProof/" + filename, status: "checking" } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadCompanyOtherProof', upload.single('image'), async function (req, res, next) {
    let userId = req.body.userId
    const imagePath = path.join(__dirname, '../assets/img/companyProof');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        User.findByIdAndUpdate(userId, { $push: { companyOtherProof: { value: req.headers.host + "/img/companyProof/" + filename, status: "checking" } } }).exec((err, done) => {
            if (!err && done) {
                res.json({ status: 200 })
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/uploadPaymentProof', upload.single('image'), async function (req,res, next){
    const imagePath = path.join(__dirname, '../assets/img/payment');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        res.json({status:200, imageUrl: req.headers.host + "/img/payment/" + filename})
    }
})

app.post('/managerUploadPaymentDisbursement', upload.single('image'), async function (req,res, next){
    let loanId = req.body.loanId
    const imagePath = path.join(__dirname, '../assets/img/disbursementProof');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        Loan.findByIdAndUpdate(loanId, { 
            status : "accepted", 
            approvalManager : "approve", 
            approvalManagerTime : moment().toISOString(), 
            disbursementProof: req.headers.host + "/img/disbursementProof/" + filename
        }).exec((err, loanDetails) => {
            if (!err && loanDetails) {             
                for(let i=0; i< parseInt(loanDetails.duration ); i++ ){
                    let paymentDueDate = loanDetails.paymentType == "day" ? momentTZ(moment(loanDetails.approvalManagerTime).add((i+1),'days')).format() : 
                                         loanDetails.paymentType == "week" ? momentTZ(moment(loanDetails.approvalManagerTime).add(((i+1)*7),'days')).format() :
                                         loanDetails.paymentType == "twoweek" ? momentTZ(moment(loanDetails.approvalManagerTime).add(((i+1)*14),'days')).format() : 
                                         momentTZ(moment(loanDetails.approvalManagerTime).add((i+1),'M')).format()
                    
                    let newPayment = new Payment({
                        userId : loanDetails.userId, 
                        loanId : loanDetails._id,
                        status : "preset",
                        paymentDueDate : paymentDueDate,
                        paymentNumber : (i+1),
                    })
                    newPayment.save((err)=>{})
                    
                    let loanNextPayment = loanDetails.paymentType == "day" ? momentTZ(moment(loanDetails.approvalManagerTime).add(1,'days')).format() : 
                                          loanDetails.paymentType == "week" ? momentTZ(moment(loanDetails.approvalManagerTime).add(7,'days')).format() :
                                          loanDetails.paymentType == "twoweek" ? momentTZ(moment(loanDetails.approvalManagerTime).add(14,'days')).format() : 
                                          momentTZ(moment(loanDetails.approvalManagerTime).add(1,'M')).format()

                    Loan.findByIdAndUpdate(loanDetails.userId,{nextPayment : loanNextPayment}).exec((err,done)=>{
                        if(!err && done){
                            let message = {
                                header : "Your loan have been approved and disbursed.",
                                content : "Your loan ID : " + loanId + " has been approved and disbursed at " + moment().add(8, 'hours').format('MMM Do, YYYY') +  ". MrRinggit.",
                            }
                            pushNotification(loanDetails.userId,message)
                            res.redirect('/manager/taskDisbursement/pending')
                        }else{
                            res.json({ status: 400 })
                        }
                    })
                }
            } else {
                res.json({ status: 400 })
            }
        })
    }
})

app.post('/managerUploadPaymentExpenses', upload.single('image'), async function (req,res, next){
    // console.log(req.body)
    let expensesId = req.body.expensesId
    const imagePath = path.join(__dirname, '../assets/img/expensesProof/paymentManager');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        Expenses.findByIdAndUpdate(expensesId, { 
            status : "paid", 
            approvalManager : "approve", 
            approvalManagerTime : moment().toISOString(), 
            disbursementProof: "/img/expensesProof/paymentManager/" + filename,
            approvalManagerId : req.session.admin._id,
            approvalManagerName : req.session.admin.name,
        }).exec((err, done) => {
            if (!err && done) {
                res.redirect('/manager/expensesPending?payExpenses=success')
            } else {
                res.redirect('/manager/expensesPending?payExpenses=failed')
            }
        })
    }
})

app.post('/managerGiveOutBonus', upload.single('image'), async function (req,res, next){
    let reqBody = req.body
    const imagePath = path.join(__dirname, '../assets/img/expensesProof/paymentManager');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        let newExpenses = new Expenses({
            type : "bonus",
            agentId : reqBody.agentId,
            category : "Performance Bonus",
            remark : reqBody.reason,
            status : "paid",
            expenses : parseInt(reqBody.expenses*100)/100,
            approvalManager : "approve",
            approvalManagerTime : moment().toISOString(),
            approvalManagerId : req.session.admin._id,
            approvalManagerName : req.session.admin.name,
            disbursementProof : "/img/expensesProof/paymentManager/" + filename,
        })

        newExpenses.save((err,done)=>{
            res.redirect('/manager/expensesOverall?GiveOutBonus=success')
        })
    }
})

app.post('/agentApplyNewExpenses', upload.single('image'), async function (req,res, next){
    const imagePath = path.join(__dirname, '../assets/img/expensesProof/paymentAgent');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        let newExpenses = new Expenses({
            expenses : req.body.amount,
            type : "expenses",
            status : "pending",
            agentId : req.body.agentId,
            category : req.body.category,
            remark : req.body.remark,
            expensesProof : "/img/expensesProof/paymentAgent/" + filename,
        })
        newExpenses.save((err,done)=>{
            res.redirect('/agent/expensesPending?new=success')
        })
    }
})

app.post('/managerSettingAgentEdit', upload.single('image'), async function (req,res, next){
    let reqBody = req.body
    const imagePath = path.join(__dirname, '../assets/img/icAgent');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        // res.json({ status: 400, msg: 'Please provide an image!' });
        Admin.findByIdAndUpdate(reqBody.agentId,{
            name : reqBody.name,
            email : reqBody.email,
            contactNumber : reqBody.contactNumber,
            status : reqBody.status,
            ic : reqBody.ic,
            bankDetail : {
                bankAccountName : reqBody.bankAccountName,
                bankName : reqBody.bankName,
                bankAccountNumber : reqBody.bankAccountNumber,
            },
            amountHold : reqBody.amountHold,
            referralCode : reqBody.referralCode,
            level : reqBody.level,
        }).exec((err,done)=>{
            if(!err && done){
                res.redirect('/manager/settingAgent?updateAgent=success')
            }else{
                res.redirect('/manager/settingAgent?updateAgent=failed')
            }
        })
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        Admin.findByIdAndUpdate(reqBody.agentId,{
            name : reqBody.name,
            email : reqBody.email,
            contactNumber : reqBody.contactNumber,
            status : reqBody.status,
            ic : reqBody.ic,
            bankDetail : {
                bankAccountName : reqBody.bankAccountName,
                bankName : reqBody.bankName,
                bankAccountNumber : reqBody.bankAccountNumber,
            },
            amountHold : reqBody.amountHold,
            referralCode : reqBody.referralCode,
            level : reqBody.level,
            frontICUrl : {
                value : "/img/icAgent/" + filename, 
            } 
        }).exec((err,done)=>{
            if(!err && done){
                res.redirect('/manager/settingAgent?updateAgent=success')
            }else{
                res.redirect('/manager/settingAgent?updateAgent=failed')
            }
        })
    }
})

app.post('/managerSettingNewAgent', upload.single('image'), async function (req,res, next){
    let plainPassword = "12345678" /* Math.random().toString(36).substr(2, 8) */
    let saltRounds = 10;
    let salt = bcrypt.genSaltSync(saltRounds);
    let hash = bcrypt.hashSync(plainPassword, salt);

    let reqBody = req.body
    let referBy = req.query.referBy ? req.query.referBy : ""
    const imagePath = path.join(__dirname, '../assets/img/icAgent');
    const fileUpload = new Resize(imagePath);
    if (!req.file) {
        res.json({ status: 400, msg: 'Please provide an image!' });
    } else {
        const filename = await fileUpload.save(req.file.buffer);
        let newId = shortid.generate()
        let newAgent = new Admin({
            name : reqBody.name,
            email : reqBody.email,
            contactNumber : reqBody.contactNumber,
            status : reqBody.status,
            ic : reqBody.ic,
            referBy : referBy,
            password  : hash,
            referralCode : newId,
            bankDetail : {
                bankAccountName : reqBody.bankAccountName,
                bankName : reqBody.bankName,
                bankAccountNumber : reqBody.bankAccountNumber,
            },
            amountHold : reqBody.amountHold,
            referralCode : reqBody.referralCode,
            level : reqBody.level,
            frontICUrl : {
                value : "/img/icAgent/" + filename, 
            },
            forceUpdatePassword : true 
        })
        newAgent.save((err,done)=>{
            if(!err && done){
                res.redirect('/manager/settingAgent?newAgentSaved=success')
            }else{
                res.redirect('/manager/settingAgent?newAgentSaved=failed')
            }
        })
    }
})

module.exports = app;

function isLoggedIn(req, res, next) {
    if (req.session.admin) {
        return next();
    }
    res.redirect('/login');
}