const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', /* isLoggedIn, */(req, res) => {
	res.render('report/agent/main'/* ,{admin : req.session.admin} */);
})

app.get('/agentPerformance', /* isLoggedIn, */(req, res) => {
	res.render('report/agent/agentPerformance'/* ,{admin : req.session.admin} */);
})

app.get('/principal', /* isLoggedIn, */(req, res) => {
	res.render('report/agent/principal'/* ,{admin : req.session.admin} */);
})

app.get('/lendOut', /* isLoggedIn, */(req, res) => {
	res.render('report/agent/lendOut'/* ,{admin : req.session.admin} */);
})

app.get('/collected', /* isLoggedIn, */(req, res) => {
	res.render('report/agent/collected'/* ,{admin : req.session.admin} */);
})

app.get('/outstanding', /* isLoggedIn, */(req, res) => {
	res.render('report/agent/outstanding'/* ,{admin : req.session.admin} */);
})



module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}