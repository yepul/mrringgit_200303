const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let valueOfEachReport = {
		agentPerformance : 0,
		principal : 0,
		lendOut : 0,
		collected : 0,
		outstanding : 0
	}
	Payment.find({status : "accepted", approvalAgentId : req.session.admin._id},{principalPaid:1,amount:1}).exec((err,paymentReceived)=>{
		async.each(paymentReceived,(eachPayment,callback)=>{
			// valueOfEachReport.principal += eachPayment.principalPaid
			valueOfEachReport.collected += eachPayment.amount
			callback()
		},(err)=>{
			Loan.find({status : "accepted", agentId : req.session.admin._id},{disbursement:1,outstanding:1}).exec((err,loanApproved)=>{
				async.each(loanApproved,(eachLoan,callback)=>{
					valueOfEachReport.lendOut += eachLoan.disbursement
					valueOfEachReport.outstanding += eachLoan.outstanding
					callback()
				},(err)=>{
					parseInt(valueOfEachReport.principal * 100) / 100
					parseInt(valueOfEachReport.collected * 100) / 100
					parseInt(valueOfEachReport.lendOut * 100) / 100
					parseInt(valueOfEachReport.outstanding * 100) / 100

					res.render('report/agent/report',{valueOfEachReport : valueOfEachReport, admin : req.session.admin});
				})
			})
		})
	})
})

app.get('/agentPerformance', isLoggedIn,(req, res) => {
	res.render('report/agent/agentPerformance',{admin : req.session.admin});
})

app.get('/principal', isLoggedIn,(req, res) => {
	res.render('report/agent/principal',{admin : req.session.admin});
})

app.get('/lendOut', isLoggedIn,(req, res) => {
	res.render('report/agent/lendOut',{admin : req.session.admin});
})

app.get('/collected', isLoggedIn,(req, res) => {
	res.render('report/agent/collected',{admin : req.session.admin});
})

app.get('/outstanding', isLoggedIn,(req, res) => {
	res.render('report/agent/outstanding',{admin : req.session.admin});
})



module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}