const express = require('express');
const async = require('async')
const bcrypt = require('bcrypt');
const moment = require('moment');
const multiparty = require('connect-multiparty');

let Admin = require('../../../models/admin/admin');
let Language = require('../../../models/language/language');
let LoanDetail = require('../../../models/loanDetail/loanDetail');
let Loan = require('../../../models/loan/loan');
let Payment = require('../../../models/payment/payment');
let Expenses = require('../../../models/expenses/expenses');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/',isLoggedIn,(req, res) => {
    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    let agentList = []
    let adminQuery = {level: "agent"}
	req.query.searchByUsername ? adminQuery.name = req.query.searchByUsername : ""
    Language.find({type: {$in : ["header","setting","settingList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            Admin.find(adminQuery).exec((err,agentAll)=>{
                async.each(agentAll,(eachAgent,callback)=>{
                    let lendOut = 0
                    let collected = 0
                    let expenses = 0
                    let bonus = 0 
                    let expensesAndBonus = 0
                    let outstanding = 0
                    Loan.find({agentId : eachAgent._id, status: "accepted"},{duration:1,installment:1,outstanding:1}).exec((err,loanAll)=>{
                        async.each(loanAll,(eachLoan,callback)=>{
                            outstanding += eachLoan.outstanding ? parseInt(eachLoan.outstanding*100)/100 : 0,
                            lendOut += parseInt(eachLoan.installment * parseInt(eachLoan.duration)*100)/100,
                            callback()
                        },(err)=>{
                            Payment.find({approvalAgentId : eachAgent._id,status : "accepted"},{amount:1}).exec((err,paymentAll)=>{
                                async.each(paymentAll,(eachPayment,callback)=>{
                                    collected += parseInt(eachPayment.amount*100)/100,
                                    callback()
                                },(err)=>{
                                    Expenses.find({agentId : eachAgent._id, status : "paid"},{type:1,expenses:1}).exec((err,expensesAll)=>{
                                        async.each(expensesAll,(eachExpenses,callback)=>{
                                            expensesAndBonus += parseInt(eachExpenses.expenses*100)/100,
                                            bonus += eachExpenses.type == "bonus" ? parseInt(eachExpenses.expenses*100)/100 : 0
                                            expenses += eachExpenses.type == "expenses" ? parseInt(eachExpenses.expenses*100)/100 : 0
                                            callback() 
                                        },(err)=>{
                                            agentList.push({
                                                agentId : eachAgent._id,
                                                name : eachAgent.name,
                                                createdAt : moment(eachAgent.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
                                                lendOut : Math.floor(parseInt(lendOut*100)/100).toFixed(2),
                                                createdAtGMT : eachAgent.createdAt,
                                                collected : Math.floor(parseInt(collected*100)/100).toFixed(2),
                                                expenses : Math.floor(parseInt(expenses*100)/100).toFixed(2),
                                                bonus : Math.floor(parseInt(bonus*100)/100).toFixed(2),
                                                expensesAndBonus : Math.floor(parseInt(expensesAndBonus*100)/100).toFixed(2),
                                                earningLosses : Math.floor(parseInt((collected - outstanding)*100)/100).toFixed(2),
                                                principal : eachAgent.amountHold ? Math.floor(parseInt(eachAgent.amountHold*100)/100).toFixed(2) : 0.00,
                                                outstanding : Math.floor(parseInt(outstanding*100)/100).toFixed(2),
                                                status : eachAgent.status,
                                            })
                                            callback()
                                        })
                                    })
                                })
                            })
                        })
                    })
                },(err)=>{
                    agentList.sort((a,b)=>(b.name - a.name))
                    res.render('setting/manager/agentList',{
                        langData: langData, 
                        admin : req.session.admin,  
                        agentList : agentList,
                    })
                })
            })
        })
    })
})

app.get('/edit/:agentId', isLoggedIn, (req,res)=>{
    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    let agentId = req.params.agentId
    Language.find({type: {$in : ["header","setting","settingList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            Admin.findById(agentId,{
                name : 1, 
                email : 1, 
                contactNumber : 1, 
                status : 1,
                ic : 1,
                bankDetail : 1,
                amountHold : 1,
                referralCode  : 1,
                level : 1,
                frontICUrl : 1,
                backICUrl : 1,
            }).exec((err,agentDetails)=>{
                agentDetails.agentId = agentId
                agentDetails.frontICUrlValue = req.headers.host + agentDetails.frontICUrl.value
                agentDetails.frontICUrl.value = req.headers.host + agentDetails.frontICUrl.value
                console.log(agentDetails)
                res.render('setting/manager/editAgent',{
                    langData : langData,
                    admin : req.session.admin, 
                    agentDetails : agentDetails
                })
            })
        })
    })
})

app.get('/addAgent', isLoggedIn, (req,res)=>{
    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    let agentId = req.params.agentId
    Language.find({type: {$in : ["header","setting","settingList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            res.render('setting/manager/addAgent',{
                langData: langData, 
                admin : req.session.admin,  
            })
        })
    })
})

app.get('/deleteAgent/:agentId', isLoggedIn, (req,res)=>{
    let agentId = req.params.agentId
    Admin.findByIdAndDelete(agentId).exec((err,done)=>{
        res.redirect('/manager/settingAgent?deleteAgent=success')
    })
})

// app.post('/editManagerAccess', isLoggedIn, (req,res)=>{
//     let managerId = req.body.managerId
//     let reqBody = {
//         name : req.body.name,
//         email : req.body.email,
//         contactNumber : req.body.contactNumber,
//         status : req.body.status,
//         adminAccess : {
//             viewAllCustomerInfo : req.body.viewAllCustomerInfo && req.body.viewAllCustomerInfo == "true" ? true : false,
//             editCustomerInfo : req.body.editCustomerInfo && req.body.editCustomerInfo == "true" ? true : false,
//             loanApplicationApproval : req.body.loanApplicationApproval && req.body.loanApplicationApproval == "true" ? true : false,
//             addLoan : req.body.addLoan && req.body.addLoan == "true" ? true : false,
//             disbursement : req.body.disbursement && req.body.disbursement == "true" ? true : false,
//             debtCollection : req.body.debtCollection && req.body.debtCollection == "true" ? true : false,
//             releaseCommission : req.body.releaseCommission && req.body.releaseCommission == "true" ? true : false,
//             blacklistRequest : req.body.blacklistRequest && req.body.blacklistRequest == "true" ? true : false,
//             payExpenses : req.body.payExpenses && req.body.payExpenses == "true" ? true : false,
//             bankedInBonusToAgent : req.body.bankedInBonusToAgent && req.body.bankedInBonusToAgent == "true" ? true : false,
//         }
//     }
//     Admin.findByIdAndUpdate(managerId,reqBody).exec((err,managerAccessUpdated)=>{
//         if(!err && managerAccessUpdated){
//             res.redirect('/manager/settingManager?updateManagerAccess=success')
//         }else{
//             res.redirect('/manager/settingManager?updateManagerAccess=failed')
//         }
//     })
// })

// app.get('/addManager', isLoggedIn, (req,res)=>{
//     let lang = req.session.lang ? req.session.lang : "en"
//     let langData = {}
//     Language.find({type: {$in : ["header","setting","settingList"]}}).exec((err,allLang)=>{
//         async.each(allLang,(eachLang,callback)=>{
//             langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
//             callback()
//         },(err)=>{
//             res.render('setting/manager/addManager',{
//                 langData: langData, 
//                 admin : req.session.admin,  
//             })
//         })
//     })
// })

// app.post('/addManager', isLoggedIn, (req,res)=>{
//     let plainPassword = Math.random().toString(36).substr(2, 8)
//     let saltRounds = 10;
//     let salt = bcrypt.genSaltSync(saltRounds);
//     let hash = bcrypt.hashSync(plainPassword, salt);

//     let newAdmin = new Admin({
//         name : req.body.name,
//         email : req.body.email,
//         contactNumber : req.body.contactNumber,
//         status : req.body.status,
//         level : "manager",
//         password : hash,
//         forceUpdatePassword : true,
//         adminAccess : {
//             viewAllCustomerInfo : req.body.viewAllCustomerInfo && req.body.viewAllCustomerInfo == "true" ? true : false,
//             editCustomerInfo : req.body.editCustomerInfo && req.body.editCustomerInfo == "true" ? true : false,
//             loanApplicationApproval : req.body.loanApplicationApproval && req.body.loanApplicationApproval == "true" ? true : false,
//             addLoan : req.body.addLoan && req.body.addLoan == "true" ? true : false,
//             disbursement : req.body.disbursement && req.body.disbursement == "true" ? true : false,
//             debtCollection : req.body.debtCollection && req.body.debtCollection == "true" ? true : false,
//             releaseCommission : req.body.releaseCommission && req.body.releaseCommission == "true" ? true : false,
//             blacklistRequest : req.body.blacklistRequest && req.body.blacklistRequest == "true" ? true : false,
//             payExpenses : req.body.payExpenses && req.body.payExpenses == "true" ? true : false,
//             bankedInBonusToAgent : req.body.bankedInBonusToAgent && req.body.bankedInBonusToAgent == "true" ? true : false,
//         }
//     })
//     console.log(plainPassword)
//     // res.json(newAdmin)
//     newAdmin.save((err,newAdminSaved)=>{
//         if(!err && newAdminSaved){
//             res.redirect('/manager/settingManager?newAdminSaved=success')
//         }else{
//             res.redirect('/manager/settingManager?newAdminSaved=failed')
//         }
//     })
// })

// app.get('/deleteManager/:managerId', isLoggedIn, (req,res)=>{
//     let managerId = req.params.managerId
//     Admin.findById(managerId).exec((err,done)=>{
//         res.redirect('/manager/settingManager?deleteManager=success')
//     })
// })

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}