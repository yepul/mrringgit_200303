const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/',isLoggedIn,(req, res) => { 
    let lang = req.session.lang ? req.session.lang : "en"
	res.render('setting/agent/changeLanguage',{admin : req.session.admin, lang: lang, successUpdateLang : false});
})

app.get('/:lang',isLoggedIn,(req, res) => {
    let lang = req.params.lang
    req.session.lang = lang
	res.render('setting/agent/changeLanguage',{admin : req.session.admin, lang: lang, successUpdateLang : true});
})


module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}