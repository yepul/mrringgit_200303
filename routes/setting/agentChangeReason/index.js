const express = require('express');
const async = require('async')
const bcrypt = require('bcrypt');
const multiparty = require('connect-multiparty');

let Admin = require('../../../models/admin/admin');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/',isLoggedIn,(req, res) => {
	res.render('setting/agent/changeReason',{admin : req.session.admin/* , updatePasswordSuccess:false, updatePasswordFail:false */});
})

app.get('/:reasonType',isLoggedIn,(req, res) => {
    let reasonType = req.params.reasonType
    let reasons = []
    let reasonTypeFull = ""
    switch(reasonType){
        case "beingBlacklisted":
            reasonTypeFull = "Reasons of being blacklisted";
            break;
        case "customerProfileDetailsRejection":
            reasonTypeFull = "Reason of customer profile detail rejection";
            break;
        case "uploadPhotoRejected":
            reasonTypeFull = "Reason of upload photo rejection";
            break;
        case "loanApplicationRejected":
            reasonTypeFull = "Reason of loan application rejection";
            break;
        case "paymentRejection":
            reasonTypeFull = "Reasons of payment rejection";
            break;
        case "remainderUpComingPaymentDueDate":
            reasonTypeFull = "Remainder of up coming payment due date";
            break;
        case "remainderOverdue":
            reasonTypeFull = "Remainder overdue";
            break;
        default:
            reasonTypeFull = reasonType
    }

    Admin.findOne({name : req.session.admin.name}).exec((err,admin)=>{
        if(admin && admin.reason != []){
            async.each(admin.reason,(eachReason,callback)=>{
                if(eachReason.type == reasonType){
                    reasons.push(eachReason)
                    callback()
                }else{
                    callback()
                }
            },(err)=>{
                // res.json({admin : req.session.admin, reasonType: reasonTypeFull, reasons: reasons})
                res.render('setting/agent/changeReasonByType',{admin : req.session.admin, reasonType: reasonTypeFull, reasons: reasons/* , updatePasswordSuccess:false, updatePasswordFail:false */});
            })
        }else{
            // res.json({admin : req.session.admin, reasonType: reasonTypeFull, reasons: reasons})
            res.render('setting/agent/changeReasonByType',{admin : req.session.admin, reasonType: reasonTypeFull, reasons: reasons/* , updatePasswordSuccess:false, updatePasswordFail:false */});
        }
    })
})

app.post('/', isLoggedIn, (req, res) => {
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}