const express = require('express');
const async = require('async')
const bcrypt = require('bcrypt');
const multiparty = require('connect-multiparty');

let Admin = require('../../../models/admin/admin');
let Language = require('../../../models/language/language');
let LoanDetail = require('../../../models/loanDetail/loanDetail');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/',isLoggedIn,(req, res) => {
    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type: {$in : ["header","setting","settingList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            LoanDetail.findOne({status:"active"}).exec((err,loanDetails)=>{
                res.render('setting/manager/loanDetails',{
                    langData: langData, 
                    admin : req.session.admin,  
                    loanDetails : loanDetails,
                    updateLoanDetailsSuccess : req.query.updateLoanDetails && req.query.updateLoanDetails == "success" ? true : false,
                    updateLoanDetailsFailed : req.query.updateLoanDetails && req.query.updateLoanDetails == "failed" ? true : false,
                })
            })
        })
    })
})

app.post('/', isLoggedIn, (req, res) => {
    let reqBody = req.body
    LoanDetail.findOneAndUpdate({status:"active"},reqBody).exec((err,done)=>{
        if(!err && done){
            res.redirect('/manager/settingLoanDetails?updateLoanDetails=success')
        }else{
            res.redirect('/manager/settingLoanDetails?updateLoanDetails=failed')
        }
    })
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}