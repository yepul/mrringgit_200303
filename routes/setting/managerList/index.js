const express = require('express');
const async = require('async')
const bcrypt = require('bcrypt');
const moment = require('moment');
const multiparty = require('connect-multiparty');
const shortid = require('shortid');
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

let Admin = require('../../../models/admin/admin');
let Language = require('../../../models/language/language');
let LoanDetail = require('../../../models/loanDetail/loanDetail');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/',isLoggedIn,(req, res) => {
    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    let managerList = []
    let adminQuery = {level: "manager"}
	req.query.searchByUsername ? adminQuery.name = req.query.searchByUsername : ""
    Language.find({type: {$in : ["header","setting","settingList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            Admin.find(adminQuery).exec((err,managerAll)=>{
                async.each(managerAll,(eachManager,callback)=>{
                    managerList.push({
                        managerId : eachManager._id,
                        name : eachManager.name,
                        email : eachManager.email,
						createdAt : moment(eachManager.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
                        createdAtGMT : eachManager.createdAt,
                        status : eachManager.status,
                    })
                    callback()
                },(err)=>{
                    res.render('setting/manager/managerList',{
                        langData: langData, 
                        admin : req.session.admin,  
                        managerList : managerList,
                    })
                })
            })
        })
    })
})

app.get('/editAccess/:managerId', isLoggedIn, (req,res)=>{
    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    let managerId = req.params.managerId
    Language.find({type: {$in : ["header","setting","settingList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            Admin.findById(managerId,{name:1, email:1, contactNumber:1, status:1, adminAccess: 1}).exec((err,managerDetails)=>{
                managerDetails.managerId = managerId
                res.render('setting/manager/editManagerAccess',{
                    langData : langData,
                    admin : req.session.admin, 
                    managerDetails : managerDetails
                })
            })
        })
    })
})

app.post('/editManagerAccess', isLoggedIn, (req,res)=>{
    let managerId = req.body.managerId
    let reqBody = {
        name : req.body.name,
        email : req.body.email,
        contactNumber : req.body.contactNumber,
        status : req.body.status,
        adminAccess : {
            viewAllCustomerInfo : req.body.viewAllCustomerInfo && req.body.viewAllCustomerInfo == "true" ? true : false,
            editCustomerInfo : req.body.editCustomerInfo && req.body.editCustomerInfo == "true" ? true : false,
            loanApplicationApproval : req.body.loanApplicationApproval && req.body.loanApplicationApproval == "true" ? true : false,
            addLoan : req.body.addLoan && req.body.addLoan == "true" ? true : false,
            disbursement : req.body.disbursement && req.body.disbursement == "true" ? true : false,
            debtCollection : req.body.debtCollection && req.body.debtCollection == "true" ? true : false,
            releaseCommission : req.body.releaseCommission && req.body.releaseCommission == "true" ? true : false,
            blacklistRequest : req.body.blacklistRequest && req.body.blacklistRequest == "true" ? true : false,
            payExpenses : req.body.payExpenses && req.body.payExpenses == "true" ? true : false,
            bankedInBonusToAgent : req.body.bankedInBonusToAgent && req.body.bankedInBonusToAgent == "true" ? true : false,
        }
    }
    Admin.findByIdAndUpdate(managerId,reqBody).exec((err,managerAccessUpdated)=>{
        if(!err && managerAccessUpdated){
            res.redirect('/manager/settingManager?updateManagerAccess=success')
        }else{
            res.redirect('/manager/settingManager?updateManagerAccess=failed')
        }
    })
})

app.get('/addManager', isLoggedIn, (req,res)=>{
    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type: {$in : ["header","setting","settingList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            res.render('setting/manager/addManager',{
                langData: langData, 
                admin : req.session.admin,  
            })
        })
    })
})

app.post('/addManager', isLoggedIn, (req,res)=>{
    let plainPassword = "12345678"/* Math.random().toString(36).substr(2, 8) */
    let saltRounds = 10;
    let salt = bcrypt.genSaltSync(saltRounds);
    let hash = bcrypt.hashSync(plainPassword, salt);
    let newId = shortid.generate()

    let newAdmin = new Admin({
        name : req.body.name,
        email : req.body.email,
        contactNumber : req.body.contactNumber,
        status : req.body.status,
        level : "manager",
        password : hash,
        forceUpdatePassword : true,
        referralCode : newId,
        adminAccess : {
            viewAllCustomerInfo : req.body.viewAllCustomerInfo && req.body.viewAllCustomerInfo == "true" ? true : false,
            editCustomerInfo : req.body.editCustomerInfo && req.body.editCustomerInfo == "true" ? true : false,
            loanApplicationApproval : req.body.loanApplicationApproval && req.body.loanApplicationApproval == "true" ? true : false,
            addLoan : req.body.addLoan && req.body.addLoan == "true" ? true : false,
            disbursement : req.body.disbursement && req.body.disbursement == "true" ? true : false,
            debtCollection : req.body.debtCollection && req.body.debtCollection == "true" ? true : false,
            releaseCommission : req.body.releaseCommission && req.body.releaseCommission == "true" ? true : false,
            blacklistRequest : req.body.blacklistRequest && req.body.blacklistRequest == "true" ? true : false,
            payExpenses : req.body.payExpenses && req.body.payExpenses == "true" ? true : false,
            bankedInBonusToAgent : req.body.bankedInBonusToAgent && req.body.bankedInBonusToAgent == "true" ? true : false,
        }
    })
    console.log(plainPassword)
    // res.json(newAdmin)
    newAdmin.save((err,newAdminSaved)=>{
        if(!err && newAdminSaved){
            res.redirect('/manager/settingManager?newAdminSaved=success')
        }else{
            res.redirect('/manager/settingManager?newAdminSaved=failed')
        }
    })
})

app.get('/deleteManager/:managerId', isLoggedIn, (req,res)=>{
    let managerId = req.params.managerId
    Admin.findById(managerId).exec((err,done)=>{
        res.redirect('/manager/settingManager?deleteManager=success')
    })
})

// app.post('/', isLoggedIn, (req, res) => {
//     let reqBody = req.body
//     LoanDetail.findOneAndUpdate({status:"active"},reqBody).exec((err,done)=>{
//         if(!err && done){
//             res.redirect('/manager/settingLoanDetails?updateLoanDetails=success')
//         }else{
//             res.redirect('/manager/settingLoanDetails?updateLoanDetails=failed')
//         }
//     })
// })

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}