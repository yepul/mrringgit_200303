const express = require('express');
const async = require('async')
const bcrypt = require('bcrypt');
const multiparty = require('connect-multiparty');

let Admin = require('../../../models/admin/admin');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/',isLoggedIn,(req, res) => {
	res.render('setting/agent/changePassword',{admin : req.session.admin, updatePasswordSuccess:false, updatePasswordFail:false});
})

app.post('/', isLoggedIn, (req, res) => {
    let currentPassword = req.body.currentPassword
    let newPassword = req.body.newPassword
    let retypePassword = req.body.retypePassword

    if(currentPassword && (newPassword == retypePassword) ){
        Admin.findOne({ name: req.session.admin.name }).exec((err, admin) => {
            if(bcrypt.compareSync(currentPassword, admin.password)) {
                let plainPassword = newPassword 
                let saltRounds = 10;
                let salt = bcrypt.genSaltSync(saltRounds);
                let hash = bcrypt.hashSync(plainPassword, salt);

                Admin.findByIdAndUpdate(admin._id,{password : hash}).exec((err,updatePasswordSuccess)=>{
                    res.render('setting/agent/changePassword',{admin : req.session.admin, updatePasswordSuccess:true, updatePasswordFail:false});
                })
            }
        })
    }else{
        res.render('setting/agent/changePassword',{admin : req.session.admin, updatePasswordSuccess:false, updatePasswordFail:true});
    }
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}