// staging
let express = require('express');
let router = express.Router();
let multiparty = require('connect-multiparty');
let async = require('async');
let request = require('request');
let moment = require('moment');
const bcrypt = require('bcrypt');
const path = require('path');
const shortid = require('shortid');
shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@');

const upload = require('../../uploadMiddleware');
const Resize = require('../../Resize');

var multipartyMiddleware = multiparty();
router.use(multipartyMiddleware);

let User = require('../../models/user/user');
let Admin = require('../../models/admin/admin');
let Contact = require('../../models/contact/contact');
let Location = require('../../models/location/location');
let CallRecord = require('../../models/callRecord/callRecord');
let Loan = require('../../models/loan/loan');
let BankDetail = require('../../models/bankDetail/bankDetail');
let Payment = require('../../models/payment/payment');
let StaticContent = require('../../models/staticContent/staticContent');
let Language = require('../../models/language/language');
let Expenses = require('../../models/expenses/expenses');
let LoanDetail = require('../../models/loanDetail/loanDetail');

router.post('/login', function (req, res, next) {
    let phoneNumber = req.body.phoneNumber
    let password = req.body.password

    User.findOne({ phoneNumber: phoneNumber }).exec((err, userDetails) => {
        if (userDetails /* && bcrypt.compareSync(password, userDetails.password) */) {
            bcrypt.compare(password, userDetails.password, function (err, result) {
                if (result == true) {
                    res.json({
                        status: 200,
                        msg: "",
                        userId: userDetails._id,
                        userDetails: userDetails
                    })
                } else {
                    res.json({
                        status: 400,
                        msg: "Invalid phone number or password!"
                    })
                }
            })
        } else {
            res.json({
                status: 400,
                msg: "Invalid phone number or password!"
            })
        }
    })
})

router.get('/userDetails/:userId', function (req, res, next) {
    let userId = req.params.userId
    User.findById(userId).exec((err, userDetails) => {
        if (!err && userDetails) {
            if(userDetails.currentLoanId == ""){
                User.findByIdAndUpdate(userDetails._id,{status : "register"}).exec((err,newUserDetails)=>{
                    if(!err && newUserDetails){
                        res.json({
                            status: 200,
                            currentLoanId: userDetails.currentLoanId ? userDetails.currentLoanId : "",
                            details : newUserDetails
                        })
                    } else {
                        res.json({ status: 400 })
                    }
                })
            } else{
                res.json({
                    status: 200,
                    currentLoanId: userDetails.currentLoanId ? userDetails.currentLoanId : "",
                    details : userDetails
                })
            }
        } else {
            res.json({ status: 400 })
        }
    })
})

router.get('/detail/:phoneNumber', function (req, res, next) {
    User.find({ phoneNumber: req.params.phoneNumber }).exec((err, user) => {
        res.json(user)
    })
})

router.post('/registerPhoneNumber', function (req, res, next) {
    if (!req.body.phoneNumber) {
        res.json({ status: 400 })
    } else {
        if (req.body.phoneNumber.substring(0, 2) != "01") {
            res.json({ status: 400 });
        } else {
            var randomCode = 999999;
            User.findOne({ phoneNumber: req.body.phoneNumber }).exec((err, user) => {
                if (!user) {
                    let newId = shortid.generate()
                    let newUser = new User({
                        phoneNumber: req.body.phoneNumber,
                        status: "new",
                        referralCode : newId,
                        verificationCode: randomCode,
                    })
                    newUser.save((err, done) => {
                        if (!err && done) {
                            /* sms setup here */
                            res.json({ status: 200 })
                        } else {
                            res.json({ status: 400 })
                        }
                    })
                } else {
                    res.json({ status: 300, msg: "Phone number already existed!" })
                }
            })
        }
    }
})

router.post('/userVerification', function (req, res, next) {
    let reqData = {
        phoneNumber: req.body.phoneNumber,
        verificationCode: req.body.verificationCode,
        password: req.body.password,
        referralCode: (req.body.referralCode ? req.body.referralCode : "")
    }
    User.findOne({referralCode : reqData.referralCode},{agent:1,agentId:1}).exec((err,referBy)=>{
        let agent = referBy && referBy.agent ? referBy.agent : "" 
        let agentId = referBy && referBy.agentId ? referBy.agentId : "" 

        User.findOne({ phoneNumber: reqData.phoneNumber, status: "new", verificationCode: reqData.verificationCode }).exec((err, user) => {
            if (err) {
                res.json({ status: 400 })
            } else {
                let plainPassword = reqData.password
                let saltRounds = 10;
                let salt = bcrypt.genSaltSync(saltRounds);
                let hashPassword = bcrypt.hashSync(plainPassword, salt);
    
                User.findOneAndUpdate({ phoneNumber: reqData.phoneNumber }, {
                    status: "register",
                    password: hashPassword,
                    referBy: reqData.referralCode,
                    agent : agent,
                    agentId : agentId,
                }).exec((err, done) => {
                    if (!err && done) {
                        res.json({ status: 200, userId: user._id })
                    } else {
                        res.json({ status: 400 })
                    }
                })
            }
        })
    })
})

router.post('/userPersonalInfo', function (req, res, next) {
    let reqBody = {
        userId: req.body.userId,
        username: req.body.username,
        ic: req.body.ic,
        race: req.body.race,
        gender: req.body.gender,
        education: req.body.education,
        marriage: req.body.marriage,
    }

    User.findById(reqBody.userId,{status:1}).exec((err,userCurrentStatus)=>{
        if(!err && userCurrentStatus && userCurrentStatus.status == "register"){
            User.findByIdAndUpdate(reqBody.userId, {
                status: "personalInfo",
                username: reqBody.username,
                ic: reqBody.ic,
                race: reqBody.race,
                gender: reqBody.gender,
                education: reqBody.education,
                marriage: reqBody.marriage
            }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }else if(!err && userCurrentStatus && userCurrentStatus.status != "register"){
            User.findByIdAndUpdate(reqBody.userId, {
                username: reqBody.username,
                ic: reqBody.ic,
                race: reqBody.race,
                gender: reqBody.gender,
                education: reqBody.education,
                marriage: reqBody.marriage
            }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }else{
            res.json({ status: 400 })
        }
    })
})

router.post('/userResidentialInfo', function (req, res, next) {
    let reqBody = {
        userId: req.body.userId,
        residentialType: req.body.residentialType,
        address: req.body.address,
        postcode: req.body.postcode,
        city: req.body.city,
        state: req.body.state,
    }

    User.findById(reqBody.userId,{status:1}).exec((err,userCurrentStatus)=>{
        if(!err && userCurrentStatus && userCurrentStatus.status == "personalInfo"){
            User.findByIdAndUpdate(reqBody.userId, {
                status: "residentialInfo",
                residentialType: reqBody.residentialType,
                address: reqBody.address,
                postcode: reqBody.postcode,
                city: reqBody.city,
                state: reqBody.state,
            }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }else{
            User.findByIdAndUpdate(reqBody.userId, {
                residentialType: reqBody.residentialType,
                address: reqBody.address,
                postcode: reqBody.postcode,
                city: reqBody.city,
                state: reqBody.state,
            }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }
    })
})

router.post('/userBankInfo', function (req, res, next) {
    let reqBody = {
        userId: req.body.userId,
        bankName: req.body.bankName,
        bankAccountName: req.body.bankAccountName,
        bankAccountNumber: req.body.bankAccountNumber,
    }

    User.findById(reqBody.userId,{status:1}).exec((err,userCurrentStatus)=>{
        if(!err && userCurrentStatus && userCurrentStatus.status == "residentialInfo"){
            User.findByIdAndUpdate(reqBody.userId, {
                status: "bankInfo",
                bankName: reqBody.bankName,
                bankAccountName: reqBody.bankAccountName,
                bankAccountNumber: reqBody.bankAccountNumber,
            }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }else{
            User.findByIdAndUpdate(reqBody.userId, {
                bankName: reqBody.bankName,
                bankAccountName: reqBody.bankAccountName,
                bankAccountNumber: reqBody.bankAccountNumber,
            }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }
    })
})

router.post('/userWorkingInfo', function (req, res, next) {
    let reqBody = {
        userId: req.body.userId,
        companyName: req.body.companyName,
        position: req.body.position,
        monthlyIncome: req.body.monthlyIncome,
        companyContactNumber: req.body.companyContactNumber,
        companyAddress: req.body.companyAddress,
        companyPostcode: req.body.companyPostcode,
        companyCity: req.body.companyCity,
        companyState: req.body.companyState,
    }
    User.findById(reqBody.userId,{status:1}).exec((err,userCurrentStatus)=>{
        if(!err && userCurrentStatus && userCurrentStatus.status == "bankInfo"){
            User.findByIdAndUpdate(reqBody.userId, {
                status: "workingInfo",
                companyName: reqBody.companyName,
                position: reqBody.position,
                monthlyIncome: reqBody.monthlyIncome,
                companyContactNumber: reqBody.companyContactNumber,
                companyAddress: reqBody.companyAddress,
                companyPostcode: reqBody.companyPostcode,
                companyCity: reqBody.companyCity,
                companyState: reqBody.companyState,
            }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }else if(!err && userCurrentStatus && userCurrentStatus.status != "bankInfo"){
            User.findByIdAndUpdate(reqBody.userId, {
                companyName: reqBody.companyName,
                position: reqBody.position,
                monthlyIncome: reqBody.monthlyIncome,
                companyContactNumber: reqBody.companyContactNumber,
                companyAddress: reqBody.companyAddress,
                companyPostcode: reqBody.companyPostcode,
                companyCity: reqBody.companyCity,
                companyState: reqBody.companyState,
            }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }else {
            res.json({ status: 400 })
        }
    })

})

router.post('/userCompanyInfo', function (req, res, next) {
    let reqBody = {
        userId: req.body.userId,
        companyName: req.body.companyName,
        companyWebsite: req.body.companyWebsite,
        companyContactNumber: req.body.companyContactNumber,
        companyAddress: req.body.companyAddress,
        companyPostcode: req.body.companyPostcode,
        companyCity: req.body.companyCity,
        companyState: req.body.companyState,
    }
    User.findById(reqBody.userId,{status:1}).exec((err,userCurrentStatus)=>{
        if(!err && userCurrentStatus && userCurrentStatus.status == "bankInfo"){
            User.findByIdAndUpdate(reqBody.userId, {
                status: "companyInfo",
                companyName: reqBody.companyName,
                companyWebsite: reqBody.companyWebsite,
                companyContactNumber: reqBody.companyContactNumber,
                companyAddress: reqBody.companyAddress,
                companyPostcode: reqBody.companyPostcode,
                companyCity: reqBody.companyCity,
                companyState: reqBody.companyState,
            }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }else if(!err && userCurrentStatus && userCurrentStatus.status != "bankInfo"){
            User.findByIdAndUpdate(reqBody.userId, {
                companyName: reqBody.companyName,
                companyWebsite: reqBody.companyWebsite,
                companyContactNumber: reqBody.companyContactNumber,
                companyAddress: reqBody.companyAddress,
                companyPostcode: reqBody.companyPostcode,
                companyCity: reqBody.companyCity,
                companyState: reqBody.companyState,
            }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        } else {
            res.json({ status: 400 })
        }
    })

})

router.post('/userEmergencyContact', function (req, res, next) {
    let userId = req.body.userId
    let reqBody = [
        {
            relationship: req.body.relationship1,
            name: req.body.name1,
            contactNumber: req.body.contactNumber1,
        },
        {
            relationship: req.body.relationship2,
            name: req.body.name2,
            contactNumber: req.body.contactNumber2,
        }
    ]
    
    User.findById(userId,{status:1}).exec((err,userCurrentStatus)=>{
        if(!err && userCurrentStatus && (userCurrentStatus.status == "workingInfo" || userCurrentStatus.status == "companyInfo")){
            User.findByIdAndUpdate(userId, { status: "emergencyInfo", emergencyContact: reqBody }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }else{
            User.findByIdAndUpdate(userId, { emergencyContact: reqBody }).exec((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        }
    })
})

router.post('/uploadContact', function (req, res, next) {
    let userId = req.body.userId
    let allContact = req.body.allContact

    async.each(allContact, (eachContact, callback) => {
        let newContact = new Contact({
            userId: userId,
            name: eachContact.name,
            number: eachContact.number
        })
        newContact.save((err, done) => {
            if (!err && done) {
                callback()
            } else {
                console.log(err)
                callback()
            }
        })
    }, (err) => {
        res.json({ status: 200 })
    })
})

router.post('/uploadLocation', function (req, res, next) {
    let userId = req.body.userId
    let latitude = req.body.latitude
    let longitude = req.body.longitude

    let newLocation = new Location({
        userId: userId,
        latitude: latitude,
        longitude: longitude
    })

    newLocation.save((err, done) => {
        if (!err && done) {
            res.json({ status: 200 })
        } else {
            res.json({ status: 400 })
        }
    })
})

router.post('/uploadCallRecord', function (req, res, next) {
    let userId = req.body.userId
    let allCallRecord = req.body.allCallRecord

    async.each(allCallRecord, (eachCallRecord, callback) => {
        let newCallRecord = new CallRecord({
            userId: userId,
            name: eachCallRecord.name,
            number: (eachCallRecord.number).substring(0, 1) == "1" ? "0" + eachCallRecord.number : eachCallRecord.number,
            type: (eachCallRecord.type).indexOf(".") ? (eachCallRecord.type).substring((eachCallRecord.type).indexOf(".") + 1) : eachCallRecord.type,
            duration: eachCallRecord.duration, //seconds
            date: eachCallRecord.date,
        })
        newCallRecord.save((err, done) => {
            if (!err && done) {
                callback()
            } else {
                console.log(err)
                callback()
            }
        })
    }, (err) => {
        res.json({ status: 200 })
    })
})

router.post('/applyLoan', function (req, res, next) {
    let reqBody = {
        userId: req.body.userId,
        loanType: req.body.loanType,
        amount: req.body.amount,
        paymentType: req.body.paymentType,
        duration: req.body.duration,
        interestRate: req.body.interestRate,
        offset: 0,
        // agentId: req.body.agentId,
    }

    User.findById(reqBody.userId,{agent:1,agentId:1}).exec((err,agentDetails)=>{
        let agent = agentDetails && agentDetails.agent ? agentDetails.agent : ""
        let agentId = agentDetails && agentDetails.agentId ? agentDetails.agentId : ""

        LoanDetail.findOne({status:"active"}).exec((err,loanApplicationSetup)=>{
            let adminFeePercentage = (reqBody.loanType == "personal" ? loanApplicationSetup.adminFeePersonal : loanApplicationSetup.adminFeeCompany)
            Loan.find({ userId: reqBody.userId, currentLoanId : ""/* status: { $in: ["pending","new", "accepted"] } */ }).exec((err, loanExist) => {
                if (loanExist.length == 0) {
                    let newLoan = new Loan({
                        userId: reqBody.userId,
                        loanType: reqBody.loanType,
                        status: "pending",
                        amount: parseFloat(reqBody.amount).toFixed(2),
                        principalAmount: parseFloat(Number(reqBody.amount) + Number(reqBody.amount * reqBody.interestRate * reqBody.duration / 100)).toFixed(2),
                        outstanding: parseFloat(Number(reqBody.amount) + Number(reqBody.amount * reqBody.interestRate * reqBody.duration / 100)).toFixed(2),
                        paymentType: reqBody.paymentType,
                        duration: reqBody.duration,
                        interestRate: reqBody.interestRate,
                        interestRateTotal: reqBody.interestRate * reqBody.duration,
                        interest: 0, //parseFloat(reqBody.amount * reqBody.interestRate * reqBody.duration / 100).toFixed(2)
                        disbursement: parseFloat(reqBody.amount * (100 - adminFeePercentage) / 100).toFixed(2),
                        processingFee: parseFloat(reqBody.amount * adminFeePercentage / 100).toFixed(2),
                        installment: parseFloat((reqBody.amount) * (100 + ((reqBody.duration) * (reqBody.interestRate))) / 100 / (reqBody.duration)).toFixed(2),
                        agentId: agentId,
                        agent : agent,
        
                        earningsAndLosses: -1 * parseFloat(reqBody.amount * (100 - adminFeePercentage) / 100).toFixed(2)
                    })
        
                    newLoan.save((err, done) => {
                        if (!err && done) {
                            User.findOneAndUpdate({ _id: reqBody.userId }, { currentLoanId: done._id, agent : agent, agentId : agentId }).exec((err, complete) => {
                                if (!err && complete) {
                                    res.json({ status: 200, loanId: done._id })
                                } else {
                                    res.json({ status: 400 })
                                }
                            })
                        } else {
                            res.json({ status: 400 })
                        }
                    })
                } else {
                    res.json({ status: 400 })
                }
            })
        })
    })

})

router.get('/loanDetails/:loanId', function (req, res, next) {
    let loanId = req.params.loanId

    Loan.findById(loanId).exec((err, loanDetails) => {
        if (!err && loanDetails) {
            res.json({
                status: 200,
                msg: "",
                loanId: loanDetails._id,
                loanType: loanDetails.loanType,
                requestedOn: loanDetails.requestedOn,
                amount: loanDetails.amount,
                paymentType: loanDetails.paymentType,
                duration: loanDetails.duration,
                installment: loanDetails.installment,
                outstanding: loanDetails.outstanding,
                processingFee: loanDetails.processingFee,
                disbursement: loanDetails.disbursement
            })
        } else {
            res.json({ status: 400 })
        }
    })
})

router.post('/confirmLoan', function (req, res, next) {
    let userId = req.body.userId
    let loanId = req.body.loanId

    Loan.findOneAndUpdate({ _id: loanId, userId: userId, status: "pending" }, { status: "new" }).exec((err, doneUpdateLoan) => {
        if (!err && doneUpdateLoan) {
            User.findByIdAndUpdate(userId,{status:"loanApplication"}).exec((err,done)=>{
                if(!err && done){
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })
        } else {
            res.json({ status: 400 })
        }
    })

})

router.get('/loanStatus/:loanId', function (req, res, next) {
    let loanId = req.params.loanId

    Loan.findById(loanId).exec((err, loanDetails) => {
        if (!err && loanDetails) {
            User.findById(loanDetails.userId).exec((err, userDetails) => {
                if (!err && userDetails) {
                    res.json({
                        status: 200,
                        loanId: loanId,
                        loanStatus: loanDetails.status == "accepted" ? "accepted" : loanDetails.status == "rejected" ? "rejected" : "processing",
                        loanType: loanDetails.loanType,
                        requestedOn: loanDetails.requestedOn,
                        paymentType: loanDetails.paymentType,
                        duration: loanDetails.duration,
                        amount: loanDetails.amount,
                        outstanding: loanDetails.outstanding,
                        installment: loanDetails.installment,
                        processingFee: loanDetails.processingFee,
                        disbursement: loanDetails.disbursement
                    })
                } else {
                    res.json({ status: 400 })
                }
            })
        } else {
            res.json({ status: 400 })
        }
    })
})

router.get('/bankDetails', function (req, res, next) {
    BankDetail.findOne({ status: "active" }).exec((err, bankDetail) => {
        if (!err && bankDetail) {
            res.json({
                status: 200,
                accountName: bankDetail.accountName,
                bankName: bankDetail.bankName,
                accountNumber: bankDetail.accountNumber,
            })
        }
    })
})

router.post('/uploadPayment', function (req, res, next) {
    let userId = req.body.userId
    let loanId = req.body.loanId
    let amount = req.body.amount
    let imageUrl = req.body.imageUrl

    Loan.findOne({ _id: loanId, userId: userId, status: "accepted" }/* ,{} */).exec((err, loanDetails) => {
        if (!err && loanDetails) {
            let newPayment = new Payment({
                userId: userId,
                loanId: loanId,
                createdAt : moment().toISOString(),
                status: "pending",
                amount: parseFloat(amount).toFixed(2),
                imageUrl: imageUrl,
                paymentNumber: loanDetails.paymentNumber + 1,
            })
            newPayment.save((err, done) => {
                if (!err && done) {
                    res.json({ status: 200 })
                } else {
                    res.json({ status: 400 })
                }
            })

        } else {
            res.json({ status: 400 })
        }
    })
})

router.get('/paymentHistory/:loanId', function(req,res,next){
    let loanId = req.params.loanId
    Loan.findOne({_id : loanId, status : "accepted"}).exec((err,loanDetails)=>{
        if(!err && loanDetails){
            Payment.find({loanId:loanId}).exec((err,paymentHistory)=>{
                let paidAmount = 0
                // let finalPaymentHistory = []
                async.each(paymentHistory,(eachPayment,callback)=>{
                    if(eachPayment.status == "accepted"){
                        paidAmount += eachPayment.amount
                        callback()
                    }else{
                        callback()
                    }
                },(err)=>{
                    res.json({
                        status : 200,
                        loanId : loanId,
                        paidAmount : paidAmount,
                        outstanding : loanDetails.outstanding,
                        installment : loanDetails.installment,
                        nextPayment : loanDetails.nextPayment ? loanDetails.nextPayment : moment().toISOString(),
                        paymentHistory : paymentHistory 
                    })
                })
            })
        }else{
            res.json({status : 400})
        }
    })
})

router.post('/changeLoanType', function(req,res,next){
    let loanType = req.body.loanType
    let loanId = req.body.loanId
    Loan.findByIdAndUpdate(loanId,{loanType : loanType}).exec((err, done)=>{
        if(!err && done){
            res.json({status : 200})
        }else{
            res.json({status : 400})
        }
    })
})

router.post('/updateOneSignal',(req,res)=>{
    let userId = req.body.userId
    let oneSignalId = req.body.oneSignalId

    User.findByIdAndUpdate(userId,{oneSignalId : oneSignalId}).exec((err,done)=>{
        if(!err && done){
            res.json({status : 200})
        }else{
            res.json({status : 400})
        }
    })
})

router.get('/loanApplicationSetup',(req,res)=>{
    LoanDetail.findOne({status:"active"}).exec((err,loanApplicationSetup)=>{
        if(!err && loanApplicationSetup){
            res.json({
                status : 200,
                setup : loanApplicationSetup
            })
        }else{
            res.json({status:400})
        }
    })
})

router.get('/referEarnData/:userId',(req,res)=>{
    let userId = req.params.userId
    let totalWithdrawal = 0
    let numberRenewed = 0
    User.findById(userId,{username:1,referralCode:1,commissionBalance:1}).exec((err,userDetails)=>{
        User.countDocuments({
            referBy : userId, 
            status: { 
                $in: [
                    "register", 
                    "personalInfo",
                    "residentialInfo",
                    "bankInfo",
                    "workingInfo",
                    "companyInfo",
                    "emergencyInfo",
                ] 
            }
        }).exec((err,numberSignedUp)=>{
            User.countDocuments({
                referBy : userId, 
                status: "loanApplication"
            }).exec((err,numberApplied)=>{
                Expenses.find({
                    status : "paid", 
                    userId : userId,
                    type : "userWithdrawCommission"
                },{expenses:1}).exec((err,expensesAll)=>{
                    async.each(expensesAll,(eachExpenses,callback)=>{
                        totalWithdrawal += eachExpenses.expenses
                        callback()
                    },(err)=>{
                        res.json({
                            status : 200,
                            referralCode : userDetails.referralCode,
                            share : "Friends! Check out this awesome licensed loaning app! Don`t forget to register with my referral code " + userDetails.referralCode + ".",
                            numberSignedUp : numberSignedUp,
                            numberApplied : numberApplied,
                            numberRenewed : numberRenewed,
                            commissionBalance : parseFloat(userDetails.commissionBalance).toFixed(2),
                            totalWithdrawal : parseFloat(totalWithdrawal).toFixed(2),
                        })
                    })
                })
            })
        })
    })
})

router.get('/commissionWithdrawalHistory/:userId',(req,res)=>{
    let userId = req.params.userId
    let withdrawalHistory = []

    Expenses.find({userId : userId, type : "userWithdrawCommission"}).exec((err,withdrawalHistoryAll)=>{
        if(!err && withdrawalHistoryAll){
            async.each(withdrawalHistoryAll,(eachWithdrawalHistory,callback)=>{
                withdrawalHistory.push({
                    createdAt : eachWithdrawalHistory.createdAt,
                    WithdrawalId : eachWithdrawalHistory._id,
                    amount : eachWithdrawalHistory.expenses,
                    status : eachWithdrawalHistory.status == "paid" ?
                                 "Approved" : eachWithdrawalHistory.status == "rejected" ?
                                 "Rejected" : "Pending",  
                    statusColour : eachWithdrawalHistory.status == "paid" ?
                                 "green" : eachWithdrawalHistory.status == "rejected" ?
                                 "red" : "yellow",  
                })
                callback()
            },(err)=>{
                withdrawalHistory.sort((a,b)=>{
                    return moment(b.createdAt).unix() - moment(a.createdAt).unix()
                })
                res.json({
                    status : 200,
                    withdrawalHistory : withdrawalHistory
                })
            })
        }else{
            res.json({
                status : 400,
                withdrawalHistory : withdrawalHistory
            })
        }
    })
})

router.get('/referSignedUpList/:userId',(req,res)=>{
    let userId = req.params.userId
    let signedUpList = []
    User.find({
        referBy : userId, 
        status: { 
            $in: [
                "register", 
                "personalInfo",
                "residentialInfo",
                "bankInfo",
                "workingInfo",
                "companyInfo",
                "emergencyInfo",
            ] 
        }},{createdAt : 1 , status : 1}).exec((err,signedUpListAll)=>{
            if(!err && signedUpListAll){
                async.each(signedUpListAll,(eachSignedUpList,callback)=>{
                    let profileCompletion = ""
                    switch(eachSignedUpList.status){
                        case "register":
                            profileCompletion = "0%"
                            break;
                        case "personalInfo":
                            profileCompletion = "20%"
                            break; 
                        case "residentialInfo":
                            profileCompletion = "40%"
                            break; 
                        case "bankInfo":
                            profileCompletion = "60%"
                            break; 
                        case "workingInfo":
                            profileCompletion = "80%"
                            break; 
                        case "companyInfo":
                            profileCompletion = "80%"
                            break; 
                        case "emergencyInfo":
                            profileCompletion = "80%"
                            break; 
                        default:
                            profileCompletion = "0%" 
                    }

                    signedUpList.push({
                        createdAt : eachSignedUpList.createdAt,
                        contactId : eachSignedUpList._id,
                        profileCompletion : profileCompletion,                
                    })
                    callback()
                },(err)=>{
                    res.json({
                        status : 200,
                        signedUpList : signedUpList
                    })
                })
            }else{
                res.json({
                    status : 400,
                    signedUpList : signedUpList
                })
            }
        })

})

router.get('/referAppliedLoanList/:userId',(req,res)=>{
    let userId = req.params.userId
    let appliedLoanList = []

    User.find({referBy : userId,},{_id:1}).exec((err,referList)=>{
        async.each(referList,(eachReferList,callback)=>{
            Loan.find({
                userId : eachReferList._id, 
                approvalManager : "approve", 
                status : {$in : ["accepted","paid"]}
            },{approvalManagerTime : 1, amount : 1, }).exec((err,appliedLoanAll)=>{
                async.each(appliedLoanAll,(eachAppliedLoan,callback)=>{
                    appliedLoanList.push({
                        dateOfApproval : eachAppliedLoan.approvalManagerTime,
                        contactId : eachAppliedLoan._id,
                        amount :  "RM" +  parseFloat(eachAppliedLoan.amount).toFixed(2),
                        status : eachAppliedLoan.status == "paid" ? "Repaid" : "Payback",
                        statusColour : eachAppliedLoan.status == "paid" ? "green" : "yellow",
                        commission : eachAppliedLoan.status == "paid" ? "RM" +  parseFloat(eachAppliedLoan.amount * 10 / 100).toFixed(2) : "-",
                    })
                    callback()
                },(err)=>{
                    callback()
                })
            })
        },(err)=>{
            res.json({
                status : 200,
                appliedLoanList : appliedLoanList
            })
        })
    })
})

router.get('/referRenewedLoanList/:userId',(req,res)=>{
    let userId = req.params.userId
    let renewedLoanList = []

    User.find({referBy : userId,},{_id:1}).exec((err,referList)=>{
        async.each(referList,(eachReferList,callback)=>{
            Loan.find({
                userId : eachReferList._id, 
                approvalManager : "approve", 
                status : "offset"
            },{approvalManagerTime : 1, amount : 1, }).exec((err,renewedLoanAll)=>{
                async.each(renewedLoanAll,(eachRenewedLoan,callback)=>{
                    renewedLoanList.push({
                        dateOfApproval : eachRenewedLoan.approvalManagerTime,
                        contactId : eachRenewedLoan._id,
                        amount :  "RM" +  parseFloat(eachRenewedLoan.amount).toFixed(2),
                        status : eachRenewedLoan.status == "paid" ? "Repaid" : "Payback",
                        statusColour : eachRenewedLoan.status == "paid" ? "green" : "yellow",
                        commission : eachRenewedLoan.status == "paid" ? "RM" +  parseFloat(eachRenewedLoan.amount * 10 / 100).toFixed(2) : "-",
                    })
                    callback()
                },(err)=>{
                    callback()
                })
            })
        },(err)=>{
            res.json({
                status : 200,
                renewedLoanList : renewedLoanList
            })
        })
    })
})

router.post('/requestWithdrawal/:userId',(req,res)=>{
    let userId = req.params.userId
    let amount = req.body.amount

    User.findById(userId,{commissionBalance:1,agentId:1}).exec((err,userDetails)=>{
        if(!err && userDetails && userDetails.commissionBalance >= amount){
            let newExpenses = new Expenses({
                type : "userWithdrawCommission",
                expenses : amount,
                userId : userId,
                agentId : userDetails.agentId,
                status : "pending",
                category : "Withrawal Commission",
                remark : "Withrawal Commission",
            })
            newExpenses.save((err,expensesDetails)=>{
                if(!err && expensesDetails){
                    let message = {
                        header : "Your withrawal commission have been registered.",
                        content : "Your withrawal commission ID : " + expensesDetails._id + " has been registered at " + moment().add(8, 'hours').format('MMM Do, YYYY') +  ". MrRinggit.",
                    }
                    pushNotification(userId,message)
                    res.json({status : 200})
                }else{
                    res.json({status : 400})
                }
            })
        }else{
            res.json({status : 400})
        }
    })
})

router.get('/getStaticContent/:contentName',(req,res,next)=>{
    let contentName = req.params.contentName
    StaticContent.findOne({contentName: contentName}).exec((err,contentDetails)=>{
        if(!err && contentDetails){
            res.json({
                status : 200,
                contentId : contentDetails._id,
                contentName : contentDetails.contentName,
                type : contentDetails.type,
                title : contentDetails.title,
                description : contentDetails.description,
                contentImage : contentDetails.contentImage,
                content : contentDetails.content,
            })
        }else{
            res.json({status : 400})
        }
    })
})

module.exports = router;