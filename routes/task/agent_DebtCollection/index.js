const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/pending', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryPayment = {
		status : "pending", 
		approvalAgent : "pending", 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""
	Payment.find(queryPayment).exec((err,payments)=>{
		if(!err && payments){
			let allPayment = []
			async.each(payments,(eachPayment, callback)=>{	
				Loan.findById(eachPayment.loanId, {
					loanType:1,
					installment:1,
					outstanding:1,
					principalAmount:1,
					interest:1,
					userId:1,
					agent:1,
					agentId:1,
					nextPayment:1,
					status:1,
					interest : 1,
					interestRate : 1,
					principalAmount : 1,
				}).exec((err,loanDetails)=>{
					if(!err && loanDetails){
						let queryUser = {_id : eachPayment.userId}
						searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			
						User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
							if(!err && userDetails){
								let collectedAmount = parseInt(eachPayment.amount* 100) / 100
								let interestPaid = loanDetails.interest > 0 ? loanDetails.interest : 0/* loanDetails.interest > parseInt(eachPayment.amount * loanDetails.interestRate / (100 + loanDetails.interestRate) * 100) / 100 ? parseInt(eachPayment.amount * loanDetails.interestRate / (100 + loanDetails.interestRate) * 100) / 100 : loanDetails.interest */
									
								allPayment.push({
									paymentId : eachPayment._id,
									loanId : loanDetails._id,
									customerId : userDetails._id,
									customerName : userDetails.username,
									phoneNumber : userDetails.phoneNumber,
									loanType : loanDetails.loanType,
									installment : loanDetails.installment,
									collected : collectedAmount,
									outstanding : loanDetails.outstanding,
									agentId : loanDetails.agentId,
									agent : loanDetails.agent,
									paidBy : eachPayment.paidBy,
									status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
									// paymentDueDate : loanDetails.nextPayment,
									paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
									paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
									paidOnGMT : eachPayment.createdAt,
									paymentProof : eachPayment.imageUrl,
									interest : loanDetails.interest,
									interestRate : loanDetails.interestRate,
									principalAmount : loanDetails.principalAmount,
									principalPaid : collectedAmount - interestPaid,
									interestPaid : interestPaid,	
									
									
								})
								callback()
							}else{
								callback()
							}
						})
					}else{
						callback()
					}
				})
	
			},(err)=>{
				allPayment.sort((a,b)=>{
					return moment(a.paidOnGMT).unix() - moment(b.paidOnGMT).unix()
				})
				res.render('task/agent_DebtCollection/pending', {checkDebtCollection:"false", payments : allPayment, admin: req.session.admin });
			})
		}else{
			res.render('task/agent_DebtCollection/pending', {checkDebtCollection:"false", payments : [], admin: req.session.admin });
		}
	})
})

app.get('/pending/:paymentId', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact
	let paymentId = req.params.paymentId

	let queryPayment = {
		_id: paymentId,
		status : "pending", 
		approvalAgent : "pending", 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""
	Payment.find(queryPayment).exec((err,payments)=>{
		if(!err && payments){
			let allPayment = []
			async.each(payments,(eachPayment, callback)=>{	
				Loan.findById(eachPayment.loanId, {
					loanType:1,
					installment:1,
					outstanding:1,
					principalAmount:1,
					interest:1,
					userId:1,
					agent:1,
					agentId:1,
					nextPayment:1,
					status:1,
					interest : 1,
					interestRate : 1,
					principalAmount : 1,
				}).exec((err,loanDetails)=>{
					if(!err && loanDetails){
						let queryUser = {_id : eachPayment.userId}
						searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			
						User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
							if(!err && userDetails){
								let collectedAmount = parseInt(eachPayment.amount* 100) / 100
								let interestPaid = loanDetails.interest > 0 ? loanDetails.interest : 0/* loanDetails.interest > parseInt(eachPayment.amount * loanDetails.interestRate / (100 + loanDetails.interestRate) * 100) / 100 ? parseInt(eachPayment.amount * loanDetails.interestRate / (100 + loanDetails.interestRate) * 100) / 100 : loanDetails.interest */
									
								allPayment.push({
									paymentId : eachPayment._id,
									loanId : loanDetails._id,
									customerId : userDetails._id,
									customerName : userDetails.username,
									phoneNumber : userDetails.phoneNumber,
									loanType : loanDetails.loanType,
									installment : loanDetails.installment,
									collected : collectedAmount,
									outstanding : loanDetails.outstanding,
									agentId : loanDetails.agentId,
									agent : loanDetails.agent,
									paidBy : eachPayment.paidBy,
									status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
									// paymentDueDate : loanDetails.nextPayment,
									paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
									paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
									paidOnGMT : eachPayment.createdAt,
									paymentProof : eachPayment.imageUrl,
									interest : loanDetails.interest,
									interestRate : loanDetails.interestRate,
									principalAmount : loanDetails.principalAmount,
									principalPaid : collectedAmount - interestPaid,
									interestPaid : interestPaid,						
								})
								callback()
							}else{
								callback()
							}
						})
					}else{
						callback()
					}
				})
	
			},(err)=>{
				allPayment.sort((a,b)=>{
					return moment(a.paidOnGMT).unix() - moment(b.paidOnGMT).unix()
				})
				res.render('task/agent_DebtCollection/pending', {checkDebtCollection:"true", payments : allPayment, admin: req.session.admin });
			})
		}else{
			res.render('task/agent_DebtCollection/pending', {checkDebtCollection:"true", payments : [], admin: req.session.admin });
		}
	})
})

app.get('/rejectingPayment/:paymentId', isLoggedIn,(req,res)=>{
	let paymentId = req.params.paymentId
	let rejectionReasonAgent = "Invalid payment!"
	Payment.findByIdAndUpdate(paymentId,{status : "rejected", approvalAgentName : req.session.admin.name , approvalAgentId : req.session.admin._id, approvalAgent : "reject", rejectionReasonAgent : rejectionReasonAgent, approvalAgentTime : moment().toISOString()}).exec((err,done)=>{
		if(!err && done){
			res.redirect('/agent/taskDebtCollection/pending')
		}else{
			res.json({status:400})
		}
	})
})

app.post('/approvedPayment/:paymentId', isLoggedIn,(req,res)=>{
	let paymentId = req.params.paymentId
	Payment.findByIdAndUpdate(paymentId,{
		approvalAgent : "approve", 
		approvalAgentName : req.session.admin.name ,
		approvalAgentId : req.session.admin._id, 
		approvalAgentTime : moment().toISOString(),
		principalPaid : req.body.principalPaid,
		interestPaid : req.body.interestPaid,

	}).exec((err,done)=>{
		if(!err && done){
			res.redirect('/agent/taskDebtCollection/pending')
		}else{
			res.json({status:400})
		}
	})
})

app.get('/agentApproved', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryPayment = {
		status : "pending", 
		approvalAgent : "approve", 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""
	Payment.find(queryPayment).exec((err,payments)=>{
		if(!err && payments){
			let allPayment = []
			async.each(payments,(eachPayment, callback)=>{	
				Loan.findById(eachPayment.loanId, {
					loanType:1,
					installment:1,
					outstanding:1,
					principalAmount:1,
					interest:1,
					userId:1,
					agent:1,
					agentId:1,
					nextPayment:1,
					status:1,
					interest : 1,
					interestRate : 1,
					principalAmount : 1,
				}).exec((err,loanDetails)=>{
					if(!err && loanDetails){
						let queryUser = {_id : eachPayment.userId}
						searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			
						User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
							if(!err && userDetails){
								let collectedAmount = parseInt(eachPayment.amount* 100) / 100
									
								allPayment.push({
									paymentId : eachPayment._id,
									loanId : loanDetails._id,
									customerId : userDetails._id,
									customerName : userDetails.username,
									phoneNumber : userDetails.phoneNumber,
									loanType : loanDetails.loanType,
									installment : loanDetails.installment,
									collected : collectedAmount,
									outstanding : loanDetails.outstanding,
									agentId : loanDetails.agentId,
									agent : loanDetails.agent,
									paidBy : eachPayment.paidBy,
									status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
									// paymentDueDate : loanDetails.nextPayment,
									paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
									// paidOn : eachPayment.createdAt,
									paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
									paidOnGMT : eachPayment.createdAt,
									paymentProof : eachPayment.imageUrl,
									interest : loanDetails.interest,
									interestRate : loanDetails.interestRate,
									principalAmount : loanDetails.principalAmount,
									principalPaid : eachPayment.principalPaid,
									interestPaid : eachPayment.interestPaid,
									// approvalAgentTime : eachPayment.approvalAgentTime,		
									approvalAgentTime : moment(eachPayment.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
									approvalAgentTimeGMT : eachPayment.approvalAgentTime,		
									
									
								})
								callback()
							}else{
								callback()
							}
						})
					}else{
						callback()
					}
				})
	
			},(err)=>{
				allPayment.sort((a,b)=>{
					return moment(b.approvalAgentTimeGMT).unix() - moment(a.approvalAgentTimeGMT).unix()
				})
				res.render('task/agent_DebtCollection/agentApproved', {checkDebtCollection:"false", payments : allPayment, admin: req.session.admin });
			})
		}else{
			res.render('task/agent_DebtCollection/agentApproved', {checkDebtCollection:"false", payments : [], admin: req.session.admin });
		}
	})
})

app.get('/agentApproved/:paymentId', isLoggedIn, (req, res) => {
	let paymentId = req.params.paymentId
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryPayment = {
		_id : paymentId,
		status : "pending", 
		approvalAgent : "approve", 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""
	Payment.find(queryPayment).exec((err,payments)=>{
		if(!err && payments){
			let allPayment = []
			async.each(payments,(eachPayment, callback)=>{	
				Loan.findById(eachPayment.loanId, {
					loanType:1,
					installment:1,
					outstanding:1,
					principalAmount:1,
					interest:1,
					userId:1,
					agent:1,
					agentId:1,
					nextPayment:1,
					status:1,
					interest : 1,
					interestRate : 1,
					principalAmount : 1,
				}).exec((err,loanDetails)=>{
					if(!err && loanDetails){
						let queryUser = {_id : eachPayment.userId}
						searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			
						User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
							if(!err && userDetails){
								let collectedAmount = parseInt(eachPayment.amount* 100) / 100
									
								allPayment.push({
									paymentId : eachPayment._id,
									loanId : loanDetails._id,
									customerId : userDetails._id,
									customerName : userDetails.username,
									phoneNumber : userDetails.phoneNumber,
									loanType : loanDetails.loanType,
									installment : loanDetails.installment,
									collected : collectedAmount,
									outstanding : loanDetails.outstanding,
									agentId : loanDetails.agentId,
									agent : loanDetails.agent,
									paidBy : eachPayment.paidBy,
									status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
									// paymentDueDate : loanDetails.nextPayment,
									paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
									// paidOn : eachPayment.createdAt,
									paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
									paidOnGMT : eachPayment.createdAt,
									paymentProof : eachPayment.imageUrl,
									interest : loanDetails.interest,
									interestRate : loanDetails.interestRate,
									principalAmount : loanDetails.principalAmount,
									principalPaid : eachPayment.principalPaid,
									interestPaid : eachPayment.interestPaid,
									// approvalAgentTime : eachPayment.approvalAgentTime,		
									approvalAgentTime : moment(eachPayment.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
									approvalAgentTimeGMT : eachPayment.approvalAgentTime,	
									
									
								})
								callback()
							}else{
								callback()
							}
						})
					}else{
						callback()
					}
				})
	
			},(err)=>{
				allPayment.sort((a,b)=>{
					return moment(b.approvalAgentTimeGMT).unix() - moment(a.approvalAgentTimeGMT).unix()
				})
				res.render('task/agent_DebtCollection/agentApproved', {checkDebtCollection:"true", payments : allPayment, admin: req.session.admin });
			})
		}else{
			res.render('task/agent_DebtCollection/agentApproved', {checkDebtCollection:"true", payments : [], admin: req.session.admin });
		}
	})
})

app.get('/agentRejected', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryPayment = {
		status : "rejected", 
		approvalAgent : "reject", 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""
	Payment.find(queryPayment).exec((err,payments)=>{
		if(!err && payments){
			let allPayment = []
			async.each(payments,(eachPayment, callback)=>{	
				Loan.findById(eachPayment.loanId, {
					loanType:1,
					installment:1,
					outstanding:1,
					principalAmount:1,
					interest:1,
					userId:1,
					agent:1,
					agentId:1,
					nextPayment:1,
					status:1,
					interest : 1,
					interestRate : 1,
					principalAmount : 1,
				}).exec((err,loanDetails)=>{
					if(!err && loanDetails){
						let queryUser = {_id : eachPayment.userId}
						searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			
						User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
							if(!err && userDetails){
								let collectedAmount = parseInt(eachPayment.amount* 100) / 100
								let interestPaid = loanDetails.interest > 0 ? loanDetails.interest : 0/* loanDetails.interest > parseInt(eachPayment.amount * loanDetails.interestRate / (100 + loanDetails.interestRate) * 100) / 100 ? parseInt(eachPayment.amount * loanDetails.interestRate / (100 + loanDetails.interestRate) * 100) / 100 : loanDetails.interest */
									
								allPayment.push({
									paymentId : eachPayment._id,
									loanId : loanDetails._id,
									customerId : userDetails._id,
									customerName : userDetails.username,
									phoneNumber : userDetails.phoneNumber,
									loanType : loanDetails.loanType,
									installment : loanDetails.installment,
									collected : collectedAmount,
									outstanding : loanDetails.outstanding,
									agentId : loanDetails.agentId,
									agent : loanDetails.agent,
									paidBy : eachPayment.paidBy,
									status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
									// paymentDueDate : loanDetails.nextPayment,
									paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
									// paidOn : eachPayment.createdAt,
									paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
									paidOnGMT : eachPayment.createdAt,
									paymentProof : eachPayment.imageUrl,
									interest : loanDetails.interest,
									interestRate : loanDetails.interestRate,
									principalAmount : loanDetails.principalAmount,
									principalPaid : collectedAmount - interestPaid,
									interestPaid : interestPaid,
									// approvalAgentTime : eachPayment.approvalAgentTime,		
									approvalAgentTime : moment(eachPayment.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
									approvalAgentTimeGMT : eachPayment.approvalAgentTime,	
									rejectionReasonAgent : eachPayment.rejectionReasonAgent,	
									
									
								})
								callback()
							}else{
								callback()
							}
						})
					}else{
						callback()
					}
				})
	
			},(err)=>{
				allPayment.sort((a,b)=>{
					return moment(b.approvalAgentTimeGMT).unix() - moment(a.approvalAgentTimeGMT).unix()
				})
				res.render('task/agent_DebtCollection/agentRejected', {checkDebtCollection:"false", payments : allPayment, admin: req.session.admin });
			})
		}else{
			res.render('task/agent_DebtCollection/agentRejected', {checkDebtCollection:"false", payments : [], admin: req.session.admin });
		}
	})
})

app.get('/managerApproved', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryPayment = {
		status : "accepted", 
		approvalManager : "approve", 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""
	Payment.find(queryPayment).exec((err,payments)=>{
		if(!err && payments){
			let allPayment = []
			async.each(payments,(eachPayment, callback)=>{	
				Loan.findById(eachPayment.loanId, {
					loanType:1,
					installment:1,
					outstanding:1,
					principalAmount:1,
					interest:1,
					userId:1,
					agent:1,
					agentId:1,
					nextPayment:1,
					status:1,
					interest : 1,
					interestRate : 1,
					principalAmount : 1,
				}).exec((err,loanDetails)=>{
					if(!err && loanDetails){
						let queryUser = {_id : eachPayment.userId}
						searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			
						User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
							if(!err && userDetails){
								let collectedAmount = parseInt(eachPayment.amount* 100) / 100
									
								allPayment.push({
									paymentId : eachPayment._id,
									loanId : loanDetails._id,
									customerId : userDetails._id,
									customerName : userDetails.username,
									phoneNumber : userDetails.phoneNumber,
									loanType : loanDetails.loanType,
									installment : loanDetails.installment,
									collected : collectedAmount,
									outstanding : loanDetails.outstanding,
									agentId : loanDetails.agentId,
									agent : loanDetails.agent,
									paidBy : eachPayment.paidBy,
									status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
									// paymentDueDate : loanDetails.nextPayment,
									paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
									// paidOn : eachPayment.createdAt,
									paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
									paidOnGMT : eachPayment.createdAt,
									paymentProof : eachPayment.imageUrl && eachPayment.imageUrl != "" ? eachPayment.imageUrl : "http://128.199.170.141:7000/img/no-image.jpg",
									interest : loanDetails.interest,
									interestRate : loanDetails.interestRate,
									principalAmount : loanDetails.principalAmount,
									principalPaid : eachPayment.principalPaid,
									interestPaid : eachPayment.interestPaid,
									// approvalAgentTime : eachPayment.approvalAgentTime,		
									approvalAgentTime : moment(eachPayment.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
									approvalAgentTimeGMT : eachPayment.approvalAgentTime,		
									// approvalManagerTime : eachPayment.approvalManagerTime,	
									approvalManagerTime : moment(eachPayment.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
									approvalManagerTimeGMT : eachPayment.approvalManagerTime,	
									
									
								})
								callback()
							}else{
								callback()
							}
						})
					}else{
						callback()
					}
				})
	
			},(err)=>{
				allPayment.sort((a,b)=>{
					return moment(b.approvalManagerTimeGMT).unix() - moment(a.approvalManagerTimeGMT).unix()
				})
				res.render('task/agent_DebtCollection/managerApproved', {checkDebtCollection:"false", payments : allPayment, admin: req.session.admin });
			})
		}else{
			res.render('task/agent_DebtCollection/managerApproved', {checkDebtCollection:"false", payments : [], admin: req.session.admin });
		}
	})
})

app.get('/managerRejected', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryPayment = {
		status : "rejected", 
		approvalManager : "reject", 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""
	Payment.find(queryPayment).exec((err,payments)=>{
		if(!err && payments){
			let allPayment = []
			async.each(payments,(eachPayment, callback)=>{	
				Loan.findById(eachPayment.loanId, {
					loanType:1,
					installment:1,
					outstanding:1,
					principalAmount:1,
					interest:1,
					userId:1,
					agent:1,
					agentId:1,
					nextPayment:1,
					status:1,
					interest : 1,
					interestRate : 1,
					principalAmount : 1,
				}).exec((err,loanDetails)=>{
					if(!err && loanDetails){
						let queryUser = {_id : eachPayment.userId}
						searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			
						User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
							if(!err && userDetails){
								let collectedAmount = parseInt(eachPayment.amount* 100) / 100
									
								allPayment.push({
									paymentId : eachPayment._id,
									loanId : loanDetails._id,
									customerId : userDetails._id,
									customerName : userDetails.username,
									phoneNumber : userDetails.phoneNumber,
									loanType : loanDetails.loanType,
									installment : loanDetails.installment,
									collected : collectedAmount,
									outstanding : loanDetails.outstanding,
									agentId : loanDetails.agentId,
									agent : loanDetails.agent,
									paidBy : eachPayment.paidBy,
									status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
									// paymentDueDate : loanDetails.nextPayment,
									paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
									// paidOn : eachPayment.createdAt,
									paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
									paidOnGMT : eachPayment.createdAt,
									paymentProof : eachPayment.imageUrl && eachPayment.imageUrl != "" ? eachPayment.imageUrl : "http://128.199.170.141:7000/img/no-image.jpg",
									interest : loanDetails.interest,
									interestRate : loanDetails.interestRate,
									principalAmount : loanDetails.principalAmount,
									principalPaid : eachPayment.principalPaid,
									interestPaid : eachPayment.interestPaid,
									// approvalAgentTime : eachPayment.approvalAgentTime,		
									approvalAgentTime : moment(eachPayment.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
									approvalAgentTimeGMT : eachPayment.approvalAgentTime,		
									// approvalManagerTime : eachPayment.approvalManagerTime,	
									approvalManagerTime : moment(eachPayment.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
									approvalManagerTimeGMT : eachPayment.approvalManagerTime,	
									rejectionReasonManager : eachPayment.approvalManagerTime,
									
									
								})
								callback()
							}else{
								callback()
							}
						})
					}else{
						callback()
					}
				})
	
			},(err)=>{
				allPayment.sort((a,b)=>{
					return moment(b.approvalManagerTimeGMT).unix() - moment(a.approvalManagerTimeGMT).unix()
				})
				res.render('task/agent_DebtCollection/managerRejected', {checkDebtCollection:"false", payments : allPayment, admin: req.session.admin });
			})
		}else{
			res.render('task/agent_DebtCollection/managerRejected', {checkDebtCollection:"false", payments : [], admin: req.session.admin });
		}
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}