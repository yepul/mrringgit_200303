const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');
let Loan = require('../../../models/loan/loan');
let Payment = require('../../../models/payment/payment');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	let numberOfEachTask = {
		disbursement : 0,
		debtCollection :0,
		blacklistRequest : 0,
		releaseCommmission : 0,
		payExpenses : 0,
	}
	Loan.countDocuments({status : "new", approvalAgent : "approve"}).exec((err, numberOfDisbursement)=>{
		numberOfEachTask.disbursement = numberOfDisbursement
		// res.render('task/manager/task', { numberOfEachTask: numberOfEachTask, admin: req.session.admin });
		Payment.countDocuments({status : "pending", approvalAgent : "approve"}).exec((err, numberOfDebtCollection)=>{
			numberOfEachTask.disbursement = numberOfDisbursement
			numberOfEachTask.debtCollection = numberOfDebtCollection
			res.render('task/manager/task', { numberOfEachTask: numberOfEachTask, admin: req.session.admin });
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}