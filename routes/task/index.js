const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let User = require('../../models/user/user');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	if(req.session.admin && req.session.admin.level == "agent"){
		res.redirect('/agent/task')
	}else if(req.session.admin && req.session.admin.level == "manager"){
		res.redirect('/manager/task')
	}
})

// app.get('/newApplication', isLoggedIn, (req, res) => {
// 	res.render('task/newApplication', { admin: req.session.admin });
// })

// app.get('/agentApprovedApplication', isLoggedIn, (req, res) => {
// 	res.render('task/agentApprovedApplication', { admin: req.session.admin });
// })

// app.get('/agentRejectedApplication', isLoggedIn, (req, res) => {
// 	res.render('task/agentRejectedApplication', { admin: req.session.admin });
// })

// app.get('/agentLoanDisbursedApproved', isLoggedIn, (req, res) => {
// 	res.render('task/agentLoanDisbursedApproved', { admin: req.session.admin });
// })

// app.get('/agentLoanDisbursedRejected', isLoggedIn, (req, res) => {
// 	res.render('task/agentLoanDisbursedRejected', { admin: req.session.admin });
// })

// app.get('/newDebtCollection', isLoggedIn, (req, res) => {
// 	res.render('task/newDebtCollection', { admin: req.session.admin });
// })

// app.get('/agentApprovedDebtCollection', isLoggedIn, (req, res) => {
// 	res.render('task/agentApprovedDebtCollection', { admin: req.session.admin });
// })

// app.get('/agentRejectedDebtCollection', isLoggedIn, (req, res) => {
// 	res.render('task/agentRejectedDebtCollection', { admin: req.session.admin });
// })

// app.get('/agentApprovedDebtCollectionManager', isLoggedIn, (req, res) => {
// 	res.render('task/agentApprovedDebtCollectionManager', { admin: req.session.admin });
// })

// app.get('/agentRejectedDebtCollectionManager', isLoggedIn, (req, res) => {
// 	res.render('task/agentRejectedDebtCollectionManager', { admin: req.session.admin });
// })

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		return next();
	}
	res.redirect('/login');
}