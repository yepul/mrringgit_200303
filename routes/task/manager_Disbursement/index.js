const express = require('express');
const async = require('async')
const moment = require('moment')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');
let Loan = require('../../../models/loan/loan');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/pending', isLoggedIn, (req, res) => {
	Loan.find({status : "new", approvalAgent : "approve"}).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan,callback)=>{
			User.findOne({_id : eachLoan.userId},{username:1,phoneNumber:1,bankName:1,bankAccountName:1,bankAccountNumber:1}).exec((err,userDetails)=>{
				if(!err && userDetails /* && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						loanType : eachLoan.loanType,
						disbursement : eachLoan.disbursement,
						bankName : userDetails.bankName,
						bankAccountName : userDetails.bankAccountName,
						bankAccountNumber : userDetails.bankAccountNumber,
						agent : eachLoan.agent,
						// approvalOn : eachLoan.approvalAgentTime,
						approvalOn : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalOnGMT : eachLoan.approvalAgentTime,
					})
					callback()
				}else{
					callback()
				}
			})
		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.approvalOnGMT).unix() - moment(a.approvalOnGMT).unix()
			})
			// res.render('task/agent_NewApplication/newApplication', { loans : allLoans, admin: req.session.admin });
			res.render('task/manager_Disbursement/pending', {rejectingLoanModal : "false", uploadReceiptModal : "false", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/approve/:loanId', isLoggedIn, (req,res)=>{
	let loanId = req.params.loanId
	Loan.findByIdAndUpdate(loanId, {status : "accepted", approvalManager : "approve", approvalManagerTime : moment().toISOString(), nextPayment : moment().add(1, 'month').toISOString()}).exec((err,done)=>{
		if(!err && done){
			res.redirect('/manager/taskDisbursement/pending')
		}else{
			console.log(err)
			res.json({status:400})
		}
	})
})

app.get('/uploadReceipt/:loanId', isLoggedIn, (req,res)=>{
	let loanId = req.params.loanId
	Loan.find({_id: loanId, status : "new", approvalAgent : "approve"}).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan,callback)=>{
			User.findOne({_id : eachLoan.userId},{username:1,phoneNumber:1,bankName:1,bankAccountName:1,bankAccountNumber:1}).exec((err,userDetails)=>{
				if(!err && userDetails /* && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						loanType : eachLoan.loanType,
						disbursement : eachLoan.disbursement,
						bankName : userDetails.bankName,
						bankAccountName : userDetails.bankAccountName,
						bankAccountNumber : userDetails.bankAccountNumber,
						agent : eachLoan.agent,
						// approvalOn : eachLoan.approvalAgentTime,
						approvalOn : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalOnGMT : eachLoan.approvalAgentTime,
					})
					callback()
				}else{
					callback()
				}
			})
		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.approvalOnGMT).unix() - moment(a.approvalOnGMT).unix()
			})
			// res.render('task/agent_NewApplication/newApplication', { loans : allLoans, admin: req.session.admin });
			res.render('task/manager_Disbursement/pending', {rejectingLoanModal : "false", uploadReceiptModal : "true", loans : allLoans, admin: req.session.admin });
		})
	})

})

app.get('/rejectingLoan/:loanId', isLoggedIn, (req,res)=>{
	let loanId = req.params.loanId
	Loan.findById(loanId).exec((err,loanDetails)=>{
		if(!err && loanDetails){
			User.findById(loanDetails.userId).exec((err,userDetails)=>{
				if(!err && userDetails){
					let allLoans = [{
						loanId : loanDetails._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						monthlyIncome : userDetails.monthlyIncome,
						loanType : loanDetails.loanType,
						amount : loanDetails.amount,
						duration : loanDetails.duration + " " + loanDetails.paymentType,
						interest : loanDetails.interest,
						offset : loanDetails.offset,
						disbursement : loanDetails.disbursement,
						installment : loanDetails.installment,
						type : loanDetails.paymentType,
						// requestedOn : loanDetails.requestedOn,
						requestedOn : moment(loanDetails.requestedOn).add(8, 'hours').format('MMM Do, YYYY'),
						agent : loanDetails.agent
					}]
					// res.render('task/agent_NewApplication/newApplication', {showRejectLoanModal : "true", loans : allLoans, admin: req.session.admin });
					res.render('task/manager_Disbursement/pending', {rejectingLoanModal : "true", uploadReceiptModal : "false", loans : allLoans, admin: req.session.admin });
				}
			})
		}else{
			console.log(err)
			res.json({status:400})
		}
	})
})

app.post('/rejectingLoan/:loanId', isLoggedIn, (req,res)=>{
	// res.json({status:200, body:req.body})
	let loanId = req.params.loanId
	let rejectionReasonManager = req.body.reason
	Loan.findByIdAndUpdate(loanId, {status : "rejected", approvalManager : "reject", rejectionReasonManager : rejectionReasonManager, approvalManagerTime : moment().toISOString()}).exec((err,done)=>{
		if(!err && done){
			User.findByIdAndUpdate(done.userId,{currentLoanId : ""}).exec((err,done)=>{
				if(!err && done){
					res.redirect('/manager/taskDisbursement/pending')
				}else{
					res.json({status:400})
				}
			})
		}else{
			console.log(err)
			res.json({status:400})
		}
	})
})

app.get('/disbursed', isLoggedIn, (req, res) => {
	Loan.find({status : "accepted"}).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan,callback)=>{
			User.findOne({_id : eachLoan.userId},{username:1,phoneNumber:1,bankName:1,bankAccountName:1,bankAccountNumber:1}).exec((err,userDetails)=>{
				if(!err && userDetails /* && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						loanType : eachLoan.loanType,
						disbursement : eachLoan.disbursement,
						bankName : userDetails.bankName,
						bankAccountName : userDetails.bankAccountName,
						bankAccountNumber : userDetails.bankAccountNumber,
						agent : eachLoan.agent,
						// approvalOn : eachLoan.approvalAgentTime,
						approvalOn : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalOnGMT : eachLoan.approvalAgentTime,
						// approvalManagerTime : eachLoan.approvalManagerTime,
						approvalManagerTime : moment(eachLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalManagerTimeGMT : eachLoan.approvalManagerTime,
						disbursementProof : eachLoan.disbursementProof
					})
					callback()
				}else{
					callback()
				}
			})
		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.approvalManagerTimeGMT).unix() - moment(a.approvalManagerTimeGMT).unix()
			})
			// res.render('task/agent_NewApplication/newApplication', { loans : allLoans, admin: req.session.admin });
			res.render('task/manager_Disbursement/disbursed', {receiptCheckModal : "false", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/checkDisbursementProof/:loanId', isLoggedIn, (req, res) => {
	let loanId = req.params.loanId
	Loan.find({_id:loanId,status : "accepted"}).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan,callback)=>{
			User.findOne({_id : eachLoan.userId},{username:1,phoneNumber:1,bankName:1,bankAccountName:1,bankAccountNumber:1}).exec((err,userDetails)=>{
				if(!err && userDetails /* && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						loanType : eachLoan.loanType,
						disbursement : eachLoan.disbursement,
						bankName : userDetails.bankName,
						bankAccountName : userDetails.bankAccountName,
						bankAccountNumber : userDetails.bankAccountNumber,
						agent : eachLoan.agent,
						// approvalOn : eachLoan.approvalAgentTime,
						approvalOn : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalOnGMT : eachLoan.approvalAgentTime,
						// approvalManagerTime : eachLoan.approvalManagerTime,
						approvalManagerTime : moment(eachLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalManagerTimeGMT : eachLoan.approvalManagerTime,
						disbursementProof : eachLoan.disbursementProof
					})
					callback()
				}else{
					callback()
				}
			})
		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.approvalManagerTimeGMT).unix() - moment(a.approvalManagerTimeGMT).unix()
			})
			res.render('task/manager_Disbursement/disbursed', {receiptCheckModal : "true", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/rejected', isLoggedIn, (req, res) => {
	Loan.find({status : "rejected"}).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan,callback)=>{
			User.findOne({_id : eachLoan.userId},{username:1,phoneNumber:1,bankName:1,bankAccountName:1,bankAccountNumber:1}).exec((err,userDetails)=>{
				if(!err && userDetails /* && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						loanType : eachLoan.loanType,
						disbursement : eachLoan.disbursement,
						bankName : userDetails.bankName,
						bankAccountName : userDetails.bankAccountName,
						bankAccountNumber : userDetails.bankAccountNumber,
						agent : eachLoan.agent,
						// approvalOn : eachLoan.approvalAgentTime,
						approvalOn : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalOnGMT : eachLoan.approvalAgentTime,
						// approvalManagerTime : eachLoan.approvalManagerTime,
						approvalManagerTime : moment(eachLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalManagerTimeGMT : eachLoan.approvalManagerTime,
						rejectionReasonManager : eachLoan.rejectionReasonManager
					})
					callback()
				}else{
					callback()
				}
			})
		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.approvalManagerTimeGMT).unix() - moment(a.approvalManagerTimeGMT).unix()
			})
			// res.render('task/agent_NewApplication/newApplication', { loans : allLoans, admin: req.session.admin });
			res.render('task/manager_Disbursement/rejected', {rejectReasonModal:"false", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/rejected/:loanId', isLoggedIn, (req, res) => {
	let loanId = req.params.loanId
	Loan.find({_id : loanId, status : "rejected"}).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan,callback)=>{
			User.findOne({_id : eachLoan.userId},{username:1,phoneNumber:1,bankName:1,bankAccountName:1,bankAccountNumber:1}).exec((err,userDetails)=>{
				if(!err && userDetails /* && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						loanType : eachLoan.loanType,
						disbursement : eachLoan.disbursement,
						bankName : userDetails.bankName,
						bankAccountName : userDetails.bankAccountName,
						bankAccountNumber : userDetails.bankAccountNumber,
						agent : eachLoan.agent,
						// approvalOn : eachLoan.approvalAgentTime,
						approvalOn : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalOnGMT : eachLoan.approvalAgentTime,
						// approvalManagerTime : eachLoan.approvalManagerTime,
						approvalManagerTime : moment(eachLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalManagerTimeGMT : eachLoan.approvalManagerTime,
						rejectionReasonManager : eachLoan.rejectionReasonManager
					})
					callback()
				}else{
					callback()
				}
			})
		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.approvalManagerTimeGMT).unix() - moment(a.approvalManagerTimeGMT).unix()
			})
			// res.render('task/agent_NewApplication/newApplication', { loans : allLoans, admin: req.session.admin });
			res.render('task/manager_Disbursement/rejected', {rejectReasonModal:"true", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/customerDetails/personalInfo/:customerId/:loanId', isLoggedIn, (req, res) => {
	let customerId = req.params.customerId
	let loanId = req.params.loanId
	User.findById(customerId).exec((err,userDetails)=>{
		if(!err && userDetails){
			// console.log(userDetails)
			res.render('dashboard/customerInfoManager/personalInfo', { loanId : loanId, userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

app.get('/customerDetails/residentialInfo/:customerId/:loanId', isLoggedIn, (req, res) => {
	let customerId = req.params.customerId
	let loanId = req.params.loanId
	User.findById(customerId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoManager/residentialInfo', { loanId : loanId,  userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

app.get('/customerDetails/bankAccountInfo/:customerId/:loanId', isLoggedIn, (req, res) => {
	let customerId = req.params.customerId
	let loanId = req.params.loanId
	User.findById(customerId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoManager/bankAccountInfo', { loanId : loanId,  userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

app.get('/customerDetails/workingInfo/:customerId/:loanId', isLoggedIn, (req, res) => {
	let customerId = req.params.customerId
	let loanId = req.params.loanId
	User.findById(customerId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoManager/workingInfo', { loanId : loanId,  userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

app.get('/customerDetails/emergencyContactInfo/:customerId/:loanId', isLoggedIn, (req, res) => {
	let customerId = req.params.customerId
	let loanId = req.params.loanId
	User.findById(customerId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoManager/emergencyContactInfo', { loanId : loanId,  userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}