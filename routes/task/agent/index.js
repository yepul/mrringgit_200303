const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');
let Loan = require('../../../models/loan/loan');
let Payment = require('../../../models/payment/payment');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	let numberOfEachTask = {
		newApplication : 0,
		debtCollection :0,
		commissionWithdraw : 0
	}
	Loan.countDocuments({status : "new"}).exec((err, numberOfNewApplication)=>{
		// numberOfEachTask.newApplication = numberOfDocument
		// res.render('task/agent/task', { numberOfEachTask: numberOfEachTask, admin: req.session.admin });
		Payment.countDocuments({status : "pending"}).exec((err, numberOfDebtCollection)=>{
			numberOfEachTask.newApplication = numberOfNewApplication
			numberOfEachTask.debtCollection = numberOfDebtCollection
			res.render('task/agent/task', { numberOfEachTask: numberOfEachTask, admin: req.session.admin });
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}