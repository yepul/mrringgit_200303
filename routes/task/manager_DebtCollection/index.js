const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Loan = require('../../../models/loan/loan');
let Payment = require('../../../models/payment/payment');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/pending', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact
	let searchByAgent = req.query.searchByAgent

	let queryPayment = {
		status : "pending", 
		approvalAgent : "approve", 
	}
	searchByAgent && searchByAgent != "" ? queryPayment.approvalAgentName = searchByAgent : ""
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""

		Payment.find(queryPayment).exec((err,payments)=>{
			if(!err && payments){
				let allPayment = []
				async.each(payments,(eachPayment, callback)=>{	
					Loan.findById(eachPayment.loanId, {
						loanType:1,
						installment:1,
						outstanding:1,
						principalAmount:1,
						interest:1,
						userId:1,
						agent:1,
						agentId:1,
						nextPayment:1,
						status:1,
						interest : 1,
						interestRate : 1,
						principalAmount : 1,
					}).exec((err,loanDetails)=>{
						if(!err && loanDetails){
							let queryUser = {_id : eachPayment.userId}
							searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
				
							User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
								if(!err && userDetails){
									let collectedAmount = parseInt(eachPayment.amount* 100) / 100
										
									allPayment.push({
										paymentId : eachPayment._id,
										loanId : loanDetails._id,
										customerId : userDetails._id,
										customerName : userDetails.username,
										phoneNumber : userDetails.phoneNumber,
										loanType : loanDetails.loanType,
										installment : loanDetails.installment,
										collected : collectedAmount,
										outstanding : loanDetails.outstanding,
										agentId : loanDetails.agentId,
										agent : loanDetails.agent,
										paidBy : eachPayment.paidBy,
										status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
										// paymentDueDate : loanDetails.nextPayment,
										paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
										// paidOn : eachPayment.createdAt,
										paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
										paidOnGMT : eachPayment.createdAt,
										paymentProof : eachPayment.imageUrl,
										interest : loanDetails.interest,
										interestRate : loanDetails.interestRate,
										principalAmount : loanDetails.principalAmount,
										principalPaid : eachPayment.principalPaid,
										interestPaid : eachPayment.interestPaid,	
										
										
									})
									callback()
								}else{
									callback()
								}
							})
						}else{
							callback()
						}
					})
		
				},(err)=>{
					allPayment.sort((a,b)=>{
						return moment(a.paidOnGMT).unix() - moment(b.paidOnGMT).unix()
					})
					res.render('task/manager_DebtCollection/pending', {checkDebtCollection:"false", payments : allPayment, admin: req.session.admin });
				})
			}else{
				res.render('task/manager_DebtCollection/pending', {checkDebtCollection:"false", payments : [], admin: req.session.admin });
			}
		})
})

app.get('/pending/:paymentId', isLoggedIn, (req, res) => {
	let paymentId = req.params.paymentId
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact
	let searchByAgent = req.query.searchByAgent

	let queryPayment = {
		_id : paymentId,
		status : "pending", 
		approvalAgent : "approve", 
	}
	searchByAgent && searchByAgent != "" ? queryPayment.approvalAgentName = searchByAgent : ""
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""

		Payment.find(queryPayment).exec((err,payments)=>{
			if(!err && payments){
				let allPayment = []
				async.each(payments,(eachPayment, callback)=>{	
					Loan.findById(eachPayment.loanId, {
						loanType:1,
						installment:1,
						outstanding:1,
						principalAmount:1,
						interest:1,
						userId:1,
						agent:1,
						agentId:1,
						nextPayment:1,
						status:1,
						interest : 1,
						interestRate : 1,
						principalAmount : 1,
					}).exec((err,loanDetails)=>{
						if(!err && loanDetails){
							let queryUser = {_id : eachPayment.userId}
							searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
				
							User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
								if(!err && userDetails){
									let collectedAmount = parseInt(eachPayment.amount* 100) / 100
										
									allPayment.push({
										paymentId : eachPayment._id,
										loanId : loanDetails._id,
										customerId : userDetails._id,
										customerName : userDetails.username,
										phoneNumber : userDetails.phoneNumber,
										loanType : loanDetails.loanType,
										installment : loanDetails.installment,
										collected : collectedAmount,
										outstanding : loanDetails.outstanding,
										agentId : loanDetails.agentId,
										agent : loanDetails.agent,
										paidBy : eachPayment.paidBy,
										status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
										// paymentDueDate : loanDetails.nextPayment,
										paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
										// paidOn : eachPayment.createdAt,
										paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
										paidOnGMT : eachPayment.createdAt,
										paymentProof : eachPayment.imageUrl,
										interest : loanDetails.interest,
										interestRate : loanDetails.interestRate,
										principalAmount : loanDetails.principalAmount,
										principalPaid : eachPayment.principalPaid,
										interestPaid : eachPayment.interestPaid,
										
										
									})
									callback()
								}else{
									callback()
								}
							})
						}else{
							callback()
						}
					})
		
				},(err)=>{
					allPayment.sort((a,b)=>{
						return moment(a.paidOnGMT).unix() - moment(b.paidOnGMT).unix()
					})
					res.render('task/manager_DebtCollection/pending', {checkDebtCollection:"true", payments : allPayment, admin: req.session.admin });
				})
			}else{
				res.render('task/manager_DebtCollection/pending', {checkDebtCollection:"true", payments : [], admin: req.session.admin });
			}
		})
})

app.get('/rejectingPayment/:paymentId', isLoggedIn,(req,res)=>{
	let paymentId = req.params.paymentId
	let rejectionReasonManager = "Invalid payment!"
	Payment.findByIdAndUpdate(paymentId,{status : "rejected", approvalManagerName : req.session.admin.name , approvalManagerId : req.session.admin._id, approvalManager : "reject", rejectionReasonManager : rejectionReasonManager, approvalManagerTime : moment().toISOString()}).exec((err,done)=>{
		if(!err && done){
			res.redirect('/manager/taskDebtCollection/pending')
		}else{
			res.json({status:400})
		}
	})
})

app.post('/approvedPayment/:paymentId', isLoggedIn,(req,res)=>{
	let paymentId = req.params.paymentId
	Payment.findByIdAndUpdate(paymentId,{
		status : "accepted", 
		approvalManager : "approve", 
		principalPaid : req.body.principalPaid,
		interestPaid : req.body.interestPaid,
		approvalManagerName : req.session.admin.name,
		approvalManagerId : req.session.admin._id,
		approvalManagerTime : moment().toISOString(),

	}).exec((err,paymentAccepted)=>{
		if(!err && paymentAccepted){
			Loan.findById(paymentAccepted.loanId).exec((err,loanDetails)=>{
				if(!err && loanDetails){
                    let currentPaymentDate = moment(loanDetails.nextPayment).unix() * 1000
					let nextPaymentDate = moment(currentPaymentDate).add(1, 'month').toISOString()
					
                    Loan.findByIdAndUpdate(paymentAccepted.loanId,{
						paymentNumber : paymentAccepted.paymentNumber,
						nextPayment : nextPaymentDate,
						$inc : {
							outstanding : -1 * paymentAccepted.amount,
							principalAmount : -1 * paymentAccepted.principalPaid,
							interest : -1 * paymentAccepted.interestPaid,
							earningsAndLosses : paymentAccepted.amount,
						},
                    }).exec((err,approved)=>{
                        if(!err && approved && approved.outstanding <= 0){
							Loan.findByIdAndUpdate(approved._id,{fullyPaidOn : moment().toISOString(), status: "paid"}).exec((err,doneUpdateFullyPaidLoan)=>{
								if(!err && doneUpdateFullyPaidLoan){
									User.findByIdAndUpdate(doneUpdateFullyPaidLoan.userId,{currentLoanId : ""}).exec((err,done)=>{
										if(!err && done){
											res.redirect('/manager/taskDebtCollection/pending')
										}else{
											res.json({status:400})
										}
									})
								}else{
									res.json({status:400})
								}
							})
						}else if(!err && approved){
							res.redirect('/manager/taskDebtCollection/pending')
                        }else{
                            res.json({status:400, err:err})
                        }
                    })
				}else{
					res.json({status:400})
				}
			})
		}else{
			res.json({status:400})
		}
	})
})

app.get('/collected', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact
	let searchByAgent = req.query.searchByAgent

	let queryPayment = {
		status : "accepted",  
	}
	searchByAgent && searchByAgent != "" ? queryPayment.approvalAgentName = searchByAgent : ""
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""

		Payment.find(queryPayment).exec((err,payments)=>{
			if(!err && payments){
				let allPayment = []
				async.each(payments,(eachPayment, callback)=>{	
					Loan.findById(eachPayment.loanId, {
						loanType:1,
						installment:1,
						outstanding:1,
						principalAmount:1,
						interest:1,
						userId:1,
						agent:1,
						agentId:1,
						nextPayment:1,
						status:1,
						interest : 1,
						interestRate : 1,
						principalAmount : 1,
					}).exec((err,loanDetails)=>{
						if(!err && loanDetails){
							let queryUser = {_id : eachPayment.userId}
							searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
				
							User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
								if(!err && userDetails){
									let collectedAmount = parseInt(eachPayment.amount* 100) / 100
										
									allPayment.push({
										paymentId : eachPayment._id,
										loanId : loanDetails._id,
										customerId : userDetails._id,
										customerName : userDetails.username,
										phoneNumber : userDetails.phoneNumber,
										loanType : loanDetails.loanType,
										installment : loanDetails.installment,
										collected : collectedAmount,
										outstanding : loanDetails.outstanding,
										agentId : loanDetails.agentId,
										agent : loanDetails.agent,
										paidBy : eachPayment.paidBy,
										status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
										// paymentDueDate : loanDetails.nextPayment,
										paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
										// paidOn : eachPayment.createdAt,
										paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
										paidOnGMT : eachPayment.createdAt,
										paymentProof : eachPayment.imageUrl,
										interest : loanDetails.interest,
										interestRate : loanDetails.interestRate,
										principalAmount : loanDetails.principalAmount,
										principalPaid : eachPayment.principalPaid,
										interestPaid : eachPayment.interestPaid,		
										// approvalManagerTime : eachPayment.approvalManagerTime,	
										approvalManagerTime : moment(eachPayment.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
										approvalManagerTimeGMT : eachPayment.approvalManagerTime,
										approvalAgent : eachPayment.approvalAgent == "approve" ? "Approved" : "Rejected",	
										
										
									})
									callback()
								}else{
									callback()
								}
							})
						}else{
							callback()
						}
					})
		
				},(err)=>{
					allPayment.sort((a,b)=>{
						return moment(a.paidOnGMT).unix() - moment(b.paidOnGMT).unix()
					})
					res.render('task/manager_DebtCollection/collected', {checkDebtCollection:"false", payments : allPayment, admin: req.session.admin });
				})
			}else{
				res.render('task/manager_DebtCollection/collected', {checkDebtCollection:"false", payments : [], admin: req.session.admin });
			}
		})
})

app.get('/collected/:paymentId', isLoggedIn, (req, res) => {
	let paymentId = req.params.paymentId
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact
	let searchByAgent = req.query.searchByAgent

	let queryPayment = {
		_id : paymentId,
		status : "accepted",  
	}
	searchByAgent && searchByAgent != "" ? queryPayment.approvalAgentName = searchByAgent : ""
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""

		Payment.find(queryPayment).exec((err,payments)=>{
			if(!err && payments){
				let allPayment = []
				async.each(payments,(eachPayment, callback)=>{	
					Loan.findById(eachPayment.loanId, {
						loanType:1,
						installment:1,
						outstanding:1,
						principalAmount:1,
						interest:1,
						userId:1,
						agent:1,
						agentId:1,
						nextPayment:1,
						status:1,
						interest : 1,
						interestRate : 1,
						principalAmount : 1,
					}).exec((err,loanDetails)=>{
						if(!err && loanDetails){
							let queryUser = {_id : eachPayment.userId}
							searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
				
							User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
								if(!err && userDetails){
									let collectedAmount = parseInt(eachPayment.amount* 100) / 100
										
									allPayment.push({
										paymentId : eachPayment._id,
										loanId : loanDetails._id,
										customerId : userDetails._id,
										customerName : userDetails.username,
										phoneNumber : userDetails.phoneNumber,
										loanType : loanDetails.loanType,
										installment : loanDetails.installment,
										collected : collectedAmount,
										outstanding : loanDetails.outstanding,
										agentId : loanDetails.agentId,
										agent : loanDetails.agent,
										paidBy : eachPayment.paidBy,
										status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
										// paymentDueDate : loanDetails.nextPayment,
										paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
										// paidOn : eachPayment.createdAt,
										paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
										paidOnGMT : eachPayment.createdAt,
										paymentProof : eachPayment.imageUrl,
										interest : loanDetails.interest,
										interestRate : loanDetails.interestRate,
										principalAmount : loanDetails.principalAmount,
										principalPaid : eachPayment.principalPaid,
										interestPaid : eachPayment.interestPaid,		
										// approvalManagerTime : eachPayment.approvalManagerTime,	
										approvalManagerTime : moment(eachPayment.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
										approvalManagerTimeGMT : eachPayment.approvalManagerTime,
										approvalAgent : eachPayment.approvalAgent == "approve" ? "Approved" : "Rejected",	
										
										
									})
									callback()
								}else{
									callback()
								}
							})
						}else{
							callback()
						}
					})
		
				},(err)=>{
					allPayment.sort((a,b)=>{
						return moment(a.paidOnGMT).unix() - moment(b.paidOnGMT).unix()
					})
					res.render('task/manager_DebtCollection/collected', {checkDebtCollection:"true", payments : allPayment, admin: req.session.admin });
				})
			}else{
				res.render('task/manager_DebtCollection/collected', {checkDebtCollection:"true", payments : [], admin: req.session.admin });
			}
		})
})

app.get('/rejected', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact
	let searchByAgent = req.query.searchByAgent

	let queryPayment = {
		status : "rejected", 
		approvalManager : "reject", 
	}
	searchByAgent && searchByAgent != "" ? queryPayment.approvalAgentName = searchByAgent : ""
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""

		Payment.find(queryPayment).exec((err,payments)=>{
			if(!err && payments){
				let allPayment = []
				async.each(payments,(eachPayment, callback)=>{	
					Loan.findById(eachPayment.loanId, {
						loanType:1,
						installment:1,
						outstanding:1,
						principalAmount:1,
						interest:1,
						userId:1,
						agent:1,
						agentId:1,
						nextPayment:1,
						status:1,
						interest : 1,
						interestRate : 1,
						principalAmount : 1,
					}).exec((err,loanDetails)=>{
						if(!err && loanDetails){
							let queryUser = {_id : eachPayment.userId}
							searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
				
							User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
								if(!err && userDetails){
									let collectedAmount = parseInt(eachPayment.amount* 100) / 100
										
									allPayment.push({
										paymentId : eachPayment._id,
										loanId : loanDetails._id,
										customerId : userDetails._id,
										customerName : userDetails.username,
										phoneNumber : userDetails.phoneNumber,
										loanType : loanDetails.loanType,
										installment : loanDetails.installment,
										collected : collectedAmount,
										outstanding : loanDetails.outstanding,
										agentId : loanDetails.agentId,
										agent : loanDetails.agent,
										paidBy : eachPayment.paidBy,
										status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
										// paymentDueDate : loanDetails.nextPayment,
										paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
										// paidOn : eachPayment.createdAt,
										paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
										paidOnGMT : eachPayment.createdAt,
										paymentProof : eachPayment.imageUrl,
										interest : loanDetails.interest,
										interestRate : loanDetails.interestRate,
										principalAmount : loanDetails.principalAmount,
										principalPaid : eachPayment.principalPaid,
										interestPaid : eachPayment.interestPaid,		
										// approvalManagerTime : eachPayment.approvalManagerTime,	
										approvalManagerTime : moment(eachPayment.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
										approvalManagerTimeGMT : eachPayment.approvalManagerTime,
										approvalAgent : eachPayment.approvalAgent == "approve" ? "Approved" : "Rejected",	
										rejectionReasonManager : eachPayment.rejectionReasonManager,
										
									})
									callback()
								}else{
									callback()
								}
							})
						}else{
							callback()
						}
					})
		
				},(err)=>{
					allPayment.sort((a,b)=>{
						return moment(a.paidOnGMT).unix() - moment(b.paidOnGMT).unix()
					})
					res.render('task/manager_DebtCollection/rejected', {checkDebtCollection:"false", payments : allPayment, admin: req.session.admin });
				})
			}else{
				res.render('task/manager_DebtCollection/rejected', {checkDebtCollection:"false", payments : [], admin: req.session.admin });
			}
		})
})

app.get('/rejected/:paymentId', isLoggedIn, (req, res) => {
	let paymentId = req.params.paymentId
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact
	let searchByAgent = req.query.searchByAgent

	let queryPayment = {
		_id : paymentId,
		status : "rejected", 
		approvalManager : "reject", 
	}
	searchByAgent && searchByAgent != "" ? queryPayment.approvalAgentName = searchByAgent : ""
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryPayment.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryPayment.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""

		Payment.find(queryPayment).exec((err,payments)=>{
			if(!err && payments){
				let allPayment = []
				async.each(payments,(eachPayment, callback)=>{	
					Loan.findById(eachPayment.loanId, {
						loanType:1,
						installment:1,
						outstanding:1,
						principalAmount:1,
						interest:1,
						userId:1,
						agent:1,
						agentId:1,
						nextPayment:1,
						status:1,
						interest : 1,
						interestRate : 1,
						principalAmount : 1,
					}).exec((err,loanDetails)=>{
						if(!err && loanDetails){
							let queryUser = {_id : eachPayment.userId}
							searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
				
							User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
								if(!err && userDetails){
									let collectedAmount = parseInt(eachPayment.amount* 100) / 100
										
									allPayment.push({
										paymentId : eachPayment._id,
										loanId : loanDetails._id,
										customerId : userDetails._id,
										customerName : userDetails.username,
										phoneNumber : userDetails.phoneNumber,
										loanType : loanDetails.loanType,
										installment : loanDetails.installment,
										collected : collectedAmount,
										outstanding : loanDetails.outstanding,
										agentId : loanDetails.agentId,
										agent : loanDetails.agent,
										paidBy : eachPayment.paidBy,
										status : loanDetails.outstanding == 0 ? "Paid" : "On Loan",
										// paymentDueDate : loanDetails.nextPayment,
										paymentDueDate : moment(loanDetails.nextPayment).add(8, 'hours').format('MMM Do, YYYY'),
										// paidOn : eachPayment.createdAt,
										paidOn : moment(eachPayment.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
										paidOnGMT : eachPayment.createdAt,
										paymentProof : eachPayment.imageUrl,
										interest : loanDetails.interest,
										interestRate : loanDetails.interestRate,
										principalAmount : loanDetails.principalAmount,
										principalPaid : eachPayment.principalPaid,
										interestPaid : eachPayment.interestPaid,		
										// approvalManagerTime : eachPayment.approvalManagerTime,	
										approvalManagerTime : moment(eachPayment.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
										approvalManagerTimeGMT : eachPayment.approvalManagerTime,
										approvalAgent : eachPayment.approvalAgent == "approve" ? "Approved" : "Rejected",	
										rejectionReasonManager : eachPayment.rejectionReasonManager, 
										
									})
									callback()
								}else{
									callback()
								}
							})
						}else{
							callback()
						}
					})
		
				},(err)=>{
					allPayment.sort((a,b)=>{
						return moment(a.paidOnGMT).unix() - moment(b.paidOnGMT).unix()
					})
					res.render('task/manager_DebtCollection/rejected', {checkDebtCollection:"true", payments : allPayment, admin: req.session.admin });
				})
			}else{
				res.render('task/manager_DebtCollection/rejected', {checkDebtCollection:"true", payments : [], admin: req.session.admin });
			}
		})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}