const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Loan = require('../../../models/loan/loan');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

// app.get('/', isLoggedIn, (req, res) => {
// 	let numberOfEachTask = {
// 		newApplication : 0,
// 		debtCollection :0,
// 		commissionWithdraw : 0
// 	}
// 	Loan.countDocuments({status : "new"}).exec((err, numberOfDocument)=>{
// 		console.log(numberOfDocument)
// 		numberOfEachTask.newApplication = numberOfDocument
// 		res.render('task/agent_NewApplication/task', { numberOfEachTask: numberOfEachTask, admin: req.session.admin });
// 	})
// })

app.get('/newApplication', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryLoan = {
		status : "new", 
		approvalAgent : "pending", 
		$or : [{agentId: req.session.admin._id},{agentId : null},{agentId : ""}]
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
		queryLoan.requestedOn = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryLoan.requestedOn = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryLoan.requestedOn = {$lte : moment(searchByEndDate).toISOString()} : ""
	// console.log(queryLoan)
	Loan.find(queryLoan).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan, callback)=>{
			let queryUser = {_id : eachLoan.userId}
			searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			// console.log(queryUser)
			User.findOne(queryUser,{username:1, phoneNumber:1, monthlyIncome:1}).exec((err,userDetails)=>{
				if(!err && userDetails/*  && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						monthlyIncome : userDetails.monthlyIncome,
						loanType : eachLoan.loanType,
						amount : eachLoan.amount,
						duration : eachLoan.duration + " " + eachLoan.paymentType,
						interest : eachLoan.interest,
						offset : eachLoan.offset,
						disbursement : eachLoan.disbursement,
						installment : eachLoan.installment,
						type : eachLoan.paymentType,
						// requestedOn : eachLoan.requestedOn,
						requestedOn : moment(eachLoan.requestedOn).add(8, 'hours').format('MMM Do, YYYY'),
						requestedOnGMT : eachLoan.requestedOn,
						agent : eachLoan.agent
					})
					callback()
				}else{
					callback()
				}
			})

		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.requestedOnGMT).unix() - moment(a.requestedOnGMT).unix()
			})
			res.render('task/agent_NewApplication/newApplication', {showRejectLoanModal:"false", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/approve/:loanId', isLoggedIn, (req,res)=>{
	let loanId = req.params.loanId
	Loan.findByIdAndUpdate(loanId, {approvalAgent : "approve", agentId : req.session.admin._id, agent:req.session.admin.name, approvalAgentTime : moment().toISOString()}).exec((err,loanDetails)=>{
		if(!err && loanDetails){	
			User.findByIdAndUpdate(loanDetails.userId,{agentId : req.session.admin._id, agent:req.session.admin.name}).exec((err,done)=>{
				if(!err && done){
					let message = {
						header : "Your loan have been registered.",
						content : "Your loan ID : " + loanId + " has been registered at " + moment().add(8, 'hours').format('MMM Do, YYYY') +  ". MrRinggit.",
					}
					pushNotification(loanDetails.userId,message)
					res.redirect('/agent/taskNewApplication/newApplication')
				}else{
					console.log(err)
					res.json({status:400})
				}
			})		
		}else{
			console.log(err)
			res.json({status:400})
		}
	})
})

app.get('/rejectingLoan/:loanId', isLoggedIn, (req,res)=>{
	let loanId = req.params.loanId
	Loan.findById(loanId).exec((err,loanDetails)=>{
		if(!err && loanDetails){
			User.findById(loanDetails.userId).exec((err,userDetails)=>{
				if(!err && userDetails){
					let allLoans = [{
						loanId : loanDetails._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						monthlyIncome : userDetails.monthlyIncome,
						loanType : loanDetails.loanType,
						amount : loanDetails.amount,
						duration : loanDetails.duration + " " + loanDetails.paymentType,
						interest : loanDetails.interest,
						offset : loanDetails.offset,
						disbursement : loanDetails.disbursement,
						installment : loanDetails.installment,
						type : loanDetails.paymentType,
						// requestedOn : loanDetails.requestedOn,
						requestedOn : moment(loanDetails.requestedOn).add(8, 'hours').format('MMM Do, YYYY'),
						requestedOnGMT : loanDetails.requestedOn,
						agent : loanDetails.agent
					}]
					res.render('task/agent_NewApplication/newApplication', {showRejectLoanModal : "true", loans : allLoans, admin: req.session.admin });
				}
			})
		}else{
			console.log(err)
			res.json({status:400})
		}
	})
})

app.post('/rejectingLoan/:loanId', isLoggedIn, (req,res)=>{
	// res.json({status:200, body:req.body})
	let loanId = req.params.loanId
	let rejectionReasonAgent = req.body.reason
	Loan.findByIdAndUpdate(loanId, {status : "rejected", approvalAgent : "reject", rejectionReasonAgent : rejectionReasonAgent, approvalAgentTime : moment().toISOString()}).exec((err,done)=>{
		if(!err && done){
			User.findByIdAndUpdate(done.userId,{currentLoanId : ""}).exec((err,done)=>{
				if(!err && done){
					res.redirect('/agent/taskNewApplication/newApplication')
				}else{
					res.json({status:400})
				}
			})
		}else{
			console.log(err)
			res.json({status:400})
		}
	})
})

app.get('/reject/:loanId', isLoggedIn, (req,res)=>{
	let loanId = req.params.loanId
	let rejectionReasonAgent = "High risk"
	Loan.findByIdAndUpdate(loanId, {status : "rejected", approvalAgent : "reject", rejectionReasonAgent : rejectionReasonAgent, approvalAgentTime : moment().toISOString()}).exec((err,done)=>{
		if(!err && done){
			User.findByIdAndUpdate(done.userId,{currentLoanId : ""}).exec((err,done)=>{
				if(!err && done){
					res.redirect('/agent/taskNewApplication/newApplication')
				}else{
					res.json({status:400})
				}
			})
		}else{
			console.log(err)
			res.json({status:400})
		}
	})
})

app.get('/approvedApplication', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryLoan = {
		status : "new", 
		approvalAgent : "approve", 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
		queryLoan.approvalAgentTime = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryLoan.approvalAgentTime = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryLoan.approvalAgentTime = {$lte : moment(searchByEndDate).toISOString()} : ""

	Loan.find(queryLoan).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan, callback)=>{
			let queryUser = {_id : eachLoan.userId}
			searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			User.findOne(queryUser,{username:1, phoneNumber:1, monthlyIncome:1}).exec((err,userDetails)=>{
				if(!err && userDetails /* && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						monthlyIncome : userDetails.monthlyIncome,
						loanType : eachLoan.loanType,
						amount : eachLoan.amount,
						duration : eachLoan.duration + " " + eachLoan.paymentType,
						interest : eachLoan.interest,
						offset : eachLoan.offset,
						disbursement : eachLoan.disbursement,
						installment : eachLoan.installment,
						type : eachLoan.paymentType,
						// requestedOn : eachLoan.requestedOn,
						requestedOn : moment(eachLoan.requestedOn).add(8, 'hours').format('MMM Do, YYYY'),
						requestedOnGMT : eachLoan.requestedOn,
						agent : eachLoan.agent,
						// approvalAgentTime : eachLoan.approvalAgentTime,
						approvalAgentTime : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalAgentTimeGMT : eachLoan.approvalAgentTime,
					})
					callback()
				}else{
					callback()
				}
			})

		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.approvalAgentTimeGMT).unix() - moment(a.approvalAgentTimeGMT).unix()
			})
			res.render('task/agent_NewApplication/approvedApplication', { loans : allLoans, admin: req.session.admin });
		})
	})
	// res.render('task/agent_NewApplication/approvedApplication', { admin: req.session.admin });
})

app.get('/customerDetails/personalInfo/:customerId/:loanId', isLoggedIn, (req, res) => {
	let customerId = req.params.customerId
	let loanId = req.params.loanId
	User.findById(customerId).exec((err,userDetails)=>{
		if(!err && userDetails){
			// console.log(userDetails)
			res.render('dashboard/customerInfoAgent/personalInfo', { loanId : loanId, userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

app.get('/customerDetails/residentialInfo/:customerId/:loanId', isLoggedIn, (req, res) => {
	let customerId = req.params.customerId
	let loanId = req.params.loanId
	User.findById(customerId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoAgent/residentialInfo', { loanId : loanId,  userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
	// res.render('dashboard/customerInfoAgent/residentialInfo', { admin: req.session.admin });
})

app.get('/customerDetails/bankAccountInfo/:customerId/:loanId', isLoggedIn, (req, res) => {
	let customerId = req.params.customerId
	let loanId = req.params.loanId
	User.findById(customerId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoAgent/bankAccountInfo', { loanId : loanId,  userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
	// res.render('dashboard/customerInfoAgent/bankAccountInfo', { admin: req.session.admin });
})

app.get('/customerDetails/workingInfo/:customerId/:loanId', isLoggedIn, (req, res) => {
	let customerId = req.params.customerId
	let loanId = req.params.loanId
	User.findById(customerId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoAgent/workingInfo', { loanId : loanId,  userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
	// res.render('dashboard/customerInfoAgent/workingInfo', { admin: req.session.admin });
})

app.get('/customerDetails/emergencyContactInfo/:customerId/:loanId', isLoggedIn, (req, res) => {
	let customerId = req.params.customerId
	let loanId = req.params.loanId
	User.findById(customerId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoAgent/emergencyContactInfo', { loanId : loanId,  userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
	// res.render('dashboard/customerInfoAgent/emergencyContactInfo', { admin: req.session.admin });
})

app.get('/loanDetail/:loanId', isLoggedIn, (req, res) => {
	let loanId = req.params.loanId
	Loan.findById(loanId).exec((err,loanDetails)=>{
		if(!err && loanDetails){
			res.render('loan/loanDetailAgent/loanDetail', { loanId : loanId,  loanDetails:loanDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

app.post('/updateLoan', isLoggedIn, (req, res) => {
	let loanId = req.body.loanId
	Loan.findByIdAndUpdate(loanId,req.body).exec((err,doneUpdate)=>{
		if(!err && doneUpdate){
			res.redirect('/agent/taskNewApplication/newApplication');
		}
	})
})

app.get('/rejectedApplication', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryLoan = {
		status : "rejected", 
		approvalAgent : "reject", 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
		queryLoan.approvalAgentTime = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryLoan.approvalAgentTime = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryLoan.approvalAgentTime = {$lte : moment(searchByEndDate).toISOString()} : ""

	Loan.find(queryLoan).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan, callback)=>{
			let queryUser = {_id : eachLoan.userId}
			searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			User.findOne(queryUser,{username:1, phoneNumber:1, monthlyIncome:1}).exec((err,userDetails)=>{
				if(!err && userDetails/*  && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						monthlyIncome : userDetails.monthlyIncome,
						loanType : eachLoan.loanType,
						amount : eachLoan.amount,
						duration : eachLoan.duration + " " + eachLoan.paymentType,
						interest : eachLoan.interest,
						offset : eachLoan.offset,
						disbursement : eachLoan.disbursement,
						installment : eachLoan.installment,
						type : eachLoan.paymentType,
						// requestedOn : eachLoan.requestedOn,
						requestedOn : moment(eachLoan.requestedOn).add(8, 'hours').format('MMM Do, YYYY'),
						requestedOnGMT : eachLoan.requestedOn,
						agent : eachLoan.agent,
						// approvalAgentTime : eachLoan.approvalAgentTime,
						approvalAgentTime : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalAgentTimeGMT : eachLoan.approvalAgentTime,
						rejectionReasonAgent : eachLoan.rejectionReasonAgent
					})
					callback()
				}else{
					callback()
				}
			})

		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.approvalAgentTimeGMT).unix() - moment(a.approvalAgentTimeGMT).unix()
			})
			res.render('task/agent_NewApplication/rejectedApplication', {rejectReasonModal:"false", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/rejectReason/:loanId', isLoggedIn, (req, res) => {
	let loanId = req.params.loanId
	Loan.find({
		_id : loanId,
		status : "rejected", 
		approvalAgent : "reject", 
	}).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan, callback)=>{
			User.findOne({_id : eachLoan.userId},{username:1, phoneNumber:1, monthlyIncome:1}).exec((err,userDetails)=>{
				if(!err && userDetails/*  && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						monthlyIncome : userDetails.monthlyIncome,
						loanType : eachLoan.loanType,
						amount : eachLoan.amount,
						duration : eachLoan.duration + " " + eachLoan.paymentType,
						interest : eachLoan.interest,
						offset : eachLoan.offset,
						disbursement : eachLoan.disbursement,
						installment : eachLoan.installment,
						type : eachLoan.paymentType,
						// requestedOn : eachLoan.requestedOn,
						requestedOn : moment(eachLoan.requestedOn).add(8, 'hours').format('MMM Do, YYYY'),
						requestedOnGMT : eachLoan.requestedOn,
						agent : eachLoan.agent,
						// approvalAgentTime : eachLoan.approvalAgentTime,
						approvalAgentTime : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalAgentTimeGMT : eachLoan.approvalAgentTime,
						rejectionReasonAgent : eachLoan.rejectionReasonAgent
					})
					callback()
				}else{
					callback()
				}
			})

		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.requestedOnGMT).unix() - moment(a.requestedOnGMT).unix()
			})
			res.render('task/agent_NewApplication/rejectedApplication', {rejectReasonModal:"true", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/loanDisbursedApproved', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryLoan = {
		status : "accepted", 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
		queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryLoan.approvalManagerTime = {$lte : moment(searchByEndDate).toISOString()} : ""

	Loan.find(queryLoan).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan, callback)=>{
			let queryUser = {_id : eachLoan.userId}
			searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			User.findOne(queryUser,{username:1, phoneNumber:1, bankName:1, bankAccountName:1, bankAccountNumber:1}).exec((err,userDetails)=>{
				if(!err && userDetails/*  && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						// monthlyIncome : userDetails.monthlyIncome,
						bankName : userDetails.bankName,
						bankAccountName : userDetails.bankAccountName,
						bankAccountNumber : userDetails.bankAccountNumber,
						loanType : eachLoan.loanType,
						amount : eachLoan.amount,
						duration : eachLoan.duration + " " + eachLoan.paymentType,
						interest : eachLoan.interest,
						offset : eachLoan.offset,
						disbursement : eachLoan.disbursement,
						installment : eachLoan.installment,
						type : eachLoan.paymentType,
						// requestedOn : eachLoan.requestedOn,
						requestedOn : moment(eachLoan.requestedOn).add(8, 'hours').format('MMM Do, YYYY'),
						requestedOnGMT : eachLoan.requestedOn,
						agent : eachLoan.agent,
						// approvalAgentTime : eachLoan.approvalAgentTime,
						approvalAgentTime : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalAgentTimeGMT : eachLoan.approvalAgentTime,
						rejectionReasonAgent : eachLoan.rejectionReasonAgent,
						// approvalManagerTime : eachLoan.approvalManagerTime,
						approvalManagerTime : moment(eachLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalManagerTimeGMT : eachLoan.approvalManagerTime,
						disbursementProof : eachLoan.disbursementProof
					})
					callback()
				}else{
					callback()
				}
			})

		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.approvalManagerTimeGMT).unix() - moment(a.approvalManagerTimeGMT).unix()
			})
			res.render('task/agent_NewApplication/loanDisbursedApproved', {receiptCheckModal:"false", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/checkDisbursementProof/:loanId', isLoggedIn, (req, res) => {
	let loanId = req.params.loanId
	Loan.find({
		_id : loanId,
		status : "accepted",  
	}).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan, callback)=>{
			User.findOne({_id : eachLoan.userId},{username:1, phoneNumber:1, bankName:1, bankAccountName:1, bankAccountNumber:1}).exec((err,userDetails)=>{
				if(!err && userDetails/*  && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						// monthlyIncome : userDetails.monthlyIncome,
						bankName : userDetails.bankName,
						bankAccountName : userDetails.bankAccountName,
						bankAccountNumber : userDetails.bankAccountNumber,
						loanType : eachLoan.loanType,
						amount : eachLoan.amount,
						duration : eachLoan.duration + " " + eachLoan.paymentType,
						interest : eachLoan.interest,
						offset : eachLoan.offset,
						disbursement : eachLoan.disbursement,
						installment : eachLoan.installment,
						type : eachLoan.paymentType,
						// requestedOn : eachLoan.requestedOn,
						requestedOn : moment(eachLoan.requestedOn).add(8, 'hours').format('MMM Do, YYYY'),
						requestedOnGMT : eachLoan.requestedOn,
						agent : eachLoan.agent,
						// approvalAgentTime : eachLoan.approvalAgentTime,
						approvalAgentTime : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalAgentTimeGMT : eachLoan.approvalAgentTime,
						rejectionReasonAgent : eachLoan.rejectionReasonAgent,
						// approvalManagerTime : eachLoan.approvalManagerTime,
						approvalManagerTime : moment(eachLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalManagerTimeGMT : eachLoan.approvalManagerTime,
						disbursementProof : eachLoan.disbursementProof
					})
					callback()
				}else{
					callback()
				}
			})

		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.requestedOnGMT).unix() - moment(a.requestedOnGMT).unix()
			})
			console.log(loans)
			res.render('task/agent_NewApplication/loanDisbursedApproved', {receiptCheckModal:"true", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/loanDisbursedRejected', isLoggedIn, (req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

	let queryLoan = {
		status : "rejected", 
		approvalManager : "reject",
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
		queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryLoan.approvalManagerTime = {$lte : moment(searchByEndDate).toISOString()} : ""
	Loan.find(queryLoan).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan, callback)=>{
			let queryUser = {_id : eachLoan.userId}
			searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
			User.findOne(queryUser,{username:1, phoneNumber:1, monthlyIncome:1}).exec((err,userDetails)=>{
				if(!err && userDetails/*  && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						monthlyIncome : userDetails.monthlyIncome,
						loanType : eachLoan.loanType,
						amount : eachLoan.amount,
						duration : eachLoan.duration + " " + eachLoan.paymentType,
						interest : eachLoan.interest,
						offset : eachLoan.offset,
						disbursement : eachLoan.disbursement,
						installment : eachLoan.installment,
						type : eachLoan.paymentType,
						// requestedOn : eachLoan.requestedOn,
						requestedOn : moment(eachLoan.requestedOn).add(8, 'hours').format('MMM Do, YYYY'),
						requestedOnGMT : eachLoan.requestedOn,
						agent : eachLoan.agent,
						// approvalAgentTime : eachLoan.approvalAgentTime,
						approvalAgentTime : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalAgentTimeGMT : eachLoan.approvalAgentTime,
						// approvalManagerTime : eachLoan.approvalManagerTime,
						approvalManagerTime : moment(eachLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalManagerTimeGMT : eachLoan.approvalManagerTime,
						rejectionReasonManager : eachLoan.rejectionReasonManager
					})
					callback()
				}else{
					callback()
				}
			})

		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.approvalManagerTimeGMT).unix() - moment(a.approvalManagerTimeGMT).unix()
			})
			res.render('task/agent_NewApplication/loanDisbursedRejected', {rejectReasonModal:"false", loans : allLoans, admin: req.session.admin });
		})
	})
})

app.get('/loanDisbursedRejected/:loanId', isLoggedIn, (req, res) => {
	let loanId = req.params.loanId
	Loan.find({
		_id : loanId,
		status : "rejected", 
		approvalManager : "reject", 
	}).exec((err,loans)=>{
		let allLoans = []
		async.each(loans,(eachLoan, callback)=>{
			User.findOne({_id : eachLoan.userId},{username:1, phoneNumber:1, monthlyIncome:1}).exec((err,userDetails)=>{
				if(!err && userDetails/*  && userDetails.username */){
					allLoans.push({
						loanId : eachLoan._id,
						customerId : userDetails._id,
						customerName : userDetails.username,
						phoneNumber : userDetails.phoneNumber,
						monthlyIncome : userDetails.monthlyIncome,
						loanType : eachLoan.loanType,
						amount : eachLoan.amount,
						duration : eachLoan.duration + " " + eachLoan.paymentType,
						interest : eachLoan.interest,
						offset : eachLoan.offset,
						disbursement : eachLoan.disbursement,
						installment : eachLoan.installment,
						type : eachLoan.paymentType,
						// requestedOn : eachLoan.requestedOn,
						requestedOn : moment(eachLoan.requestedOn).add(8, 'hours').format('MMM Do, YYYY'),
						requestedOnGMT : eachLoan.requestedOn,
						agent : eachLoan.agent,
						// approvalAgentTime : eachLoan.approvalAgentTime,
						approvalAgentTime : moment(eachLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalAgentTimeGMT : eachLoan.approvalAgentTime,
						// approvalManagerTime : eachLoan.approvalManagerTime,
						approvalManagerTime : moment(eachLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
						approvalManagerTimeGMT : eachLoan.approvalManagerTime,
						rejectionReasonManager : eachLoan.rejectionReasonManager
					})
					callback()
				}else{
					callback()
				}
			})

		},(err)=>{
			allLoans.sort((a,b)=>{
				return moment(b.requestedOnGMT).unix() - moment(a.requestedOnGMT).unix()
			})
			res.render('task/agent_NewApplication/loanDisbursedRejected', {rejectReasonModal:"true", loans : allLoans, admin: req.session.admin });
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}