const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

// app.get('/newApplication', isLoggedIn, (req, res) => {
// 	res.render('task/agent/newApplication', { admin: req.session.admin });
// })

module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}