const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');
let Expenses = require('../../../models/expenses/expenses');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	res.redirect('/manager/expensesOverall')
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		return next();
	}
	res.redirect('/login');
}