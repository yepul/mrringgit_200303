const express = require('express');
const async = require('async')
const moment = require('moment')
const multiparty = require('connect-multiparty');

let Language = require('../../../models/language/language');
let Expenses = require('../../../models/expenses/expenses');
let Admin = require('../../../models/admin/admin');
let Loan = require('../../../models/loan/loan');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	let expensesOverall = []
    Language.find({type: {$in : ["header","expenses","expensesList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
		},(err)=>{
			let agentIdAll = []
			Expenses.find({status:"paid"},{agentId:1}).exec((err,agentIds)=>{
				async.each(agentIds,(eachAgentIds,callback)=>{
					if(eachAgentIds && eachAgentIds.agentId){
						agentIdAll.push(eachAgentIds.agentId)
						callback()
					}else{
						callback()
					}
				},(err)=>{
					Admin.find({_id: {$in : agentIdAll}},{name:1, amountHold:1}).exec((err,agentList)=>{
						async.each(agentList,(eachAgent,callback)=>{
							let lendOut = 0
							let bonus = 0
							let totalExpenses = 0
							let totalExpensesBonus = 0
							let createdAtGMT = ""
                            let paidAtGMT = ""
                            let paymentProof = ""
		
							Loan.find({agentId : eachAgent._id, status: "accepted"},{duration:1,installment:1}).exec((err,loanAll)=>{
								async.each(loanAll,(eachLoan,callback)=>{
									lendOut += eachLoan.installment * parseInt(eachLoan.duration)
									callback()
								},(err)=>{
									Expenses.find({agentId : eachAgent._id}).exec((err,expensesAll)=>{
										async.each(expensesAll,(eachExpenses,callback)=>{
											bonus += eachExpenses.type == "bonus" ? eachExpenses.expenses : 0
											totalExpenses += eachExpenses.type == "expenses" ? eachExpenses.expenses : 0
											totalExpensesBonus += eachExpenses.expenses
                                            createdAtGMT = eachExpenses.createdAt
                                            paidAtGMT = eachExpenses.approvalManagerTime
                                            paymentProof = eachExpenses.disbursementProof
										// 	callback()
										// },(err)=>{
											expensesOverall.push({
												agentId : eachAgent._id,
												agentName : eachAgent.name,
												lendOut : parseInt(lendOut*100)/100,
												totalExpenses : parseInt(totalExpenses*100)/100,
												bonus: parseInt(bonus*100)/100,
												totalExpensesBonus : parseInt(totalExpensesBonus*100)/100,
												principal: parseInt(eachAgent.amountHold*100)/100,
												createdAt : moment(createdAtGMT).add(8, 'hours').format('MMM Do, YYYY'),
												createdAtGMT : createdAtGMT,
												paidAt : moment(paidAtGMT).add(8, 'hours').format('MMM Do, YYYY'),
                                                paidAtGMT : paidAtGMT,
                                                paymentProof : paymentProof ? (paymentProof.indexOf("http") > 0 ? req.headers.host + paymentProof : "http://" + req.headers.host + paymentProof) : "-"
											})
											callback()
										},(err)=>{
											callback()
										})
									})
								})
							})
						},(err)=>{
							// res.json(expensesOverall)
							expensesOverall.sort((a,b)=>{
								return b.totalExpenses - a.totalExpenses
							})
							res.render('expenses/manager/bankedIn',{
								langData:langData, 
								expenses: expensesOverall, 
                                admin: req.session.admin,
							})
						})
					})
				})
			})
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin && req.session.admin.level == "manager") {
		return next();
	}
	res.redirect('/login');
}