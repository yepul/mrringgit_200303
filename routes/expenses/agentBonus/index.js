const express = require('express');
const async = require('async')
const moment = require('moment')
const multiparty = require('connect-multiparty');

let Admin = require('../../../models/admin/admin');
let Expenses = require('../../../models/expenses/expenses');
let Language = require('../../../models/language/language');
let Loan = require('../../../models/loan/loan');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	let principal = 0
	let bonus = 0
    let langData = {}
    Language.find({type: {$in : ["header","expenses","expensesList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
		},(err)=>{
			Expenses.find({status:"paid", type:"bonus", agentId:req.session.admin._id}).exec((err,allExpenses)=>{
				let expenses = []
				async.each(allExpenses,(eachExpenses,callback)=>{
                    bonus += eachExpenses.expenses
					expenses.push({
						expensesId : eachExpenses._id,
						expenses : eachExpenses.expenses,
						category : eachExpenses.category,
						remark : eachExpenses.remark,
						createdAt : moment(eachExpenses.createdAt).add(8, 'hours').format('MMM Do, YYYY'),
						createdAtGMT : eachExpenses.createdAt,
                        disbursementProof : eachExpenses.disbursementProof
					})
					callback()
				},(err)=>{
					Admin.findById(req.session.admin._id,{amountHold:1}).exec((err,adminDetails)=>{
						principal = parseInt(adminDetails.amountHold * 100)/100
						bonus = parseInt(bonus * 100)/100
						res.render('expenses/agent/bonus',{langData:langData, bonus: bonus, principal: principal, expenses: expenses, admin: req.session.admin})
					})
				})
			})
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin && req.session.admin.level == "agent") {
		return next();
	}
	res.redirect('/login');
}