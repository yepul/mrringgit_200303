const express = require('express');
const async = require('async')
const moment = require('moment')
const multiparty = require('connect-multiparty');

let Language = require('../../../models/language/language');
let Expenses = require('../../../models/expenses/expenses');
let Admin = require('../../../models/admin/admin');
let Loan = require('../../../models/loan/loan');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	let expensesOverall = []
    Language.find({type: {$in : ["header","expenses","expensesList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
		},(err)=>{
			Admin.find({level : "agent"},{bankDetail:1, name:1, amountHold:1}).exec((err,agentList)=>{
				async.each(agentList,(eachAgent,callback)=>{
					let lendOut = 0
					let bonus = 0
					let totalExpenses = 0
					let totalExpensesBonus = 0

					Loan.find({agentId : eachAgent._id, status: "accepted"},{duration:1,installment:1}).exec((err,loanAll)=>{
						async.each(loanAll,(eachLoan,callback)=>{
							lendOut += eachLoan.installment * parseInt(eachLoan.duration)
							callback()
						},(err)=>{
							Expenses.find({ agentId : eachAgent._id}).exec((err,expensesAll)=>{
								let expensesId = ""
								async.each(expensesAll,(eachExpenses,callback)=>{
									bonus += eachExpenses.type == "bonus" ? eachExpenses.expenses : 0
									totalExpenses += eachExpenses.type == "expenses" ? eachExpenses.expenses : 0
									totalExpensesBonus += eachExpenses.expenses
									expensesId = eachExpenses._id
									callback()
								},(err)=>{
									expensesOverall.push({
										expensesId : expensesId,
										agentId : eachAgent._id,
										agentName : eachAgent.name,
										agentBankName : eachAgent.bankDetail.bankName,
										agentBankAccountName : eachAgent.bankDetail.bankAccountName,
										agentBankAccountNumber : eachAgent.bankDetail.bankAccountNumber,
										lendOut : parseInt(lendOut*100)/100,
										totalExpenses : parseInt(totalExpenses*100)/100,
										bonus: parseInt(bonus*100)/100,
										totalExpensesBonus : parseInt(totalExpensesBonus*100)/100,
										principal: parseInt(eachAgent.amountHold*100)/100,
									})
									callback()
								})
							})
						})
					})
				},(err)=>{
					// res.json(expensesOverall)
					expensesOverall.sort((a,b)=>{
						return b.totalExpenses - a.totalExpenses
					})
					res.render('expenses/manager/overall',{
						langData:langData, 
						expenses: expensesOverall, 
						admin: req.session.admin,
						giveOutBonusModel : "false"
					})
				})
			})
		})
	})
})

app.get('/:expensesId', isLoggedIn, (req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	let expensesOverall = []
	let expensesId = req.params.expensesId
    Language.find({type: {$in : ["header","expenses","expensesList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
		},(err)=>{
			Admin.find({level : "agent"},{bankDetail:1, name:1, amountHold:1}).exec((err,agentList)=>{
				async.each(agentList,(eachAgent,callback)=>{
					let lendOut = 0
					let bonus = 0
					let totalExpenses = 0
					let totalExpensesBonus = 0

					Loan.find({agentId : eachAgent._id, status: "accepted"},{duration:1,installment:1}).exec((err,loanAll)=>{
						async.each(loanAll,(eachLoan,callback)=>{
							lendOut += eachLoan.installment * parseInt(eachLoan.duration)
							callback()
						},(err)=>{
							Expenses.find({_id:expensesId, agentId : eachAgent._id}).exec((err,expensesAll)=>{
								async.each(expensesAll,(eachExpenses,callback)=>{
									bonus += eachExpenses.type == "bonus" ? eachExpenses.expenses : 0
									totalExpenses += eachExpenses.type == "expenses" ? eachExpenses.expenses : 0
									totalExpensesBonus += eachExpenses.expenses
									callback()
								},(err)=>{
									expensesOverall.push({
										agentId : eachAgent._id,
										agentName : eachAgent.name,
										agentBankName : eachAgent.bankDetail.bankName,
										agentBankAccountName : eachAgent.bankDetail.bankAccountName,
										agentBankAccountNumber : eachAgent.bankDetail.bankAccountNumber,
										lendOut : parseInt(lendOut*100)/100,
										totalExpenses : parseInt(totalExpenses*100)/100,
										bonus: parseInt(bonus*100)/100,
										totalExpensesBonus : parseInt(totalExpensesBonus*100)/100,
										principal: parseInt(eachAgent.amountHold*100)/100,
									})
									callback()
								})
							})
						})
					})
				},(err)=>{
					// res.json(expensesOverall)
					expensesOverall.sort((a,b)=>{
						return b.totalExpenses - a.totalExpenses
					})
					res.render('expenses/manager/overall',{
						langData:langData, 
						expenses: expensesOverall, 
						admin: req.session.admin,
						giveOutBonusModel : "true"
					})
				})
			})
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin && req.session.admin.level == "manager") {
		return next();
	}
	res.redirect('/login');
}