const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let Loan = require('../../../models/loan/loan');
let Admin = require('../../../models/admin/admin');
let Expenses = require('../../../models/expenses/expenses');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	let principal = 0
    Language.find({type: {$in : ["header","expenses","expensesList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
		},(err)=>{
			Admin.findById(req.session.admin._id,{bankDetail:1,reason:1,amountHold:1}).exec((err,adminDetails)=>{
				principal = parseInt(adminDetails.amountHold * 100)/100
				res.render('expenses/agent/applyNewExpenses',{langData : langData, principal: principal, adminDetails: adminDetails, admin: req.session.admin})
			})
		})
	})
})

// app.post('/', isLoggedIn, (req, res) => {
// 	let newExpenses = new Expenses({
// 		expenses : req.body.amount,
// 		type : "expenses",
// 		status : "pending",
// 		agentId : req.body.agentId,
// 		category : req.body.category,
// 		remark : req.body.remark,
// 		expensesProof : req.body.expensesProof,
// 	})
// 	newExpenses.save((err,done)=>{
// 		res.redirect('/agent/expensesPending?new=success')
// 	})
// })

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin && req.session.admin.level == "agent") {
		return next();
	}
	res.redirect('/login');
}