const express = require('express');
const async = require('async')
const moment = require('moment')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');
let Loan = require('../../../models/loan/loan');
let Payment = require('../../../models/payment/payment');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let valueOfEachReport = {
		agentPerformance : 0,
		principal : 0,
		lendOut : 0,
		earnings : 0,
		outstanding : 0
    }
    
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","report","reportList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			Payment.find({status : "accepted"},{principalPaid:1,amount:1}).exec((err,paymentReceived)=>{
				async.each(paymentReceived,(eachPayment,callback)=>{
					// valueOfEachReport.principal += eachPayment.principalPaid
					valueOfEachReport.earnings += eachPayment.amount
					callback()
				},(err)=>{
					Loan.find({status : "accepted"},{disbursement:1,outstanding:1}).exec((err,loanApproved)=>{
						async.each(loanApproved,(eachLoan,callback)=>{
							valueOfEachReport.lendOut += eachLoan.disbursement
							valueOfEachReport.outstanding += eachLoan.outstanding
							callback()
						},(err)=>{
							parseInt(valueOfEachReport.principal * 100) / 100
							parseInt(valueOfEachReport.earnings * 100) / 100
							parseInt(valueOfEachReport.lendOut * 100) / 100
							parseInt(valueOfEachReport.outstanding * 100) / 100
		
							res.render('report/manager/report',{langData: langData, valueOfEachReport : valueOfEachReport, admin : req.session.admin});
						})
					})
				})
			})
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}