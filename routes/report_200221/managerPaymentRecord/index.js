const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/:loanId', isLoggedIn,(req, res) => {
    let loanId = req.params.loanId

    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type: {$in : ["header","report","reportList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            Loan.findById(loanId).exec((err,loanDetails)=>{
                let disbursement = parseInt(loanDetails.disbursement * 100) / 100
                let outstanding = parseInt(loanDetails.outstanding * 100) / 100
                let allPaymentRecord = []
                Payment.find({loanId : loanId, status:"accepted"}).exec((err,allPayment)=>{
                    async.each(allPayment,(eachPayment,callback)=>{
                        allPaymentRecord.push({
                            paymentId : eachPayment._id,
                            status : eachPayment.status == "accepted" ? "Paid" : "Unpaid",
                            agent : loanDetails.agent,
                            agentId : loanDetails.agentId,
                            installment : parseInt(loanDetails.installment * 100) / 100,
                            paymentDueDate : eachPayment.paymentDueDate,
                            amount : parseInt(eachPayment.amount * 100) / 100,
                            createdAt : eachPayment.createdAt,
                            paymentProof : eachPayment.imageUrl,
                        })
                        callback()
                    },(err)=>{
                        allPaymentRecord.sort((a,b)=>{
                            return moment(b.createdAt).unix() - moment(a.createdAt).unix()
                        })
                        res.render('report/manager/paymentRecord',{langData: langData, paymentProofModal: "false", loanId: loanId, disbursement: disbursement, outstanding: outstanding, allPaymentRecord: allPaymentRecord, admin: req.session.admin});
                    })
                })
            })
        })
    })

})

app.get('/:loanId/:paymentId', isLoggedIn,(req, res) => {
    let loanId = req.params.loanId
    let paymentId = req.params.paymentId

    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type: {$in : ["header","report","reportList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            Loan.findById(loanId).exec((err,loanDetails)=>{
                let disbursement = parseInt(loanDetails.disbursement * 100) / 100
                let outstanding = parseInt(loanDetails.outstanding * 100) / 100
                let allPaymentRecord = []
                Payment.find({_id: paymentId, loanId : loanId, status:"accepted"}).exec((err,allPayment)=>{
                    async.each(allPayment,(eachPayment,callback)=>{
                        allPaymentRecord.push({
                            paymentId : eachPayment._id,
                            status : eachPayment.status == "accepted" ? "Paid" : "Unpaid",
                            agent : loanDetails.agent,
                            agentId : loanDetails.agentId,
                            installment : parseInt(loanDetails.installment * 100) / 100,
                            paymentDueDate : eachPayment.paymentDueDate,
                            amount : parseInt(eachPayment.amount * 100) / 100,
                            createdAt : eachPayment.createdAt,
                            paymentProof : eachPayment.imageUrl,
                        })
                        callback()
                    },(err)=>{
                        allPaymentRecord.sort((a,b)=>{
                            return moment(b.createdAt).unix() - moment(a.createdAt).unix()
                        })
                        res.render('report/manager/paymentRecord',{langData: langData, paymentProofModal: "true", loanId: loanId, disbursement: disbursement, outstanding: outstanding, allPaymentRecord: allPaymentRecord, admin: req.session.admin});
                    })
                })
            })
        })
    })

})

module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}