const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact
	let totalLendOut = 0

	let queryLoan = {
		status : "accepted", 
		agentId : req.session.admin._id, 
	}
	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryLoan.approvalManagerTime = {$lte : moment(searchByEndDate).toISOString()} : ""

	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","report","reportList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			Loan.find(queryLoan).exec((err,loanApproved)=>{
				let allLendOut = []
				async.each(loanApproved,(eachLoan,callback)=>{
					totalLendOut += eachLoan.disbursement
		
					let collected = 0
					Payment.find({loanId : eachLoan._id, status : "accepted"},{principalPaid:1,amount:1}).exec((err,allPayment)=>{
						async.each(allPayment,(eachPayment,callback)=>{
							collected += eachPayment.amount
							callback()
						},(err)=>{
							let queryUser = {_id : eachLoan.userId}
							searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
							User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
								if(!err && userDetails){
									allLendOut.push({
										loanId : eachLoan._id,
										username : userDetails.username,
										userId : userDetails._id,
										phoneNumber : userDetails.phoneNumber,
										loanType : eachLoan.loanType,
										approvedOn : eachLoan.approvalManagerTime,
										amount : parseInt(eachLoan.amount * 100) / 100,
										duration : eachLoan.duration + " " + eachLoan.paymentType,
										interest : parseInt(eachLoan.interest * 100) / 100,
										offset : parseInt(eachLoan.offset * 100) / 100,
										disbursement : parseInt(eachLoan.disbursement * 100) / 100,
										collected : parseInt(collected * 100) / 100,
										outstanding : parseInt(eachLoan.outstanding * 100) / 100,
										agent : eachLoan.agent,
										agentId : eachLoan.agentId
									})
									callback()
								}else{
									callback()
								}
							})
						})
					})
				},(err)=>{
					allLendOut.sort((a,b)=>{
						return moment(b.approvedOn).unix() - moment(a.approvedOn).unix()
					})
					// res.json(allLendOut)
					res.render('report/agent/lendOut',{langData: langData, totalLendOut: totalLendOut, allLendOut: allLendOut, admin: req.session.admin});
				})
			})
		})
	})
})



module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}