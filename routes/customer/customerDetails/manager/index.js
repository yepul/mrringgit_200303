const express = require('express');
const async = require('async')
const moment = require('moment')
const multiparty = require('connect-multiparty');

let User = require('../../../../models/user/user');
let Loan = require('../../../../models/loan/loan');
let Payment = require('../../../../models/payment/payment');
let Language = require('../../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/personalInfo/:userId', isLoggedIn,(req, res) => {
    let userId = req.params.userId
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","customer","customerList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			User.findById(userId).exec((err,userDetails)=>{
				if(!err && userDetails){
					res.render('dashboard/customerInfoManager/personalInfoView', {langData: langData, userDetails:userDetails, admin: req.session.admin });
				}else{
					res.json({status:400})
				}
			})
		})
	})
})

app.get('/residentialInfo/:userId', isLoggedIn,(req, res) => {
    let userId = req.params.userId
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","customer","customerList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			User.findById(userId).exec((err,userDetails)=>{
				if(!err && userDetails){
					res.render('dashboard/customerInfoManager/residentialInfoView', {langData: langData, userDetails:userDetails, admin: req.session.admin });
				}else{
					res.json({status:400})
				}
			})
		})
	})
})

app.get('/bankAccountInfo/:userId', isLoggedIn,(req, res) => {
    let userId = req.params.userId
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","customer","customerList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			User.findById(userId).exec((err,userDetails)=>{
				if(!err && userDetails){
					res.render('dashboard/customerInfoManager/bankAccountInfoView', {langData: langData, userDetails:userDetails, admin: req.session.admin });
				}else{
					res.json({status:400})
				}
			})
		})
	})
})

app.get('/workingInfo/:userId', isLoggedIn,(req, res) => {
    let userId = req.params.userId
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","customer","customerList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			User.findById(userId).exec((err,userDetails)=>{
				if(!err && userDetails){
					res.render('dashboard/customerInfoManager/workingInfoView', {langData: langData, userDetails:userDetails, admin: req.session.admin });
				}else{
					res.json({status:400})
				}
			})
		})
	})
})

app.get('/emergencyContactInfo/:userId', isLoggedIn,(req, res) => {
    let userId = req.params.userId
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","customer","customerList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			User.findById(userId).exec((err,userDetails)=>{
				if(!err && userDetails){
					res.render('dashboard/customerInfoManager/emergencyContactInfoView', {langData: langData, userDetails:userDetails, admin: req.session.admin });
				}else{
					res.json({status:400})
				}
			})
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}