const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

    let queryLoan = {status : "paid"}

    searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
    queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
        searchByStartDate && searchByStartDate != "" ? 
        queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString()} :
        searchByEndDate && searchByEndDate != "" ? 
        queryLoan.approvalManagerTime = {$lte : moment(searchByEndDate).toISOString()} : ""

    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type: {$in : ["header","customer","customerList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            Loan.find(queryLoan).exec((err,allLoan)=>{
                let allOnLoan = []
                async.each(allLoan,(eachLoan,callback)=>{
                    Payment.find({loanId: eachLoan._id, status: "accepted"},{amount:1, interestPaid:1, principalPaid:1}).exec((err,allPayment)=>{
                        
                        let collected = 0
                        async.each(allPayment,(eachPayment,callback)=>{
                            collected += eachPayment.amount
                            callback()
                        },(err)=>{
                            let queryUser = {_id : eachLoan.userId}
                            searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
                            User.findOne(queryUser,{username: 1, phoneNumber: 1, status: 1}).exec((err,userDetails)=>{
                                if(!err && userDetails){
                                    allOnLoan.push({
                                        loanId : eachLoan._id,
                                        userId : userDetails._id,
                                        username : userDetails.username,
                                        phoneNumber : userDetails.phoneNumber,
                                        loanType : eachLoan.loanType,
                                        // approvedAt : eachLoan.approvalManagerTime,
                                        approvedAt : moment(eachLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
                                        approvedAtGMT : eachLoan.approvalManagerTime,
                                        amount : parseInt(eachLoan.amount * 100) / 100,
                                        duration : eachLoan.duration + " " + eachLoan.paymentType,
                                        installment : parseInt(eachLoan.installment * 100) / 100,
                                        collected : parseInt(collected * 100) / 100,
                                        outstanding : parseInt(eachLoan.outstanding * 100) / 100,
                                        numberOverdue : eachLoan.numberOverdue,
                                        dayOverdue : eachLoan.dayOverdue,
                                        amountOverdue : parseInt(eachLoan.amountOverdue * 100) / 100,
                                        earningsAndLosses : parseInt(eachLoan.earningsAndLosses * 100) / 100,
                                        agent : eachLoan.agent,
                                        agentId : eachLoan.agentId,
                                        status : userDetails.status == "loanApplication" ? "Normal" : "Not Normal",  
                                        fine : eachLoan.fine,   
                                        offset : eachLoan.offset,    
                                        // fullyPaidOn : eachLoan.fullyPaidOn,  
                                        fullyPaidOn : moment(eachLoan.fullyPaidOn).add(8, 'hours').format('MMM Do, YYYY'), 
                                        fullyPaidOnGMT : eachLoan.fullyPaidOn,                           
                                    })
                                    callback()
                                }else{
                                    callback()
                                }
                            })
                        })
                    })
                },(err)=>{
                    allOnLoan.sort((a,b)=>{
                        return moment(b.approvedAtGMT).unix() - moment(a.approvedAtGMT).unix()
                    })
                    res.render('customer/manager/paid', {langData: langData, allOnLoan: allOnLoan, admin: req.session.admin});
                })
            })
        })
    })
})



module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}