const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact
    let allRejected = []

    let queryLoan = {status : "rejected"}

    searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
    queryLoan.rejectedAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
        searchByStartDate && searchByStartDate != "" ? 
        queryLoan.rejectedAt = {$gte : moment(searchByStartDate).toISOString()} :
        searchByEndDate && searchByEndDate != "" ? 
        queryLoan.rejectedAt = {$lte : moment(searchByEndDate).toISOString()} : ""

    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type: {$in : ["header","customer","customerList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            Loan.find(queryLoan).exec((err,allRejectedLoan)=>{
                async.each(allRejectedLoan,(eachRejectedLoan,callback)=>{
                    let queryUser = {_id : eachRejectedLoan.userId}
                    searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
                    User.findOne(queryUser).exec((err,userDetails)=>{
                        let unpaid = 0
                        let paid = 0
                        let earningsAndLosses = 0
                        let numberOverdue = 0
                        let dayOverdue = 0
            
                        Payment.countDocuments({status : "accepted", userId : userDetails._id}).exec((err,noOfPaid)=>{
                            paid = noOfPaid;
                            Loan.countDocuments({status : {$ne : "paid"}, userId : userDetails._id}).exec((err,noOfActiveLoan)=>{
                                Loan.find({userId : userDetails._id, status : {$ne : "paid"}}).exec((err,allLoan)=>{
                                    async.each(allLoan,(eachLoan,callback)=>{
                                        unpaid += eachLoan.numberOverdue
                                        earningsAndLosses += eachLoan.earningsAndLosses
                                        numberOverdue += eachLoan.numberOverdue
                                        dayOverdue += eachLoan.dayOverdue
                                        callback()
                                    },(err)=>{
                                        allRejected.push({
                                            userId : userDetails._id,
                                            username : userDetails.username,
                                            // rejectedAt : eachRejectedLoan.approvalAgent == "reject" ? eachRejectedLoan.approvalAgentTime : eachRejectedLoan.approvalManagerTime,
                                            rejectedAt : eachRejectedLoan.approvalAgent == "reject" ? moment(eachRejectedLoan.approvalAgentTime).add(8, 'hours').format('MMM Do, YYYY') : moment(eachRejectedLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
                                            rejectedAtGMT : eachRejectedLoan.approvalAgent == "reject" ? eachRejectedLoan.approvalAgentTime : eachRejectedLoan.approvalManagerTime,
                                            phoneNumber : userDetails.phoneNumber,
                                            ic : userDetails.ic,
                                            unpaid : unpaid,
                                            paid : paid,
                                            earningsAndLosses : earningsAndLosses,
                                            numberOverdue : numberOverdue,
                                            dayOverdue : dayOverdue,
                                            agent : eachRejectedLoan.agent,
                                            agentId : eachRejectedLoan.agentId,
                                            stage : noOfActiveLoan > 0 ? "On Loan" : "In Progress",
                                            status : "Rejected",
                                            remark : eachRejectedLoan.approvalAgent == "reject" ? eachRejectedLoan.rejectionReasonAgent : eachRejectedLoan.rejectionReasonManager,
        
                                        })
                                        callback()
                                    })
                                })
                            })
                        })
                    })
                },(err)=>{
                    allRejected.sort((a,b)=>{
                        return moment(b.rejectedAtGMT).unix() - moment(a.rejectedAtGMT).unix()
                    })
                    res.render('customer/agent/rejected', {langData: langData, allRejected: allRejected, admin: req.session.admin});
                })
            })
        })
    })
})



module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}