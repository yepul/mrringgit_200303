const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

    let queryUser = {status : "blacklisted"}
    
    searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
    searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
    queryUser.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
        searchByStartDate && searchByStartDate != "" ? 
        queryUser.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
        searchByEndDate && searchByEndDate != "" ? 
        queryUser.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""

    let allBlacklisted = []
    let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type: {$in : ["header","customer","customerList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
            User.find(queryUser).exec((err,allUser)=>{
                let unpaid = 0
                let paid = 0
                let earningsAndLosses = 0
                let numberOverdue = 0
                let dayOverdue = 0
                async.each(allUser,(eachUser,callback)=>{
                    Payment.countDocuments({status : "accepted", userId : eachUser._id}).exec((err,noOfPaid)=>{
                        paid = noOfPaid;
                        Loan.countDocuments({status : {$ne : "paid"}, userId : eachUser._id}).exec((err,noOfActiveLoan)=>{
                            Loan.find({userId: eachUser._id, status : {$ne : "paid"}}).exec((err,allLoan)=>{
                                async.each(allLoan,(eachLoan,callback)=>{
                                    unpaid += eachLoan.numberOverdue
                                    earningsAndLosses += eachLoan.earningsAndLosses
                                    numberOverdue += eachLoan.numberOverdue
                                    dayOverdue += eachLoan.dayOverdue
                                    callback()
                                },(err)=>{
                                    allBlacklisted.push({
                                        userId : eachUser.userId,
                                        username : eachUser.username,
                                        // blacklistedAt : eachUser.blacklistedAt,
                                        blacklistedAt : moment(eachUser.blacklistedAt).add(8, 'hours').format('MMM Do, YYYY'),
                                        blacklistedAtGMT : eachUser.blacklistedAt,
                                        phoneNumber : eachUser.phoneNumber,
                                        ic : eachUser.ic,
                                        unpaid : unpaid,
                                        paid : paid,
                                        earningsAndLosses : earningsAndLosses,
                                        numberOverdue : numberOverdue,
                                        dayOverdue : dayOverdue,
                                        agent : eachUser.agent,
                                        agentId : eachUser.agentId,
                                        stage : noOfActiveLoan > 0 ? "On Loan" : "In Progress",
                                        status : "Blacklisted",
                                    })
                                    callback()
                                })
                            })
                        })
                    })
                },(err)=>{
                    allBlacklisted.sort((a,b)=>{
                        return moment(b.blacklistedAtGMT).unix() - moment(a.blacklistedAtGMT).unix()
                    })
                    res.render('customer/manager/blacklisted', {langData: langData, allBlacklisted: allBlacklisted, admin: req.session.admin});
                })
            })
        })
    })
})



module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}