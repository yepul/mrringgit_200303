const express = require('express');
const async = require('async')
const bcrypt = require('bcrypt');
const multiparty = require('connect-multiparty');

let Admin = require('../../models/admin/admin');
let Language = require('../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', (req, res) => {
    let adminId = req.query.id
    res.render('forceResetPassword/index', {adminId: adminId });
})

app.post('/', (req, res) => {
    let currentPassword = req.body.currentPassword
    let newPassword = req.body.newPassword
    let retypePassword = req.body.retypePassword
    let adminId = req.body.adminId

    if (currentPassword && (newPassword == retypePassword)) {
        Admin.findOne({ _id: adminId }).exec((err, admin) => {
            if (bcrypt.compareSync(currentPassword, admin.password)) {
                let plainPassword = newPassword
                let saltRounds = 10;
                let salt = bcrypt.genSaltSync(saltRounds);
                let hash = bcrypt.hashSync(plainPassword, salt);

                Admin.findByIdAndUpdate(adminId, { password: hash, forceUpdatePassword: false }).exec((err, updatePasswordSuccess) => {
                    if (!err && updatePasswordSuccess) {
                        res.redirect('/login?updatePassword=success')
                    } else {
                        res.redirect('/login?updatePassword=failed')
                    }
                })
            } else {
                res.redirect('/login?updatePassword=failed')
            }
        })
    } else {
        res.redirect('/login?updatePassword=failed')
    }
})

module.exports = app;