const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

    let queryUser = {status : {$in : ["new", "register", "personalInfo", "residentialInfo", "bankInfo", "workingInfo", "companyInfo", "emergencyInfo"]}}
    
    searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
    searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
    queryUser.createdAt = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
        searchByStartDate && searchByStartDate != "" ? 
        queryUser.createdAt = {$gte : moment(searchByStartDate).toISOString()} :
        searchByEndDate && searchByEndDate != "" ? 
        queryUser.createdAt = {$lte : moment(searchByEndDate).toISOString()} : ""

    let allInProgress = []
    User.find(queryUser).exec((err,allUser)=>{
        async.each(allUser,(eachUser,callback)=>{
            let stage;
            switch(eachUser.status){
                case "new" :
                    stage = "New";
                    break;
                case "register" :
                    stage = "Register";
                    break;
                case "personalInfo" :
                    stage = "Personal Info";
                    break;
                case "residentialInfo" :
                    stage = "Residential Info";
                    break;
                case "bankInfo" :
                    stage = "Bank Info";
                    break;
                case "workingInfo" :
                    stage = "Working Info";
                    break;
                case "companyInfo" :
                    stage = "Company Info";
                    break;
                case "emergencyInfo" :
                    stage = "Emergency Info";
                    break;
            }
            allInProgress.push({
                userId : eachUser._id,
                username : eachUser.username,
                createdAt : eachUser.createdAt,
                phoneNumber : eachUser.phoneNumber,
                ic : eachUser.ic,
                stage : stage,
                agent : eachUser.agent,
                agentId : eachUser.agentId,
                status : eachUser.status != "blacklisted" ? "Normal" : "Blacklisted",
            })
            callback()
        },(err)=>{
            allInProgress.sort((a,b)=>{
                return moment(b.createdAt).unix() - moment(a.createdAt).unix()
            })
            res.render('customer/manager/inProgress', {allInProgress: allInProgress, admin: req.session.admin});
        })
    })
})



module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}