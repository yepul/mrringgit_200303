const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact

    let queryLoan = {agentId: req.session.admin._id, status : "paid"}

    searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
    queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
        searchByStartDate && searchByStartDate != "" ? 
        queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString()} :
        searchByEndDate && searchByEndDate != "" ? 
        queryLoan.approvalManagerTime = {$lte : moment(searchByEndDate).toISOString()} : ""

    Loan.find(queryLoan).exec((err,allLoan)=>{
        let allOnLoan = []
        async.each(allLoan,(eachLoan,callback)=>{
            Payment.find({loanId: eachLoan._id, status: "accepted"},{amount:1, interestPaid:1, principalPaid:1}).exec((err,allPayment)=>{
                
                let collected = 0
                async.each(allPayment,(eachPayment,callback)=>{
                    collected += eachPayment.amount
                    callback()
                },(err)=>{
                    let queryUser = {_id : eachLoan.userId}
                    searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
                    User.findOne(queryUser,{username: 1, phoneNumber: 1, status: 1}).exec((err,userDetails)=>{
                        if(!err && userDetails){
                            allOnLoan.push({
                                loanId : eachLoan._id,
                                userId : userDetails._id,
                                username : userDetails.username,
                                phoneNumber : userDetails.phoneNumber,
                                loanType : eachLoan.loanType,
                                approvedAt : eachLoan.approvalManagerTime,
                                amount : parseInt(eachLoan.amount * 100) / 100,
                                duration : eachLoan.duration + " " + eachLoan.paymentType,
                                installment : parseInt(eachLoan.installment * 100) / 100,
                                collected : parseInt(collected * 100) / 100,
                                outstanding : parseInt(eachLoan.outstanding * 100) / 100,
                                numberOverdue : eachLoan.numberOverdue,
                                dayOverdue : eachLoan.dayOverdue,
                                amountOverdue : parseInt(eachLoan.amountOverdue * 100) / 100,
                                earningsAndLosses : parseInt(eachLoan.earningsAndLosses * 100) / 100,
                                agent : eachLoan.agent,
                                agentId : eachLoan.agentId,
                                status : userDetails.status == "loanApplication" ? "Normal" : "Not Normal",  
                                fine : eachLoan.fine,   
                                offset : eachLoan.offset,    
                                fullyPaidOn : eachLoan.fullyPaidOn,                            
                            })
                            callback()
                        }else{
                            callback()
                        }
                    })
                })
            })
        },(err)=>{
            allOnLoan.sort((a,b)=>{
                return moment(b.approvedAt).unix() - moment(a.approvedAt).unix()
            })
            res.render('customer/agent/paid', {allOnLoan: allOnLoan, admin: req.session.admin});
        })
    })
})



module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}