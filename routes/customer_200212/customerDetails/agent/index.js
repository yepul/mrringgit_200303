const express = require('express');
const async = require('async')
const moment = require('moment')
const multiparty = require('connect-multiparty');

let User = require('../../../../models/user/user');
let Loan = require('../../../../models/loan/loan');
let Payment = require('../../../../models/payment/payment');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/personalInfo/:userId', isLoggedIn,(req, res) => {
    let userId = req.params.userId
	User.findById(userId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoAgent/personalInfoView', { userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

app.get('/residentialInfo/:userId', isLoggedIn,(req, res) => {
    let userId = req.params.userId
	User.findById(userId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoAgent/residentialInfoView', { userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

app.get('/bankAccountInfo/:userId', isLoggedIn,(req, res) => {
    let userId = req.params.userId
	User.findById(userId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoAgent/bankAccountInfoView', { userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

app.get('/workingInfo/:userId', isLoggedIn,(req, res) => {
    let userId = req.params.userId
	User.findById(userId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoAgent/workingInfoView', { userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

app.get('/emergencyContactInfo/:userId', isLoggedIn,(req, res) => {
    let userId = req.params.userId
	User.findById(userId).exec((err,userDetails)=>{
		if(!err && userDetails){
			res.render('dashboard/customerInfoAgent/emergencyContactInfoView', { userDetails:userDetails, admin: req.session.admin });
		}else{
			res.json({status:400})
		}
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}