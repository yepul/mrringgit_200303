const express = require('express');
const async = require('async')
const moment = require('moment')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');
let Loan = require('../../../models/loan/loan');
let Payment = require('../../../models/payment/payment');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let valueOfEachItem = {
		onLoan : 0,
		overduePayment : 0,
		paid : 0,
		inProgress : 0,
		blacklisted : 0,
		rejected : 0
	}
	
	Loan.countDocuments({status : "accepted"}).exec((err,noOfOnLoan)=>{
		Loan.countDocuments({numberOverdue : {$gt : 0}}).exec((err,noOfOverduePayment)=>{
			Loan.countDocuments({status : "paid"}).exec((err,noOfPaid)=>{
				User.countDocuments({status : {$in : ["new", "register", "personalInfo", "residentialInfo", "bankInfo", "workingInfo", "companyInfo", "emergencyInfo"]}}).exec((err,noOfInProgress)=>{
					User.countDocuments({status : "blacklisted"}).exec((err,noOfBlacklisted)=>{
						Loan.countDocuments({status : "rejected"}).exec((err,noOfRejected)=>{
							valueOfEachItem.onLoan = noOfOnLoan
							valueOfEachItem.overduePayment = noOfOverduePayment
							valueOfEachItem.paid = noOfPaid
							valueOfEachItem.inProgress = noOfInProgress
							valueOfEachItem.blacklisted = noOfBlacklisted
							valueOfEachItem.rejected = noOfRejected
							res.render('customer/manager/main', {valueOfEachItem: valueOfEachItem, admin: req.session.admin});
						})							
					})					
				})
			})
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}