const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	res.render('customer/agent/main'/* ,{admin : req.session.admin} */);
})

app.get('/onLoan', isLoggedIn,(req, res) => {
	res.render('customer/agent/onLoan'/* ,{admin : req.session.admin} */);
})

app.get('/overduePayment', isLoggedIn,(req, res) => {
	res.render('customer/agent/overduePayment'/* ,{admin : req.session.admin} */);
})

app.get('/paid', isLoggedIn,(req, res) => {
	res.render('customer/agent/paid'/* ,{admin : req.session.admin} */);
})

app.get('/inProgress', isLoggedIn,(req, res) => {
	res.render('customer/agent/inProgress'/* ,{admin : req.session.admin} */);
})

app.get('/blacklist', isLoggedIn,(req, res) => {
	res.render('customer/agent/blacklist'/* ,{admin : req.session.admin} */);
})

app.get('/rejected', isLoggedIn,(req, res) => {
	res.render('customer/agent/rejected'/* ,{admin : req.session.admin} */);
})


module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}