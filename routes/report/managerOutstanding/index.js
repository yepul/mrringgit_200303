const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let searchByStartDate = req.query.searchByStartDate
	let searchByEndDate = req.query.searchByEndDate
	let searchByContact = req.query.searchByContact
	let totalOutstanding = 0

	let queryLoan = {status : "accepted"}

	searchByStartDate && searchByStartDate != "" && searchByEndDate && searchByEndDate != "" ? 
	queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString(),$lte : moment(searchByEndDate).toISOString()} : 
		searchByStartDate && searchByStartDate != "" ? 
		queryLoan.approvalManagerTime = {$gte : moment(searchByStartDate).toISOString()} :
		searchByEndDate && searchByEndDate != "" ? 
		queryLoan.approvalManagerTime = {$lte : moment(searchByEndDate).toISOString()} : ""

	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","report","reportList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			Loan.find(queryLoan).exec((err,allLoan)=>{
				let allOutstanding = []
				async.each(allLoan,(eachLoan,callback)=>{
					totalOutstanding += eachLoan.outstanding
		
					let queryPayment = {
						loanId : eachLoan._id,
						status : "accepted", 
					}
					Payment.find(queryPayment,{amount:1,interestPaid:1,principalPaid:1}).exec((err,allPayment)=>{
						let collected = 0
						if(!err && allPayment){
							async.each(allPayment,(eachPayment,callback)=>{
								collected += eachPayment.amount
								callback()
							},(err)=>{
								let queryUser = {_id : eachLoan.userId}
								searchByContact && searchByContact != "" ? queryUser.phoneNumber = searchByContact : ""
		
								User.findOne(queryUser,{username : 1, phoneNumber : 1}).exec((err,userDetails)=>{
									if(!err && userDetails){
										allOutstanding.push({
											loanId : eachLoan._id,
											username : userDetails.username,
											userId : userDetails._id,
											phoneNumber : userDetails.phoneNumber,
											loanType : eachLoan.loanType,
											paymentType : eachLoan.paymentType,
											// approvedOn : eachLoan.approvalManagerTime,
											approvedOn : moment(eachLoan.approvalManagerTime).add(8, 'hours').format('MMM Do, YYYY'),
											approvedOnGMT : eachLoan.approvalManagerTime,
											amount : parseInt(eachLoan.amount * 100) / 100,
											duration : eachLoan.duration + " " + eachLoan.paymentType,
											interest : parseInt(eachLoan.interest * 100) / 100,
											offset : parseInt(eachLoan.offset * 100) / 100,
											disbursement : parseInt(eachLoan.disbursement * 100) / 100,
											collected : parseInt(collected * 100) / 100,
											outstanding : parseInt(eachLoan.outstanding * 100) / 100,
											dayOverdue : eachLoan.dayOverdue,
											numberOverdue : eachLoan.numberOverdue,
											agent : eachLoan.agent,
											agentId : eachLoan.agentId
										})
										callback()
									}else{
										callback()
									}
								})
							})
						}else{
							callback()
						}
					})
		
				},(err)=>{
					allOutstanding.sort((a,b)=>{
						return moment(b.approvedOnGMT).unix() - moment(a.approvedOnGMT).unix()
					})
					res.render('report/manager/outstanding',{langData: langData, totalOutstanding: totalOutstanding, allOutstanding: allOutstanding, admin: req.session.admin});
				})
			})
		})
	})
})



module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}