const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let User = require('../../../models/user/user');
let Payment = require('../../../models/payment/payment');
let Loan = require('../../../models/loan/loan');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn,(req, res) => {
	let valueOfEachReport = {
		agentPerformance : 0,
		principal : 0,
		lendOut : 0,
		collected : 0,
		outstanding : 0
	}
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","report","reportList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			Payment.find({status : "accepted", approvalAgentId : req.session.admin._id},{principalPaid:1,amount:1}).exec((err,paymentReceived)=>{
				async.each(paymentReceived,(eachPayment,callback)=>{
					// valueOfEachReport.principal += eachPayment.principalPaid
					valueOfEachReport.collected += eachPayment.amount
					callback()
				},(err)=>{
					Loan.find({status : "accepted", agentId : req.session.admin._id},{disbursement:1,outstanding:1}).exec((err,loanApproved)=>{
						async.each(loanApproved,(eachLoan,callback)=>{
							valueOfEachReport.lendOut += eachLoan.disbursement
							valueOfEachReport.outstanding += eachLoan.outstanding
							callback()
						},(err)=>{
							parseInt(valueOfEachReport.principal * 100) / 100
							parseInt(valueOfEachReport.collected * 100) / 100
							parseInt(valueOfEachReport.lendOut * 100) / 100
							parseInt(valueOfEachReport.outstanding * 100) / 100
		
							res.render('report/agent/report',{langData: langData, valueOfEachReport : valueOfEachReport, admin : req.session.admin});
						})
					})
				})
			})
		})
	})
})

app.get('/agentPerformance', isLoggedIn,(req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","payment","paymentList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			res.render('report/agent/agentPerformance',{langData: langData, admin : req.session.admin});
		})
	})
})

app.get('/principal', isLoggedIn,(req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","payment","paymentList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			res.render('report/agent/principal',{langData: langData, admin : req.session.admin});
		})
	})
})

app.get('/lendOut', isLoggedIn,(req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","payment","paymentList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			res.render('report/agent/lendOut',{langData: langData, admin : req.session.admin});
		})
	})
})

app.get('/collected', isLoggedIn,(req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","payment","paymentList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			res.render('report/agent/collected',{langData: langData, admin : req.session.admin});
		})
	})
})

app.get('/outstanding', isLoggedIn,(req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	let langData = {}
	Language.find({type: {$in : ["header","payment","paymentList"]}}).exec((err,allLang)=>{
		async.each(allLang,(eachLang,callback)=>{
			langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
			callback()
		},(err)=>{
			res.render('report/agent/outstanding',{langData: langData, admin : req.session.admin});
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}