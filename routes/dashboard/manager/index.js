const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	let status = ["new", "register", "personalInfo", "residentialInfo", "bankInfo", "workingInfo", "companyInfo", "emergencyInfo", "loanApplication"]
	let userNoEachStatus = {
		new: 0,
		redirect: 0,
		personalInfo: 0,
		residentialInfo: 0,
		bankInfo: 0,
		workingInfo: 0,
		emergencyInfo: 0,
		loanApplication: 0
	}
	async.each(status, (eachStatus, callback) => {
		User.countDocuments({ status: eachStatus }).exec((err, eachCount) => {
			switch (eachStatus) {
				case "new":
					userNoEachStatus.new = eachCount;
					break;
				case "register":
					userNoEachStatus.register = eachCount;
					break;
				case "personalInfo":
					userNoEachStatus.personalInfo = eachCount;
					break;
				case "residentialInfo":
					userNoEachStatus.residentialInfo = eachCount;
					break;
				case "bankInfo":
					userNoEachStatus.bankInfo = eachCount;
					break;
				case "workingInfo":
					userNoEachStatus.workingInfo = eachCount;
					break;
				case "companyInfo":
					userNoEachStatus.companyInfo = eachCount;
					break;
				case "emergencyInfo":
					userNoEachStatus.emergencyInfo = eachCount;
					break;
				case "loanApplication":
					userNoEachStatus.loanApplication = eachCount;
					break;
				default:
					break;
			}
			callback()
		})
	}, (err) => {
		// res.json({userNoEachStatus : userNoEachStatus, admin : req.session.admin})
		res.render('dashboard/manager/home', { userNoEachStatus: userNoEachStatus, admin: req.session.admin });
	})
})

app.get('/customerList', isLoggedIn, (req, res) => {
	let searchByUsername = req.query.searchByUsername
	let searchByIc = req.query.searchByIc
	let searchByContact = req.query.searchByContact
	let searchByDayOverdue = req.query.searchByDayOverdue

	let query = {}

    searchByUsername && searchByUsername != "" ? query.username = searchByUsername : ""
    searchByIc && searchByIc != "" ? query.ic = searchByIc : ""
    searchByContact && searchByContact != "" ? query.phoneNumber = searchByContact : ""
	req.query.status ? query.status = req.query.status : ""
	User.find(query, { username: 1, phoneNumber: 1, ic: 1, status: 1, createdAt:1 }).exec((err, users) => {
		users.sort((a,b)=>{
			return moment(b.createdAt).unix() - moment(a.createdAt).unix()
		})
		res.render('dashboard/manager/customerList', { users: users, admin: req.session.admin });
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	// if (req.session.admin) {
	// 	return next();
	// }
	if (req.session.admin) {
		if(req.session.admin.level == "manager"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}