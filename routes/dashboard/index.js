const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');

let User = require('../../models/user/user');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	if(req.session.admin && req.session.admin.level == "agent"){
		res.redirect('/agent/dashboard')
	}else if(req.session.admin && req.session.admin.level == "manager"){
		res.redirect('/manager/dashboard')
	}
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		return next();
	}
	res.redirect('/login');
}