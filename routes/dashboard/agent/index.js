const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Loan = require('../../../models/loan/loan');
let Payment = require('../../../models/payment/payment');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	// console.log(lang)
    let langData = {}
    Language.find({type: {$in : ["header","dashboard"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
			let status = ["new", "register", "personalInfo", "residentialInfo", "bankInfo", "workingInfo", "companyInfo", "emergencyInfo", "loanApplication"]
			let userNoEachStatus = {
				new: 0,
				redirect: 0,
				personalInfo: 0,
				residentialInfo: 0,
				bankInfo: 0,
				workingInfo: 0,
				companyInfo: 0,
				emergencyInfo: 0,
				loanApplication: 0
			}
			async.each(status, (eachStatus, callback) => {
				User.countDocuments({ status: eachStatus }).exec((err, eachCount) => {
					switch (eachStatus) {
						case "new":
							userNoEachStatus.new = eachCount;
							break;
						case "register":
							userNoEachStatus.register = eachCount;
							break;
						case "personalInfo":
							userNoEachStatus.personalInfo = eachCount;
							break;
						case "residentialInfo":
							userNoEachStatus.residentialInfo = eachCount;
							break;
						case "bankInfo":
							userNoEachStatus.bankInfo = eachCount;
							break;
						case "workingInfo":
							userNoEachStatus.workingInfo = eachCount;
							break;
						case "companyInfo":
							userNoEachStatus.companyInfo = eachCount;
							break;
						case "emergencyInfo":
							userNoEachStatus.emergencyInfo = eachCount;
							break;
						case "loanApplication":
							userNoEachStatus.loanApplication = eachCount;
							break;
						default:
							break;
					}
					callback()
				})
			}, (err) => {
				// console.log(langData)
				res.render('dashboard/agent/home', { langData : langData, userNoEachStatus: userNoEachStatus, admin: req.session.admin });
			})
		})
	})
})

app.get('/customerList', isLoggedIn, (req, res) => {
	let searchByUsername = req.query.searchByUsername
	let searchByIc = req.query.searchByIc
	let searchByContact = req.query.searchByContact
	let searchByDayOverdue = req.query.searchByDayOverdue
	let searchByPaid = req.query.searchByPaid

	let query = {}
	query.$or = [{agentId: req.session.admin._id},{agentId : null}]
    searchByUsername && searchByUsername != "" ? query.username = new RegExp(searchByUsername, 'i') : ""
    searchByIc && searchByIc != "" ? query.ic = new RegExp(searchByIc, 'i') : ""
    searchByContact && searchByContact != "" ? query.phoneNumber = new RegExp(searchByContact, 'i') : ""
	req.query.status ? query.status = req.query.status : ""
	// console.log(query)
	
	let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type: {$in : ["header","dashboard","dashboardList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
			User.countDocuments(query).exec((err,totalUser)=>{
				totalUser ? totalUser : 0
				// let page = req.query.page ? req.query.page : 1
				// let limit = req.query.show ? parseInt(req.query.show) : 10
				// let noOfPages = Math.floor(totalUser/limit) > 0 ? Math.floor(totalUser/limit) : 1
				// let firstPage = page == 1 ? true : false
				// let lastPage = (page == noOfPages ? true : false)
				// let skip = parseInt((page - 1) * limit);
				
				User.find(query, { username: 1, phoneNumber: 1, ic: 1, status: 1, createdAt:1, blacklisted :1 }).lean()/* .skip(skip).limit(limit) */.exec((err, users) => {
					async.each(users,(eachUser,callback)=>{
						switch (eachUser.status) {
							case "new":
								eachUser.status = "New Customer";
								break;
							case "personalInfo":
								eachUser.status = "Personal Info";
								break;
							case "residentialInfo":
								eachUser.status = "Residential Info";
								break;
							case "bankInfo":
								eachUser.status = "Bank Account";
								break;
							case "workingInfo":
								eachUser.status = "Working Info";
								break;
							case "companyInfo":
								eachUser.status = "Company Info";
								break;
							case "emergencyInfo":
								eachUser.status = "Emergency Contact";
								break;
							case "loanApplication":
								eachUser.status = "Loan Application";
								break;
							default:
								break;
						}
						Loan.findOne({userId : eachUser._id, status : "accepted"},{numberOverdue:1,dayOverdue:1,duration:1,installment:1,agent:1}).exec((err,loanOverdue)=>{
							if(!err && loanOverdue){
								eachUser.agent = eachUser.agent ? eachUser.agent : "-"
								eachUser.numberOverdue = loanOverdue.numberOverdue ? loanOverdue.numberOverdue : 0
								eachUser.dayOverdue = loanOverdue.dayOverdue ? loanOverdue.dayOverdue : 0
								Payment.countDocuments({loanId : loanOverdue._id, status:"preset"}).exec((err,totalUnpaid)=>{
									eachUser.paid = parseInt(loanOverdue.duration) - totalUnpaid
									eachUser.unpaid = totalUnpaid
									
									let persentage = (parseInt(loanOverdue.duration) - totalUnpaid) / parseInt(loanOverdue.duration) * 100
									switch (true) {
										case persentage <= 25 || persentage == 0 :
											eachUser.paidPersentage = 25
											break;
										case persentage > 25 && persentage <= 50 :
											eachUser.paidPersentage = 50
											break;
										case persentage > 50 && persentage <= 75 :
											eachUser.paidPersentage = 75
											break;
										case persentage > 75 :
											eachUser.paidPersentage = 100
											break;
									}
									callback()
								})
							}else{
								eachUser.agent = "-"
								eachUser.numberOverdue = 0
								eachUser.paidPersentage = 25
								eachUser.dayOverdue = 0
								eachUser.paid = 0.00
								eachUser.unpaid = 0.00
								callback()
							}
						})
					},(err)=>{
						let userResults = []
						async.each(users,(eachUser,callback)=>{
							if(
								(searchByDayOverdue && searchByDayOverdue != "" ? eachUser.dayOverdue <= parseInt(searchByDayOverdue) : true)
								&& (searchByPaid ? 
									parseInt(searchByPaid) == 25 ? eachUser.paidPersentage <= 25 : 
									parseInt(searchByPaid) == 50 ? eachUser.paidPersentage > 25 && eachUser.paidPersentage <= 50 : 
									parseInt(searchByPaid) == 75 ? eachUser.paidPersentage > 50 && eachUser.paidPersentage <= 75 : 
									parseInt(searchByPaid) == 100 ? eachUser.paidPersentage > 75 && eachUser.paidPersentage <= 100 : true     
									: true)
							){
								userResults.push(eachUser)
								callback()
							}else{
								callback()
							}
						},(err)=>{
							// res.json(userResults)

							userResults.sort((a,b)=>{
								return moment(b.createdAt).unix() - moment(a.createdAt).unix()
							})
							res.render('dashboard/agent/customerList', { 
								langData : langData, 
								users: userResults, 
								userStatus: req.query.status,
								total: totalUser,
								// firstPage: firstPage,
								// lastPage: lastPage,
								// noOfPages: noOfPages,
								admin: req.session.admin 
							});
						})
					})
				})
			})
		})
	})
})

// app.get('/customerListNew', isLoggedIn, (req, res) => {
// 	let searchByUsername = req.query.searchByUsername
// 	let searchByIc = req.query.searchByIc
// 	let searchByContact = req.query.searchByContact
// 	let searchByDayOverdue = req.query.searchByDayOverdue

// 	let query = {}

//     searchByUsername && searchByUsername != "" ? query.username = searchByUsername : ""
//     searchByIc && searchByIc != "" ? query.ic = searchByIc : ""
//     searchByContact && searchByContact != "" ? query.phoneNumber = searchByContact : ""
// 	req.query.status ? query.status = req.query.status : ""

	
// 	let lang = req.session.lang ? req.session.lang : "en"
//     let langData = {}
//     Language.find({type: {$in : ["header","dashboard","dashboardList"]}}).exec((err,allLang)=>{
//         async.each(allLang,(eachLang,callback)=>{
//             langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
//             callback()
//         },(err)=>{
// 			User.countDocuments(query).exec((err,totalUser)=>{
// 				totalUser ? totalUser : 0
// 				let page = req.query.page ? req.query.page : 1
// 				let limit = req.query.show ? parseInt(req.query.show) : 10
// 				let noOfPages = Math.floor(totalUser/limit) > 0 ? Math.floor(totalUser/limit) : 1
// 				let firstPage = page == 1 ? true : false
// 				let lastPage = (page == noOfPages ? true : false)
// 				let skip = parseInt((page - 1) * limit);
				
// 				User.find(query, { username: 1, phoneNumber: 1, ic: 1, status: 1, createdAt:1 })/* .skip(skip).limit(limit) */.exec((err, users) => {
// 					if(!err && users){
// 						users.sort((a,b)=>{
// 							return moment(b.createdAt).unix() - moment(a.createdAt).unix()
// 						})
// 						res.render('dashboard/agent/customerListNew', { 
// 							langData : langData, 
// 							users: users, 
// 							userStatus: req.query.status,
// 							total: totalUser,
// 							firstPage: firstPage,
// 							lastPage: lastPage,
// 							noOfPages: noOfPages,
// 							admin: req.session.admin 
// 						});
// 					}else{
// 						res.render('dashboard/agent/customerListNew', { 
// 							langData : langData, 
// 							users: [], 
// 							userStatus: req.query.status,
// 							total: totalUser,
// 							firstPage: firstPage,
// 							lastPage: lastPage,
// 							noOfPages: noOfPages,
// 							admin: req.session.admin 
// 						});
// 					}
// 				})
// 			})
// 		})
// 	})
// })

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}