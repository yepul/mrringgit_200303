const express = require('express');
const async = require('async')
const multiparty = require('connect-multiparty');
const moment = require('moment')

let User = require('../../../models/user/user');
let Language = require('../../../models/language/language');

let app = express();
var multipartyMiddleware = multiparty();
app.use(multipartyMiddleware);

app.get('/', isLoggedIn, (req, res) => {
	let lang = req.session.lang ? req.session.lang : "en"
	// console.log(lang)
    let langData = {}
    Language.find({type: {$in : ["header","dashboard"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
			let status = ["new", "register", "personalInfo", "residentialInfo", "bankInfo", "workingInfo", "companyInfo", "emergencyInfo", "loanApplication"]
			let userNoEachStatus = {
				new: 0,
				redirect: 0,
				personalInfo: 0,
				residentialInfo: 0,
				bankInfo: 0,
				workingInfo: 0,
				companyInfo: 0,
				emergencyInfo: 0,
				loanApplication: 0
			}
			async.each(status, (eachStatus, callback) => {
				User.countDocuments({ status: eachStatus }).exec((err, eachCount) => {
					switch (eachStatus) {
						case "new":
							userNoEachStatus.new = eachCount;
							break;
						case "register":
							userNoEachStatus.register = eachCount;
							break;
						case "personalInfo":
							userNoEachStatus.personalInfo = eachCount;
							break;
						case "residentialInfo":
							userNoEachStatus.residentialInfo = eachCount;
							break;
						case "bankInfo":
							userNoEachStatus.bankInfo = eachCount;
							break;
						case "workingInfo":
							userNoEachStatus.workingInfo = eachCount;
							break;
						case "companyInfo":
							userNoEachStatus.companyInfo = eachCount;
							break;
						case "emergencyInfo":
							userNoEachStatus.emergencyInfo = eachCount;
							break;
						case "loanApplication":
							userNoEachStatus.loanApplication = eachCount;
							break;
						default:
							break;
					}
					callback()
				})
			}, (err) => {
				// console.log(langData)
				res.render('dashboard/agent/home', { langData : langData, userNoEachStatus: userNoEachStatus, admin: req.session.admin });
			})
		})
	})
})

app.get('/customerList', isLoggedIn, (req, res) => {
	let searchByUsername = req.query.searchByUsername
	let searchByIc = req.query.searchByIc
	let searchByContact = req.query.searchByContact
	let searchByDayOverdue = req.query.searchByDayOverdue

	let query = {}

    searchByUsername && searchByUsername != "" ? query.username = searchByUsername : ""
    searchByIc && searchByIc != "" ? query.ic = searchByIc : ""
    searchByContact && searchByContact != "" ? query.phoneNumber = searchByContact : ""
	req.query.status ? query.status = req.query.status : ""

	
	let lang = req.session.lang ? req.session.lang : "en"
    let langData = {}
    Language.find({type: {$in : ["header","dashboard","dashboardList"]}}).exec((err,allLang)=>{
        async.each(allLang,(eachLang,callback)=>{
            langData[eachLang.title] = (lang == "en" ? eachLang.en : (lang == "bm" ? eachLang.bm : eachLang.ch))
            callback()
        },(err)=>{
			User.countDocuments(query).exec((err,totalUser)=>{
				totalUser ? totalUser : 0
				let page = req.query.page ? req.query.page : 1
				let limit = req.query.show ? parseInt(req.query.show) : 10
				let noOfPages = Math.floor(totalUser/limit) > 0 ? Math.floor(totalUser/limit) : 1
				let firstPage = page == 1 ? true : false
				let lastPage = (page == noOfPages ? true : false)
				let skip = parseInt((page - 1) * limit);
				
				User.find(query, { username: 1, phoneNumber: 1, ic: 1, status: 1, createdAt:1 }).skip(skip).limit(limit).exec((err, users) => {
					if(!err && users){
						users.sort((a,b)=>{
							return moment(b.createdAt).unix() - moment(a.createdAt).unix()
						})
						res.render('dashboard/agent/customerList', { 
							langData : langData, 
							users: users, 
							userStatus: req.query.status,
							total: totalUser,
							firstPage: firstPage,
							lastPage: lastPage,
							noOfPages: noOfPages,
							admin: req.session.admin 
						});
					}else{
						res.render('dashboard/agent/customerList', { 
							langData : langData, 
							users: [], 
							userStatus: req.query.status,
							total: totalUser,
							firstPage: firstPage,
							lastPage: lastPage,
							noOfPages: noOfPages,
							admin: req.session.admin 
						});
					}
				})
			})
		})
	})
})

module.exports = app;

function isLoggedIn(req, res, next) {
	if (req.session.admin) {
		if(req.session.admin.level == "agent"){
			return next();
		}else{
			res.render('partion/500')
		}
	}
	res.redirect('/login');
}