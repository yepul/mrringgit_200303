const multer = require('multer');

const upload = multer({
  limits: {
    fileSize: 1000000000/* 4 * 1024 * 1024 */,
  }
});

module.exports = upload;